<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'domain' => env('ADMIN_URL'),
    //'as' => 'auth',
    'prefix' => 'admin',
    'middleware' => ['guest'],
    //'namespace' => ''
], function () {
    Route::get('/',                         ['uses' => 'IndexController@welcome',   'as' => 'admin.index.welcome']);



    Route::get('/login',                    ['uses' => 'Auth\LoginController@showLoginForm',                'as' => 'login']);
    Route::post('/login',                   ['uses' => 'Auth\LoginController@login',                        'as' => 'login.post']);

    Route::get('/register',                 ['uses' => 'Auth\RegisterController@showRegistrationForm',      'as' => 'register']);
    Route::post('/register',                ['uses' => 'Auth\RegisterController@register',                  'as' => 'register.post']);

    Route::get('/password/reset',           ['uses' => 'Auth\ForgotPasswordController@showLinkRequestForm', 'as' => 'password.request']);
    Route::post('/password/email',          ['uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail',  'as' => 'password.email.post']);
    Route::get('/password/reset/{token}',   ['uses' => 'Auth\ResetPasswordController@showResetForm',        'as' => 'password.reset']);
    Route::post('/password/reset',          ['uses' => 'Auth\ResetPasswordController@reset',                'as' => 'password.reset.post']);

    Route::get('/activate',                    ['uses' => 'Auth\LoginController@showActivateForm',                'as' => 'activate']);
    Route::post('/activate',                    ['uses' => 'Auth\LoginController@activate',                'as' => 'activate.post']);

});


Route::group([
    'domain' => env('ADMIN_URL'),
    //'as' => 'auth',
    'prefix' => 'admin',
    'middleware' => ['auth'],
    //'namespace' => ''
], function () {
    //Route::get('/',                         ['uses' => 'IndexController@welcome',   'as' => 'admin.index.welcome']);

    RouteCmf::resource('category',     'ProjectController');
    RouteCmf::resource('product',      'ProjectController');
    RouteCmf::resource('user',         'ProjectController');
    RouteCmf::resource('option',       'ProjectController');
    RouteCmf::resource('requisite',    'ProjectController');
    RouteCmf::resource('article',      'ProjectController');
    RouteCmf::resource('contact',      'ProjectController');
    RouteCmf::resource('certificate',  'ProjectController');
    RouteCmf::resource('partner',      'ProjectController');
    RouteCmf::resource('order',      'ProjectController');
    RouteCmf::resource('page',      'ProjectController');



    Route::get('/home',                     ['uses' => 'HomeController@index',                'as' => 'home']);
    Route::post('/home',                    ['uses' => 'HomeController@index',                'as' => 'home.post']);

    Route::post('/logout',                  ['uses' => 'Auth\LoginController@logout',         'as' => 'logout.post']);
});

Route::group([
    'domain' => env('APP_URL'),
    //'as' => 'auth',
    //'prefix' => '',
    'middleware' => ['guest'],
    //'namespace' => ''
], function () {
    Route::get('/',                     ['uses' => 'IndexController@index',         'as' => 'index.welcome']);
    Route::get('/sitemap',                     ['uses' => 'IndexController@sitemap',         'as' => 'index.sitemap']);
    Route::get('/search',                     ['uses' => 'IndexController@search',         'as' => 'index.search']);

    Route::post('/products/pagination', ['uses' => 'IndexController@pagination',    'as' => 'index.products.pagination']);

    Route::get('/products',             ['uses' => 'IndexController@products',      'as' => 'index.products']);
    Route::get('/category/{id}/{slug}', ['uses' => 'IndexController@category',      'as' => 'index.category']);
    Route::get('/product/{id}/{slug}',  ['uses' => 'IndexController@product',       'as' => 'index.product']);
    Route::get('/contacts',             ['uses' => 'IndexController@contacts',      'as' => 'index.contacts']);
    Route::get('/partners',             ['uses' => 'IndexController@partners',      'as' => 'index.partners']);
    Route::get('/certificates',         ['uses' => 'IndexController@certificates',  'as' => 'index.certificates']);

    Route::post('/basket',              ['uses' => 'BasketController@index',        'as' => 'index.basket.modal.post']);
    Route::post('/basket/{id}/destroy',      ['uses' => 'BasketController@destroy',      'as' => 'index.basket.destroy.post']);
    Route::post('/basket/clear',        ['uses' => 'BasketController@clear',        'as' => 'index.basket.clear.post']);
    Route::post('/basket/{id}/store',   ['uses' => 'BasketController@store',        'as' => 'index.basket.store.post']);
    Route::post('/basket/order',        ['uses' => 'BasketController@order',        'as' => 'index.basket.order.post']);

});

Route::group([
    'domain' => env('ADMIN_URL'),
    //'as' => 'auth',
    //'prefix' => '',
    'middleware' => ['auth'],
    //'namespace' => ''
], function () {
    //Route::get('/',                     ['uses' => 'IndexController@welcome',   'as' => 'index.welcome']);
    Route::get('/home',                     ['uses' => 'HomeController@index',                'as' => 'home']);


    Route::get('/iframe/{name}',            ['uses' => 'IndexController@iframe',    'as' => 'index.iframe']);



    /**
     * Project
     */
    Route::get('/chart',                ['uses' => 'IndexController@chart',     'as' => 'index.chart']);
    Route::get('/chart/json',           ['uses' => 'IndexController@chart',     'as' => 'index.chart.json']);
    //Route::get('/iframe',               ['uses' => 'IndexController@iframe',    'as' => 'index.iframe']);
    Route::get('/pages/{view}',         ['uses' => 'IndexController@view',      'as' => 'index.view']);
    Route::post('/tab/{tab}',           ['uses' => 'IndexController@tab',       'as' => 'index.tab.post']);

    Route::post('/pages/{view}',        ['uses' => 'IndexController@ajaxView',  'as' => 'index.view.post']);

    Route::get('/home',                 ['uses' => 'HomeController@index',      'as' => 'home.index']);

    Route::post('/loauth',               ['uses' => 'IndexController@loauth',    'as' => 'loauth']);
    Route::post('/confirm',              ['uses' => 'IndexController@confirm',   'as' => 'confirm']);

    Route::post('/pagination/search',   ['uses' => 'IndexController@search',    'as' => 'pagination.search.post']);

    Route::post('/image/{type}',        ['uses' => 'ImageController@type',      'as' => 'image.type.post']);

    Route::get('/logs', ['uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',      'as' => 'dev.logs']);
});



Route::get('/_debugbar/assets/stylesheets', [
    'as' => 'debugbar-css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
]);

Route::get('/_debugbar/assets/javascript', [
    'as' => 'debugbar-js',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('decompose', ['uses' => '\Lubusin\Decomposer\Controllers\DecomposerController@index',      'as' => 'dev.decompose']);
});


