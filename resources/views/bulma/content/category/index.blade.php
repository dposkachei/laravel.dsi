@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => isset($aComposerContent[$sComposerRouteView]['title']) ? $aComposerContent[$sComposerRouteView]['title'] : '',
    ])
@endsection

@section('content')
    <section class="section" style="padding-top: 0;">
        <div class="container">
            <nav class="level">
                <!-- Left side -->
                <div class="level-left">
                    <div class="level-item">
                        <p class="subtitle is-5">
                            @include('bulma.content.components.index.find', [
                                'oItems' => $oItems,
                                'units' => ['главная категория', 'главные категории', 'главных категорий'],
                                'gender' => true
                            ])
                        </p>
                    </div>
                </div>

                <!-- Right side -->
                <div class="level-right">
                    <p class="level-item">
                        <a class="button trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.create.modal.post') }}">Добавить</a>
                    </p>
                    {{--
                    @include('bulma.content.components.index.query', [
                        'model' => $sComposerRouteView,
                        'aQuery' => [
                            'status' => [
                                'value' => '1',
                                'title' => 'Опубликованные'
                            ]
                        ]
                    ])
                    --}}
                    {{--
                    @include('bulma.content.components.index.sort', [
                        'model' => $sComposerRouteView,
                        'aColumns' => [
                            'id' => 'ID',
                            'title' => 'Заголовку',
                            'created_at' => 'Дате создания',
                        ]
                    ])
                    --}}
                    {{--<p class="level-item"><a class="button is-dark">New</a></p>--}}
                </div>
            </nav>
            <div class="admin-table table-component-pagination">
                @include('bulma.content.'.$sComposerRouteView.'.components.table', [
                    'oItems' => $oItems
                ])
            </div>
        </div>
    </section>
@endsection


@section('modals')
    @include('bulma.pages.dialogs.components.ajax')
    @include('bulma.pages.dialogs.components.confirm')
    @include('bulma.pages.dialogs.components.form')
    @include('bulma.pages.dialogs.components.info')
@endsection