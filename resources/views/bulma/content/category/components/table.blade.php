<table class="table table-admin table--mobile">
    <thead>
    <tr>
        <th>#</th>
        <th>IMG</th>
        <th>Заголовок</th>
        <th>Файлы</th>
        <th>Приоритет</th>
        <th>Текст</th>
        <th>Продукций</th>
        <th></th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th>IMG</th>
        <th>Заголовок</th>
        <th>Файлы</th>
        <th>Приоритет</th>
        <th>Текст</th>
        <th>Продукций</th>
        <th></th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        @include('bulma.content.category.components.parentable', [
            'oItem' => $oItem,
            'disabled' => true
        ])
    @endforeach
    </tbody>
</table>
@if($oItems instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('bulma.components.pagination.ajax') }}
    @else
        {{ $oItems->links('bulma.components.pagination.ajax') }}
    @endif
@endif