<div class="dialog__content is-vcentered box" style="min-width: 300px; width: 300px;">
    <h3 class="title">
        Добавить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.store') }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >
        {{ csrf_field() }}
        <label for="email" class="label">Заголовок</label>
        <p class="control">
            <input class="input" name="title" type="text" placeholder="Заголовок" {{--required--}}>
        </p>
        <label for="email" class="label">Приоритет</label>
        <p class="control">
            <input class="input" name="priority" type="number" placeholder="Приоритет" value="0"{{--required--}}>
        </p>
        <label for="title" class="label">Родительская категория</label>
        <p class="control ">
            <span class="select" style="width: 100%;">
                <select @if(isset($nActiveParentId)) class="is-disabled" @endif name="parent_id" style="width: 100%;">
                    <option value="">Не указано</option>
                    @foreach($oParents as $oParent)
                        <option value="{{ $oParent->id }}" @if(isset($nActiveParentId) && $oParent->id === $nActiveParentId) selected @endif>{{ $oParent->id }}: {{ $oParent->title }}</option>
                    @endforeach
                </select>
            </span>
        </p>
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Добавить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Cancel
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>