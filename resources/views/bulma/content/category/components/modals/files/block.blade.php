@foreach($oItem->files as $oFile)
    <tr>
        <td>{{ $oFile->id }}</td>
        <td>{{ $oFile->filename }}</td>
        <td></td>
        <td>
            <a class="button is-small" href="{{ FilePath::image($sComposerRouteView, 'original', $oFile) }}" target="_blank">
                <span class="icon is-small">
                    <i class="fa fa-eye"></i>
                </span>
            </a>
        </td>
        <td>
            <a class="button is-danger ajax-link"
               action="{{ route($sComposerRouteView.'.file.destroy.post', ['id' => $oItem->id, 'file_id' => $oFile->id]) }}"
               data-loading="1"

               data-list=".admin-table"
               data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
               data-form-data=".pagination-form"

               data-callback="refreshModalAfterSubmit, refreshAfterSubmit"
               style="height: 1.75rem; width: 1.75rem; margin: 3px;"
            >
                <span class="icon is-small">
                    <i class="fa fa-trash"></i>
                </span>
            </a>
        </td>
    </tr>
@endforeach