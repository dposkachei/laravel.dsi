<tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
    <td>{{ $oItem->id }}</td>
    <td style="width: 30px;">
        @include('bulma.content.components.table.image', [
            'oItem' => $oItem,
            'cache' => true
        ])
    </td>
    <td>
        @if(isset($prefix)) {{ $prefix }} @endif
        <a href="{{ route('index.category', ['id' => $oItem->id, 'slug' => $oItem->url ]) }}">{{ $oItem->title }}</a>
    </td>
    <td>
        <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.files.post', ['id' => $oItem->id]) }}"
           data-ajax-init="uploader"
        >
            <span class="icon is-small">
                <i class="fa fa-file-o"></i>
            </span>
        </a>
    </td>
    <td>{{ $oItem->priority }}</td>
    <td>
        @include('bulma.content.components.table.text', [
            'oItem' => $oItem
        ])
    </td>
    <td style="text-align: center">{{ $oItem->products->count() }}</td>
    <td>
        <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.create.modal.post', ['parent_id' => $oItem->id]) }}"
            title="Добавить под-категорию в эту категорию"
        >
            <span class="icon is-small">
                <i class="fa fa-plus"></i>
            </span>
        </a>
    </td>
    <td>
        @include('bulma.content.components.table.status', [
            'oItem' => $oItem
        ])
    </td>
    <td>
        @include('bulma.content.components.table.edit', [
            'oItem' => $oItem
        ])
    </td>
    <td>
        @include('bulma.content.components.table.delete', [
            'oItem' => $oItem,
            'deleteKey' => 'категорию',
            'deleteValue' => $oItem->title,
        ])
    </td>
</tr>
@if(!empty($oItem->children) && !empty($oItem->children[0]))
    @foreach($oItem->children as $oChildren)
        @include('bulma.content.category.components.parentable', [
            'oItem' => $oChildren,
            'prefix' => isset($prefix) ? $prefix.' - ' : ' - '
        ])
    @endforeach
@endif