@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => isset($aComposerContent[$sComposerRouteView]['title']) ? $aComposerContent[$sComposerRouteView]['title'] : '',
    ])
@endsection

@section('content')
    <section class="section" style="padding-top: 0;">
        <div class="container">
            <nav class="level">
                <!-- Left side -->
                <div class="level-left">
                    <div class="level-item">
                        <p class="subtitle is-5">
                            @include('bulma.content.components.index.find', [
                                'oItems' => $oItems,
                                'units' => ['заказ', 'заказа', 'заказов'],
                                'gender' => true
                            ])
                        </p>
                    </div>
                </div>

                <!-- Right side -->
                <div class="level-right">
                    <span style="margin-right: 10px;">Показать: </span>
                    @include('bulma.content.components.index.query', [
                        'model' => $sComposerRouteView,
                        'aQuery' => [
                            'type' => [
                                [
                                    'value' => '1',
                                    'title' => 'Только заказ продукции'
                                ], [
                                    'value' => '2',
                                    'title' => 'Только обратный звонок'
                                ],
                            ]
                        ]
                    ])
                </div>
            </nav>
            <div class="admin-table table-component-pagination">
                @include('bulma.content.'.$sComposerRouteView.'.components.table', [
                    'oItems' => $oItems
                ])
            </div>
        </div>
    </section>
@endsection


@section('modals')
    @include('bulma.pages.dialogs.components.ajax')
    @include('bulma.pages.dialogs.components.confirm')
    @include('bulma.pages.dialogs.components.form')
    @include('bulma.pages.dialogs.components.info')
@endsection