<table class="table table-admin table--mobile">
    <thead>
    <tr>
        <th>#</th>
        <th>Тип</th>
        <th>Имя</th>
        <th>Телефон</th>
        <th>Почта</th>
        <th>Продукции</th>
        <th>Дата</th>
        <th>Статус</th>
        <th></th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th>Тип</th>
        <th>Имя</th>
        <th>Телефон</th>
        <th>Почта</th>
        <th>Продукции</th>
        <th>Дата</th>
        <th>Статус</th>
        <th></th>
        <th>Удалить</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if($oItem->status === 0 || $oItem->status === 3) style="opacity: .5;" @elseif($oItem->status === 2) style="color: #DF8D8D" @endif>
            <td>{{ $oItem->id }}</td>
            <td>
                <span class="icon">
                    <i class="fa fa-{{ $oItem->type_icon['class'] }}" title="{{ $oItem->type_text }}"></i>
                </span>
            </td>
            <td>@if(!is_null($oItem->name)){{ $oItem->name }} @else <span style="opacity: .5;">Не указан</span> @endif</td>
            <td>@if(!is_null($oItem->phone)){{ $oItem->phone }} @else <span style="opacity: .5;">Не указан</span> @endif</td>
            <td>@if(!is_null($oItem->email)){{ $oItem->email }} @else <span style="opacity: .5;">Не указан</span> @endif</td>
            <td>
                @if($oItem->isCall())
                    -
                @else
                    <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.action.post', ['name' => 'getProducts', 'id' => $oItem->id]) }}"
                       title="Продукции"
                    >
                    <span class="icon is-small">
                        <i class="fa fa-certificate"></i>
                    </span>
                    </a>
                    Кол-во: {{count($oItem->products)}} шт.
                @endif
            </td>
            <td>
                {{-- LocalizedCarbon::instance($oItem->created_at)->diffForHumans() --}}
                {{ $oItem->created_at->format('d.m.Y H:i:s') }}
            </td>
            <td>
                @include('bulma.content.components.table.status', [
                    'oItem' => $oItem
                ])
            </td>
            <td></td>
            <td>
                @include('bulma.content.components.table.delete', [
                    'oItem' => $oItem,
                    'deleteKey' => 'id',
                    'deleteValue' => $oItem->id,
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oItems instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('bulma.components.pagination.ajax') }}
    @else
        {{ $oItems->links('bulma.components.pagination.ajax') }}
    @endif
@endif