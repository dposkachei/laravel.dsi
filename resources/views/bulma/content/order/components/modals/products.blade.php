<div class="dialog__content is-vcentered box" style="min-width: 400px; width: 400px;">
    <h3 class="title">
        Продукции
    </h3>
    <div class="ajax-form" style="text-align: left;">
        @if(!empty($oProducts) && !empty($oProducts[0]))
            <table class="table table-admin table--mobile">
                <thead>
                <tr>
                    <th style="width: 20px;">#</th>
                    <th>Продукция</th>
                    <th>Ссылка</th>
                </tr>
                </thead>
                <tbody class="is-modal-body">
                @foreach($oProducts as $oProduct)
                    <tr>
                        <td style="width: 20px;">{{ $oProduct->id }}</td>
                        <td style="width: inherit;">{{ str_limit($oProduct->title, 50) }}</td>
                        <td>
                            <a href="{{ route('index.product', ['id' => $oProduct->id, 'slug' => $oProduct->url]) }}" target="_blank">Перейти</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p style="text-align: center">Список пуст.</p>
        @endif
    </div>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>