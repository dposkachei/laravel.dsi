<div class="dialog__content is-vcentered box" style="min-width: 400px; width: 400px;">
    <h3 class="title">
        Добавить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.store') }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >
        <label for="email" class="label">Заголовок</label>
        <p class="control">
            <input class="input" name="title" type="text" placeholder="Заголовок">
        </p>
        <label for="email" class="label">Ссылка</label>
        <p class="control">
            <input class="input" name="url" type="text" placeholder="Ссылка">
        </p>
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Добавить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Cancel
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>