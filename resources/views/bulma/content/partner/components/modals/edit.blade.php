<div class="dialog__content is-vcentered box" style="min-width: 400px; width: 400px;">
    <h3 class="title">
        Изменить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >{{-- loauth --}}
        <label for="title" class="label">Заголовок</label>
        <p class="control">
            <input class="input" name="title" type="text" placeholder="Заголовок" value="{{ $oItem->title }}">
        </p>
        <label for="email" class="label">Ссылка</label>
        <p class="control">
            <input class="input" name="url" type="text" placeholder="Ссылка" value="{{ $oItem->url }}">
        </p>
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Изменить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Cancel
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>