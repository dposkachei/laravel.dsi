<table class="table table-admin table--mobile">
    <thead>
    <tr>
        <th>#</th>
        <th>IMG</th>
        <th>Страница</th>
        <th>Заголовок</th>
        <th>Текст</th>
        <th></th>
        <th>Статус</th>
        <th>Изменить</th>
        <th></th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th>IMG</th>
        <th>Страница</th>
        <th>Заголовок</th>
        <th>Текст</th>
        <th></th>
        <th>Статус</th>
        <th>Изменить</th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
            <td>{{ $oItem->id }}</td>
            <td style="width: 30px;">
                @include('bulma.content.components.table.image', [
                    'oItem' => $oItem,
                    'cache' => true
                ])
            </td>
            <td>{{ $oItem->page_title }}</td>
            <td>{{ $oItem->title }}</td>
            <td>
                @include('bulma.content.components.table.text', [
                    'oItem' => $oItem
                ])
            </td>
            <td></td>
            <td>
                @include('bulma.content.components.table.status', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('bulma.content.components.table.edit', [
                    'oItem' => $oItem
                ])
            </td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oItems instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('bulma.components.pagination.ajax') }}
    @else
        {{ $oItems->links('bulma.components.pagination.ajax') }}
    @endif
@endif