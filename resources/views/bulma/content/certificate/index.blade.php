@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => isset($aComposerContent[$sComposerRouteView]['title']) ? $aComposerContent[$sComposerRouteView]['title'] : '',
    ])
@endsection

@section('content')
    <section class="section" style="padding-top: 0;">
        <div class="container">
            <nav class="level">
                <!-- Left side -->
                <div class="level-left">
                    <div class="level-item">
                        <p class="subtitle is-5">
                            @include('bulma.content.components.index.find', [
                                'oItems' => $oItems,
                                'units' => ['сертификат', 'сертификата', 'сертификатов'],
                                'gender' => false
                            ])
                        </p>
                    </div>
                </div>

                <!-- Right side -->
                <div class="level-right">
                    <span style="margin-right: 10px;">Показать: </span>
                    @include('bulma.content.components.index.query', [
                        'model' => $sComposerRouteView,
                        'aQuery' => [
                            'type' => [
                                [
                                    'value' => '0',
                                    'title' => 'Только обычные'
                                ], [
                                    'value' => '1',
                                    'title' => 'Только для продукции'
                                ]
                            ],
                        ]
                    ])
                    <p class="level-item">
                        <a class="button trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.create.modal.post') }}">Добавить</a>
                    </p>
                </div>
            </nav>
            <div class="admin-table table-component-pagination">
                @include('bulma.content.'.$sComposerRouteView.'.components.table', [
                    'oItems' => $oItems
                ])
            </div>
        </div>
    </section>
@endsection


@section('modals')
    @include('bulma.pages.dialogs.components.ajax')
    @include('bulma.pages.dialogs.components.confirm')
    @include('bulma.pages.dialogs.components.form')
    @include('bulma.pages.dialogs.components.info')
@endsection