<div class="dialog__content is-vcentered box" style="min-width: 400px; width: 400px;">
    <h3 class="title">
        Изменить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >{{-- loauth --}}
        {{ csrf_field() }}
        <label for="email" class="label">Заголовок</label>
        <p class="control">
            <input class="input" name="title" type="text" placeholder="Заголовок" value="{{ $oItem->title }}">
        </p>
        <label class="label">Описание</label>
        <p class="control">
            <textarea class="textarea" name="text" placeholder="Описание">{{ $oItem->text }}</textarea>
        </p>
        <label for="email" class="label">Приоритет</label>
        <p class="control">
            <input class="input" name="priority" type="number" placeholder="Приоритет" value="{{ $oItem->priority }}">
        </p>
        <label for="title" class="label" >Тип</label>
        <p class="control">
            <span class="select" style="width: 100%;">
                <select name="type" style="width: 100%;">
                    @foreach(Model::init('certificate')->getTypes() as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </span>
        </p>
        <label for="title" class="label">Статус</label>
        <p class="control ">
            <span class="select" style="width: 100%;">
                <select name="status" style="width: 100%;">
                    @foreach(Model::init('certificate')->getStatuses() as $key => $status)
                        <option value="{{ $key }}" @if($oItem->status === $key) selected @endif>{{ $status }}</option>
                    @endforeach
                </select>
            </span>
        </p>
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Изменить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Cancel
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>