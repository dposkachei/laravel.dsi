<table class="table table-admin table--mobile">
    <thead>
    <tr>
        <th>#</th>
        <th>IMG</th>
        <th>Заголовок</th>
        <th>Категория</th>
        <th>Текст</th>
        <th>Сертификаты</th>
        <th>Приоритет</th>
        <th>Seo</th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th>IMG</th>
        <th>Заголовок</th>
        <th>Категория</th>
        <th>Текст</th>
        <th>Сертификаты</th>
        <th>Приоритет</th>
        <th>Seo</th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
            <td>{{ $oItem->id }}</td>
            <td style="width: 30px;">
                @include('bulma.content.components.table.image', [
                    'oItem' => $oItem,
                    'path' => 'block',
                    'cache' => true
                ])
            </td>
            <td>
                <a href="{{ route('index.product', ['id' => $oItem->id, 'slug' => $oItem->url]) }}" target="_blank" title="Перейти на страницу">
                    {{ $oItem->title }}
                </a>
            </td>
            <td>{{ $oItem->category->title }}</td>
            <td>
                @include('bulma.content.components.table.text', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.action.post', ['name' => 'getCertificates', 'id' => $oItem->id]) }}"
                   title="Сертификаты"
                >
                    <span class="icon is-small">
                        <i class="fa fa-certificate"></i>
                    </span>
                </a>
                {{-- Кол-во: --}}{{ count($oItem->certificates) }} шт.
            </td>
            <td>
                {{ $oItem->priority }}
            </td>
            <td>
                @include('bulma.content.components.table.trigger.icon', [
                    'oItem' => $oItem,
                    'route' => route($sComposerRouteView.'.action.post', [
                        'name' => 'getSeoParameters',
                        'id' => $oItem->id
                    ]),
                    'title' => 'Seo',
                    'icon' => 'fa-globe'
                ])
            </td>
            <td>
                @include('bulma.content.components.table.status', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('bulma.content.components.table.edit', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('bulma.content.components.table.delete', [
                    'oItem' => $oItem,
                    'deleteKey' => 'продукт',
                    'deleteValue' => $oItem->title,
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oItems instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('bulma.components.pagination.ajax') }}
    @else
        {{ $oItems->links('bulma.components.pagination.ajax') }}
    @endif
@endif