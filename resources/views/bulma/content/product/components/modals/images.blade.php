<div class="dialog__content is-vcentered box" style="min-width: 300px;margin-left: 10px; margin-right: 10px;">
    <h3 class="title" style="margin-bottom: 10px;">
        Изображения
    </h3>
    <form style="text-align: left;">
        {{ csrf_field() }}
        <div class="columns is-mobile is-multiline is-modal-body">
            @if(!empty($oItem->images) && !empty($oItem->images[0]))
                @include('bulma.content.'.$sComposerRouteView.'.components.modals.images.block', [
                    'oItem' => $oItem
                ])
            @endif
        </div>
        <p class="control">
            <input type="file" name="images[]" id="file" class="is-file" accept="image/*"
                   data-url="{{ route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]) }}"
                   data-id="{{ $oItem->id }}"

                   data-counter=".admin-table-counter"
                   data-list=".admin-table"
                   data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                   data-form-data=".pagination-form"
                   multiple
            >
            <label for="file" class="button label-file">
                <span class="icon is-small">
                    <i class="fa fa-upload"></i>
                </span>
                <span class="label-file-name">Выберите файл</span>
            </label>
        </p>
    </form>
    <div style="position: absolute; top: 10px; right: 10px;">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>