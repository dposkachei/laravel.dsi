<div class="dialog__content is-vcentered box" style="min-width: 400px; width: 400px;">
    <h3 class="title">
        Сео-параметры
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.action.post', [
        'name' => 'saveSeoParameters', 'id' => $oItem->id
    ]) }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >
        <label for="email" class="label">Description (описание)</label>
        <p class="control">
            <textarea
                    class="textarea"
                    name="description"
                    placeholder="Description"
            >{{ $oSeo->description or '' }}</textarea>
        </p>
        <label for="email" class="label">Keywords (ключевые слова)</label>
        <p class="control">
            <textarea
                    class="textarea"
                    name="keywords"
                    placeholder="ключевое слово, слово, слово и т.д."
            >{{ $oSeo->keywords or '' }}</textarea>
        </p>
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Сохранить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Закрыть
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>