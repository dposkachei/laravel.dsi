<div class="dialog__content is-vcentered box">
    <h3 class="title">
        Сертификаты
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.action.post', [
        'name' => 'saveCertificates', 'id' => $oItem->id
    ]) }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >
        @if(!empty($oCertificates) && !empty($oCertificates[0]))
            <table class="table table-admin table--mobile">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Сертификат</th>
                    <th>Приоритет</th>
                    <th>Активировать</th>
                </tr>
                </thead>
                <tbody class="is-modal-body">
                @foreach($oCertificates as $oCertificate)
                    <tr>
                        <td>{{ $oCertificate->id }}</td>
                        <td>{{ $oCertificate->title }}</td>
                        <td>
                            <p class="control">
                                @if(!is_null($oProductCertificate = $oItem->certificates()->where('certificate_id', $oCertificate->id)->first()))
                                    <input class="input" name="product[certificates][{{ $oCertificate->id }}][priority]" type="number" value="{{ $oProductCertificate->pivot->priority }}">
                                @else
                                    <input class="input" name="product[certificates][{{ $oCertificate->id }}][priority]" type="number" value="">
                                @endif
                            </p>
                        </td>
                        <td>
                            <p class="control">
                                <label class="checkbox">
                                    <input type="hidden" name="product[certificates][{{ $oCertificate->id }}][id]" value="{{ $oCertificate->id }}">
                                    <input type="checkbox" name="product[certificates][{{ $oCertificate->id }}][status]"
                                           @if(!is_null($oItem->certificates()->where('certificate_id', $oCertificate->id)->first())) checked @endif
                                    >
                                    Активировать
                                </label>
                            </p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <p class="control">
                <button class="button is-primary inner-form-submit" type="submit">
                    Сохранить
                </button>
                <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                    Cancel
                </button>
            </p>
        @else
            <p>Список пуст. Добавьте сертификаты для продукции.</p>
        @endif
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>