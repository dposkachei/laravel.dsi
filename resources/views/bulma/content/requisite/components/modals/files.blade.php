<div class="dialog__content is-vcentered box" style="min-width: 300px;margin-left: 10px; margin-right: 10px;">
    <h3 class="title">
        Файлы
    </h3>
    <form style="text-align: left;">
        <table class="table table-admin table--mobile">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th></th>
                <th>Посмотреть</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody class="is-modal-body">
                @include('bulma.content.'.$sComposerRouteView.'.components.modals.files.block', [
                    'oItem' => $oItem
                ])
            </tbody>
        </table>
        <p class="control">
            <input type="file" name="files[]" id="file" class="is-file" accept="application/pdf"
                   data-url="{{ route($sComposerRouteView.'.file.upload.post', ['id' => $oItem->id]) }}"
                   data-id="{{ $oItem->id }}"

                   data-counter=".admin-table-counter"
                   data-list=".admin-table"
                   data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                   data-form-data=".pagination-form"
                   multiple
            >
            <label for="file" class="button label-file">
                <span class="icon is-small">
                    <i class="fa fa-upload"></i>
                </span>
                <span class="label-file-name">Выберите файл</span>
            </label>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>