<table class="table table-admin table--mobile">
    <thead>
    <tr>
        <th>#</th>
        <th>Иконка</th>
        <th>Заголовок</th>
        <th>Текст</th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th>Иконка</th>
        <th>Заголовок</th>
        <th>Текст</th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
            <td>{{ $oItem->id }}</td>
            <td style="width: 30px;text-align: center;">
                <span class="icon">
                    <i class="fa fa-{{ $oItem->icon }}"></i>
                </span>
            </td>
            <td>
                <b>{{ $oItem->title }}</b>
            </td>
            <td>
                @include('bulma.content.components.table.text', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('bulma.content.components.table.status', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('bulma.content.components.table.edit', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('bulma.content.components.table.delete', [
                    'oItem' => $oItem,
                    'deleteKey' => 'id',
                    'deleteValue' => $oItem->id,
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oItems instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('bulma.components.pagination.ajax') }}
    @else
        {{ $oItems->links('bulma.components.pagination.ajax') }}
    @endif
@endif