<div class="dialog__content is-vcentered box" @if($oItem->type !== 2) style="min-width: 300px; width: 300px;" @else style="min-width: 300px; width: 600px;" @endif>
    <h3 class="title">
        Изменить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >
        @if($oComposerGenerateForm && isset($oComposerModelColumns[$sComposerRouteView]['form']))
            @foreach($oComposerModelColumns[$sComposerRouteView]['form'] as $key => $column)
                @if(View::exists('bulma.content.components.form.controls.'.$column['type']))
                    @include('bulma.content.components.form.controls.'.$column['type'], [
                        'key' => $key,
                        'column' => $column,
                        'oItem' => $oItem
                    ])
                @endif
            @endforeach
        @else
            <input type="hidden" name="title" value="{{ $oItem->title }}">
            <input type="hidden" name="key" value="{{ $oItem->key }}">
            <input type="hidden" name="type" value="{{ $oItem->type }}">
            {{-- Скрыть от пользователя менять заголовок, ключ, тип
            <label for="email" class="label" style="margin-bottom: 0;">Заголовок</label>
            <p class="control">
                <input
                        class="input"
                        name="title"
                        type="text"
                        placeholder="Заголовок"
                        value="{{ $oItem->title }}"
            >
            </p>
            <label for="email" class="label" style="margin-bottom: 0;">Ключ</label>
            <p class="control">
                <input
                        class="input"
                        name="key"
                        type="text"
                        placeholder="Ключ"
                        value="{{ $oItem->key }}"
                >
            </p>

            --}}

            @if(View::exists('bulma.content.option.components.controls.'.$oItem->type))
                <label for="email" class="label" style="margin-bottom: 0;">Значение</label>
                @include('bulma.content.option.components.controls.'.$oItem->type)
            @endif
            {{--
            <label for="title" class="label" style="margin-bottom: 0;">Тип поля</label>
            <p class="control">
                <span class="select" style="width: 100%;">
                    <select name="type" style="width: 100%;">
                        @foreach(Model::init('option')->getTypes() as $key => $type)
                            <option value="{{ $key }}" @if($oItem->type === $key) selected @endif>{{ $type }}</option>
                        @endforeach
                    </select>
                </span>
            </p>
            --}}
        @endif
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Изменить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Отмена
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>