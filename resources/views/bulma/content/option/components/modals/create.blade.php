<div class="dialog__content is-vcentered box" style="min-width: 300px; width: 300px;">
    <h3 class="title">
        Добавить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.store') }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >
        @if($oComposerGenerateForm && isset($oComposerModelColumns[$sComposerRouteView]['form']))
            @foreach($oComposerModelColumns[$sComposerRouteView]['form'] as $key => $column)
                @if(View::exists('bulma.content.components.form.controls.'.$column['type']))
                    @include('bulma.content.components.form.controls.'.$column['type'], [
                        'key' => $key,
                        'column' => $column
                    ])
                @endif
            @endforeach
        @else
            <label for="email" class="label" style="margin-bottom: 0;">Заголовок</label>
            <p class="control">
                <input
                        class="input"
                        name="title"
                        type="text"
                        placeholder="Заголовок"
                        {{--required--}}
                >
            </p>
            <label for="email" class="label" style="margin-bottom: 0;">Ключ</label>
            <p class="control">
                <input
                        class="input"
                        name="key"
                        type="text"
                        placeholder="Ключ"
                        {{--required--}}
                >
            </p>
            <label for="email" class="label" style="margin-bottom: 0;">Значение</label>
            @include('bulma.content.option.components.controls.1')
            <label for="title" class="label" style="margin-bottom: 0;">Тип поля</label>
            <p class="control">
                <span class="select" style="width: 100%;">
                    <select name="type" style="width: 100%;">
                        @foreach(Model::init('option')->getTypes() as $key => $type)
                            <option value="{{ $key }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </span>
            </p>
        @endif
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Добавить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Cancel
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>