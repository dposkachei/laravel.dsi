<table class="table table-admin table--mobile">
    <thead>
    <tr>
        <th>#</th>
        <th>Заголовок</th>
        {{--<th>Ключ</th>--}}
        <th>Значение</th>
        <th></th>
        <th>Статус</th>
        <th>Изменить</th>
        {{--<th>Удалить</th>--}}
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th>Заголовок</th>
        {{--<th>Ключ</th>--}}
        <th>Значение</th>
        <th></th>
        <th>Статус</th>
        <th>Изменить</th>
        {{--<th>Удалить</th>--}}
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
            <td>{{ $oItem->id }}</td>
            <td>
                {{--<a href="{{ route($sComposerRouteView.'.show', ['id' => $oItem->id]) }}">{{ $oItem->title }}</a>--}}
                <b>
                    {{ $oItem->title }}
                </b>
            </td>
            {{--<td>{{ $oItem->key }}</td>--}}
            <td>
                @if($oItem->isImage)
                    @include('bulma.content.components.table.image', [
                        'oItem' => $oItem,
                        'cache' => true
                    ])
                @else
                    {{ str_limit($oItem->value, 30) }}
                @endif

            </td>
            <td></td>
            <td style="width: 150px;">
                @include('bulma.content.components.table.status', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @if($oItem->isImage)
                    <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax
                       data-action="{{ route($sComposerRouteView.'.image.post', ['id' => $oItem->id]) }}"
                       data-ajax-init="uploader"
                    >
                        <span class="icon is-small">
                            <i class="fa fa-pencil"></i>
                        </span>
                    </a>
                @else
                    <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax
                       data-action="{{ route($sComposerRouteView.'.edit.modal.post', ['id' => $oItem->id]) }}"
                       @if($oItem->isPhone)
                       data-ajax-init="uploader"
                       @endif
                       data-id="123">
                        <span class="icon is-small">
                            <i class="fa fa-pencil"></i>
                        </span>
                    </a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oItems instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('bulma.components.pagination.ajax') }}
    @else
        {{ $oItems->links('bulma.components.pagination.ajax') }}
    @endif
@endif