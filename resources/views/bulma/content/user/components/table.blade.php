<table class="table table-admin table--mobile">
    <thead>
    <tr>
        <th>ID</th>
        <th>IMG</th>
        <th>Имя</th>
        <th>Email</th>
        <th>Роль</th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>IMG</th>
        <th>Имя</th>
        <th>Email</th>
        <th>Роль</th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if(!$oItem->status) style="opacity: .5;" @endif>
            <td>{{ $oItem->id }}</td>
            <td style="width: 30px;">
                @include('bulma.content.components.table.image', [
                    'oItem' => $oItem,
                    'cache' => true
                ])
            </td>
            <td>
                <a href="{{ route($sComposerRouteView.'.show', ['id' => $oItem->id]) }}">
                    {{ $oItem->first_name }}
                </a>
                @if(isset($oItem->activation))
                    @if(!$oItem->activation->completed)
                        <span class="icon is-danger tippy" title="Пользователь не активирован">
                                <i class="fa fa-exclamation"></i>
                            </span>
                    @endif
                @else
                    <span class="icon is-danger tippy" title="Пользователь не активирован">
                            <i class="fa fa-exclamation"></i>
                        </span>
                @endif
                @if(!Sentinel::guest() && Sentinel::getUser()->id === $oItem->id)
                    <span class="icon">
                            <i class="fa fa-user"></i>
                        </span>
                @endif
            </td>
            <td>
                {{ $oItem->email }}
            </td>
            <td>
                {{ $oItem->role->name }}
            </td>
            <td>
                @include('bulma.content.components.table.status', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('bulma.content.components.table.edit', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('bulma.content.components.table.delete', [
                    'oItem' => $oItem,
                    'deleteKey' => 'пользователя',
                    'deleteValue' => $oItem->first_name,
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@if($oItems instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('bulma.components.pagination.ajax') }}
    @else
        {{ $oItems->links('bulma.components.pagination.ajax') }}
    @endif
@endif