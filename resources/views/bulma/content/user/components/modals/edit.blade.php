<div class="dialog__content is-vcentered box" style="min-width: 300px; max-width: 300px;">
    <h3 class="title">
        Изменить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >{{-- loauth --}}
        {{ csrf_field() }}
        <p class="control has-icon" >
            <input
                    class="input"
                    type="text"
                    name="first_name"
                    placeholder="Name"
                    value="{{ $oItem->first_name }}"
                    required
            >
                <span class="icon is-small">
                    <i class="fa fa-user"></i>
                </span>
        </p>
        <p class="control has-icon" >
            <input
                    class="input"
                    name="email"
                    type="email"
                    placeholder="Email"
                    value="{{ $oItem->email }}"
                    required
            >
                <span class="icon is-small">
                    <i class="fa fa-envelope"></i>
                </span>
        </p>
        <p class="control" >
            <span class="select" style="width: 100%;">
                <select name="role_id" style="width: 100%;">
                    @foreach($oComposerRoles as $role)
                        <option value="{{ $role->id }}" @if($role->id === $oItem->role->id) selected @endif>{{ $role->name }}</option>
                    @endforeach
                </select>
            </span>
        </p>
        <p class="control has-icon" >
            <input
                    class="input"
                    type="password"
                    name="password"
                    placeholder="New password"

            >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
        </p>
        <p class="control has-icon" >
            <input
                    class="input"
                    type="password"
                    name="password_confirmation"
                    placeholder="Confirm password"

            >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
        </p>
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Изменить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Cancel
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>