<div class="dialog__content is-vcentered box" style="min-width: 300px; width: 300px;">
    <h3 class="title" style="margin-bottom: 10px;">
        Изменить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >{{-- loauth --}}
        {{ csrf_field() }}
        @if($oComposerGenerateForm && isset($oComposerModelColumns[$sComposerRouteView]['form']))
            @foreach($oComposerModelColumns[$sComposerRouteView]['form'] as $key => $column)
                @if(View::exists('bulma.content.components.form.controls.'.$column['type']))
                    @include('bulma.content.components.form.controls.'.$column['type'], [
                        'key' => $key,
                        'column' => $column,
                        'oItem' => $oItem
                    ])
                @endif
            @endforeach
        @else
            <label for="title" class="label" style="margin-bottom: 0;">Заголовок</label>
            <p class="control">
                <input
                        class="input"
                        name="title"
                        type="text"
                        placeholder="Заголовок"
                        value="{{ $oItem->title }}"
                        {{--required--}}
                >
            </p>
        @endif
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Изменить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Cancel
            </button>
        </p>
    </form>
    <div style="position: absolute; top: 10px; right: 10px;">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>