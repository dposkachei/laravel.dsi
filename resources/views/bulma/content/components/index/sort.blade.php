{{--
    'model' => 'products',
    'aColumns' => [
        'id' => 'ID',
        'status' => 'Статусу',
        'title' => 'Заголовку',
        'created_at' => 'Дате создания',
    ]

--}}

<div class="level-item box">
    <span style="padding-bottom: 1px;">
        Сортировать по
    </span>
    <span class="select is-small" style="padding-left: 10px;">
        <select class="ajax-select" action="{{ route($model.'.sort') }}"
                name="sort"
                data-list=".admin-table"
                data-list-action="{{ route($model.'.view.post') }}"
                data-form-data=".pagination-form"
                data-callback="refreshAfterSubmit"
        >
            @foreach($aColumns as $key => $text)
                <option value="{{ $key }}"
                    @if(!Session::exists($model) && $key === 'created_at')
                        selected
                    @elseif(Session::exists($model) && !Session::has($model.'.sort') && $key === 'created_at')
                        selected
                    @elseif(Session::exists($model) && Session::has($model.'.sort') && !Session::has($model.'.sort.column') && $key === 'created_at')
                        selected
                    @elseif(Session::exists($model) && Session::has($model.'.sort') && Session::get($model.'.sort.column') === $key)
                        selected
                    @endif
                >{{ $text }}</option>
            @endforeach
        </select>
    </span>
    <a class="button is-small tippy ajax-link" data-loading="1"
       @if(Session::exists($model) && Session::has($model.'.sort') && Session::get($model.'.sort.type') === 'desc')
       title="Сортировка по убыванию" on-title="Сортировка по возврастанию" off-title="Сортировка по убыванию"
       @else
       title="Сортировка по возврастанию" on-title="Сортировка по возврастанию" off-title="Сортировка по убыванию"
       @endif
       style="margin: 0 5px;"
       action="{{ route($model.'.sort') }}"
       name="type"
       value="desc"
       data-ajax-init="tippy-tooltip"
       data-list=".admin-table"
       data-list-action="{{ route($model.'.view.post') }}"
       data-form-data=".pagination-form"
       data-callback="refreshAfterSubmit, replaceIconAfterSubmit, replaceFormAttributesAfterSubmit"
    >
        <span class="icon is-small">
           <i
               @if(Session::exists($model) && Session::has($model.'.sort') && Session::get($model.'.sort.type') === 'desc')
               class="fa fa-angle-down"
               @else
               class="fa fa-angle-up"
               @endif
               on-class="fa fa-angle-up" off-class="fa fa-angle-down"></i>
        </span>
    </a>
</div>