<div class="dialog__content is-vcentered box" style="min-width: 300px; width: 90%; height: 90%;">
    <h3 class="title">
        Текстовой редактор
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]) }}"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="height: calc(100% - 80px);"
    >
        @include('bulma.components.loaders.square', [
            'visible' => true,
            'id' => 'text-preloader'
        ])
        <iframe id="iframe-summernote" src="/iframe/summernote" width="100%" height="300" align="center" style="display: none;height: 100%;"></iframe>
        <p class="control" style="display: none;">
            <textarea id="parent" class="textarea" name="text" placeholder="Описание">{{ $oItem->text }}</textarea>
        </p>
        <p class="control" style="margin-top: 10px;">
            <button class="button is-primary inner-form-submit" type="submit">
                Сохранить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Отменить
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>