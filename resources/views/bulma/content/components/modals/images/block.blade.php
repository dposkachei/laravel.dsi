@foreach($oItem->images as $image)
    <div class="column is-2" style="position: relative;">
        <img src="{{ ImagePath::image($sComposerRouteView, 'original', $image) }}" height="550" width="1360">
        <div style="position: absolute; top: 11px; right: 11px;">
            <a class="button ajax-link @classIf($image->is_main, 'is-success', 'is-white')"
               action="{{ route($sComposerRouteView.'.image.main.post', ['id' => $oItem->id, 'image_id' => $image->id]) }}"
               data-loading="1"
               data-list=".admin-table"
               data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
               data-form-data=".pagination-form"

               data-callback="refreshModalAfterSubmit, refreshAfterSubmit"
               style="height: 1.75rem; width: 1.75rem; margin: 3px;"
            >
                <span class="icon is-small">
                    <i class="fa fa-check"></i>
                </span>
            </a>
            <a class="button is-danger ajax-link"
               action="{{ route($sComposerRouteView.'.image.destroy.post', ['id' => $oItem->id, 'image_id' => $image->id]) }}"
               data-loading="1"

               data-list=".admin-table"
               data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
               data-form-data=".pagination-form"

               data-callback="refreshModalAfterSubmit, refreshAfterSubmit"
               style="height: 1.75rem; width: 1.75rem; margin: 3px;"
            >
                <span class="icon is-small">
                    <i class="fa fa-times"></i>
                </span>
            </a>
        </div>
    </div>
@endforeach
