<a class="button is-small tippy-html trigger" data-dialog="#pages-dialogs-ajax" data-ajax
   data-action="{{ route($sComposerRouteView.'.image.post', ['id' => $oItem->id]) }}"
   data-ajax-init="uploader"
   {{--data-id="123"--}}
    data-html="tippy-template-{{$oItem->id}}"
   style="padding: 0; border: none;"
>
    @if(isset($path))
        @if(isset($cache) && $cache)
            <img src="{{ ImagePath::cache()->main($sComposerRouteView, $path, $oItem) }}" width="30">
        @else
            <img src="{{ ImagePath::main($sComposerRouteView, $path, $oItem) }}" width="30">
        @endif
    @else
        @if(isset($cache) && $cache)
            <img src="{{ ImagePath::cache()->main($sComposerRouteView, 'original', $oItem) }}" width="30">
        @else
            <img src="{{ ImagePath::main($sComposerRouteView, 'original', $oItem) }}" width="30">
        @endif
    @endif
</a>
<span id="tippy-template-{{$oItem->id}}" style="display: none;">
    <div class="box" style="margin: -10px -15px;">
        <img src="{{ ImagePath::main($sComposerRouteView, 'original', $oItem) }}" width="200">
    </div>
</span>