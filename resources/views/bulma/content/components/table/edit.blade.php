<a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax
   data-action="{{ route($sComposerRouteView.'.edit.modal.post', ['id' => $oItem->id]) }}"
   {{--data-id="123"--}}
>
    <span class="icon is-small">
        <i class="fa fa-pencil"></i>
    </span>
</a>