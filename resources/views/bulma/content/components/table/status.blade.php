<div class="is-select">
    <p class="control">
        <span class="select is-small">
            <select class="ajax-select" action="{{ route($sComposerRouteView.'.status', ['id' => $oItem->id]) }}"
                    data-status="1"
                    name="status"
                    data-list=".admin-table"
                    data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                    data-form-data=".pagination-form"
                    data-callback="refreshAfterSubmit @if(isset($sCallbacks)), {{ $sCallbacks }}@endif"
            >
                @foreach(Model::init($sComposerRouteView)->getStatuses() as $key => $status)
                    <option value="{{ $key }}" @if($key === $oItem->status) selected @endif>{{ $status }}</option>
                @endforeach
            </select>
        </span>
    </p>
</div>