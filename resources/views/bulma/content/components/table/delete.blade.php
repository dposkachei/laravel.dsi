<a class="button is-danger is-small trigger" data-dialog="#pages-dialogs-confirm" data-confirm
   data-text='Действительно хотите удалить {{$deleteKey}} "{{ $deleteValue }}" ?'
   data-action="{{ route($sComposerRouteView.'.destroy', ['id' => $oItem->id]) }}"
   data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
>
    <span class="icon is-small">
        <i class="fa fa-trash"></i>
    </span>
</a>