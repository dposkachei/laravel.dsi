<a class="button is-small trigger"
   data-dialog="#pages-dialogs-ajax"
   data-ajax
   data-action="{{ $route }}"
   title="{{ $title or '' }}"
>
    <span class="icon is-small">
        <i class="fa {{ $icon or 'fa-globe' }}"></i>
    </span>
</a>