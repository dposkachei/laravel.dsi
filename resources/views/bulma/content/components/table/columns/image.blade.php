<td style="width: 30px;">
    <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.image.post', ['id' => $oItem->id]) }}"
       data-ajax-init="uploader"
       data-id="123"
       style="padding: 0; border: none;"
    >
        <img src="{{ ImagePath::cache()->main($sComposerRouteView, 'original', $oItem) }}" width="30">
    </a>
</td>