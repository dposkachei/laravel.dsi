@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Login'
    ])
@endsection

@section('content')
    <section class="section {{-- is-medium --}}" style="padding-top: 0;padding-left: 1.75rem;">
        <div class="container">
            <div class="columns">
                <div class="column is-half @if($sComposerLayout === 'app') is-offset-one-quarter @endif " style="padding: 0;">
                    <form class="ajax-form" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <label for="email" class="label">E-Mail Address</label>
                        <p class="control">
                            <input id="email" type="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help is-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </p>

                        <label for="password" class="label">Password</label>
                        <p class="control">
                            <input id="password" type="password" class="input {{ $errors->has('password') ? 'is-danger' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help is-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </p>

                        <p class="control">
                            <label class="checkbox is-unselectable">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                Remember me
                            </label>
                        </p>

                        <div class="control is-grouped">
                            <p class="control">
                                <button type="submit" class="button is-primary inner-form-submit">Login</button>
                            </p>
                            <p class="control">
                                <a class="button is-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
                            </p>
                        </div>
                    </form>
                    Type <code>npm install</code> in your terminal to install a project's dependencies.
                    <dialog>
                        This is a dialog box!
                    </dialog>

                </div>
            </div>
        </div>
    </section>
@endsection
