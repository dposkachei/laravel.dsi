@extends('bulma.layouts.simple')

@section('content')
    <section class="hero is-medium is-bold">
        <div class="hero-body">
            <div class="container">
                <form class="ajax-form" role="form" method="POST" action="{{ route('activate.post') }}">

                    <h1 class="title">
                        Активационный код
                    </h1>
                    <p class="control">
                        <input type="text" class="input" name="code" required>
                    </p>
                    <div class="control is-grouped">
                        <p class="control">
                            <button type="submit" class="button is-primary inner-form-submit">{{ trans('auth.activate') }}</button>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </section>

@endsection
