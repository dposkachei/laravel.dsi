<div id="login" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content is-vcentered box" style="width: 300px;">
        <h3 class="title" style="margin-bottom: 10px;">
            Редактировать
        </h3>
        <form class="ajax-form" role="form" method="POST" action="{{ route('logout') }}{{-- route('items.action.post',['name' => 'get']) --}}"
              data-callback="closeModalAfterSubmit"
        >{{-- loauth --}}
            {{ csrf_field() }}
            <p class="control has-icon" >
                <input
                        class="input"
                        type="text"
                        name="name"
                        placeholder="Name"
                        value="{{ Sentinel::getUser()->first_name}}"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-user"></i>
                </span>
            </p>
            <p class="control has-icon" >
                <input
                        class="input"
                        name="email"
                        type="email"
                        placeholder="Email"
                        value="{{ Sentinel::getUser()->email}}"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-envelope"></i>
                </span>
            </p>
            <p class="control has-icon" >
                <input
                        class="input"
                        type="password"
                        name="password"
                        placeholder="New password"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
            </p>
            <p class="control has-icon" >
                <input
                        class="input"
                        type="password"
                        name="password_confirmation"
                        placeholder="Confirm password"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
            </p>
            <p class="control">
                <button class="button is-primary inner-form-submit" type="submit">
                    Save
                </button>
                <button class="dialog__close button" type="button" data-dialog="#login">
                    Cancel
                </button>
            </p>
        </form>
        <div style="position: absolute; top: 10px; right: 10px;">
            <a class="dialog__close icon" data-dialog="#login">
                <i class="fa fa-times"></i>
            </a>
        </div>
        <p class="dialog__content--helper-link">
            <a class="trigger" data-dialog="#password-email">
                Forgot Password
            </a>
        </p>
    </div>
</div>