@include('bulma.components.dialogs.auth.login')
@include('bulma.components.dialogs.auth.register')
@include('bulma.components.dialogs.auth.passwords.email')
@include('bulma.components.dialogs.auth.passwords.reset')


@include('bulma.components.dialogs.search')
{{--@include('bulma.components.dialogs.summernote')--}}

{{--@include('bulma.pages.dialogs.components.ajax')--}}
@include('bulma.components.dialogs.ajax')

@include('bulma.components.dialogs.icons')