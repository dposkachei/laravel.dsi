<div id="register" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content is-vcentered box" style="width: 300px;">
        <h3 class="title" style="margin-bottom: 10px;">
            Регистрация
        </h3>
        <form class="ajax-form" role="form" method="POST" action="{{ route('register') }}"
              data-callback="closeModalAfterSubmit"
        >
            {{ csrf_field() }}
            <p class="control has-icon" >
                <input
                        class="input"
                        type="text"
                        name="first_name"
                        placeholder="Name"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-user"></i>
                </span>
            </p>
            <p class="control has-icon">
                <input
                        class="input"
                        name="email"
                        type="email"
                        placeholder="Email"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-envelope"></i>
                </span>
            </p>
            <p class="control has-icon" >
                <input
                        class="input"
                        type="password"
                        name="password"
                        placeholder="Password"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
            </p>
            <p class="control has-icon" >
                <input
                        class="input"
                        type="password"
                        name="password_confirmation"
                        placeholder="Confirm password"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
            </p>
            <p class="control">
                <button class="button is-primary inner-form-submit" type="submit">
                    Register
                </button>
                <button class="dialog__close button" type="button" data-dialog="#register">
                    Cancel
                </button>
            </p>
        </form>
        <div style="position: absolute; top: 10px; right: 10px;">
            <a class="dialog__close icon" data-dialog="#register">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
</div>