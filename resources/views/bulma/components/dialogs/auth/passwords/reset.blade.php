<div id="password-reset" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content is-vcentered box" style="width: 300px;">
        <h3 class="title" style="margin-bottom: 10px;">
            Reset Password
        </h3>
        <form class="ajax-form" role="form" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <p class="control has-icon" >
                <input
                        class="input"
                        name="email"
                        type="email"
                        placeholder="Email"
                        {{--required--}}
                >
                <span class="icon is-small">
                    <i class="fa fa-envelope"></i>
                </span>
            </p>
            <p class="control has-icon" >
                <input
                        class="input"
                        type="password"
                        name="password"
                        placeholder="Password"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
            </p>
            <p class="control has-icon" >
                <input
                        class="input"
                        type="password"
                        name="password_confirmation"
                        placeholder="Confirm password"
                        required
                >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
            </p>
            <p class="control">
                <button class="button is-primary inner-form-submit" type="submit">
                    Reset Password
                </button>
                <button class="dialog__close button" type="button" data-dialog="#password-reset">
                    Cancel
                </button>
            </p>
        </form>
        <div style="position: absolute; top: 10px; right: 10px;">
            <a class="dialog__close icon" data-dialog="#password-reset">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
</div>