<div id="dialog-summernote" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content is-vcentered box" style="min-width: 300px; width: 800px;">
        <h3 class="title" style="margin-bottom: 10px;">
            Текстовой редактор
        </h3>
        <form class="ajax-form" role="form" method="POST" action="{{ route('loauth') }}">
            {{ csrf_field() }}
            <p class="control">
                <iframe src="/iframe{{-- asset('html/summernote.html') --}}" width="100%" height="300" align="center">
                    {{--@include('bulma.components.summernote')--}}
                </iframe>
            </p>
            <p class="control">
                <button class="button is-primary inner-form-submit" type="submit">
                    Save
                </button>
                <button class="dialog__close button" type="button" data-dialog="#dialog-summernote">
                    Cancel
                </button>
            </p>
        </form>
        <div style="position: absolute; top: 10px; right: 10px;">
            <a class="dialog__close icon" data-dialog="#dialog-summernote">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
</div>