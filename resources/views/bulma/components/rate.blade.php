<p class="stars" style="height: 14px;">
    @if(isset($nRate))
        @for($i = 0; $i < 5; $i++)
            @if($i < $nRate)
                <span class="icon is-small">
                <i class="fa fa-star" style="color:#ed6c63;"></i>
            </span>
            @else
                <span class="icon is-small">
                <i class="fa fa-star" style="color:#4a4a4a;"></i>
            </span>
            @endif
        @endfor
    @else
        @for($i = 0; $i < 5; $i++)
            <span class="icon is-small">
                <i class="fa fa-star" style="color:#4a4a4a;"></i>
            </span>
        @endfor
    @endif
</p>