<section @if(isset($id)) id="{{$id}}" @endif class="section section--loader" @if(isset($visible) && $visible) style="height: 100%;" @else style="display: none;height: 100%;" @endif>
    <div class="container-loader vertical-center">
        <div class="square-spin is-dark">
            <div></div>
        </div>
    </div>
</section>