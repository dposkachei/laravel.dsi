{{--
	'title'     => 'Hello world!',
	'body'      => 'How's it hangin'?',
	'icon'      => 'icon.png',
	'timeout'   => 4000,
--}}
<script>
    Push.create("{{ $title or 'Заголовок'}}", {
        body: "{{ $body or 'Текст сообщения'}}",
        icon: "{{ $icon or asset('img/cover.png')}}",
        timeout: "{{ $timeout or 10000}}",
        vibrate: [200,100],
        onClick: function () {
            window.focus();
            this.close();
        }
    });

</script>