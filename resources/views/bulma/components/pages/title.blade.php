<section class="section" @if($sComposerLayout === 'admin') style="padding: 5px 0 0 0;" @endif>
    <div class="container" @if($sComposerLayout === 'admin') style="padding-left: 1rem;" @endif>
        <h1 class="title">{{ $title }}</h1>
    </div>
    <hr>
</section>