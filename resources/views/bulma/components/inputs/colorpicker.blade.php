<link rel="stylesheet" href="{{ asset('css/bootstrap-colorpicker.css') }}">
<script src="{{ asset('js/bootstrap-colorpicker.min.js') }}"></script>

<p id="cp2" class="control has-addons colorpicker-component">
    <input class="input" type="text"  value="#00AABB">
    <a class="button is-info input-group-addon" style="width: 50px;"></a>
</p>
<script>
    $(function() {
        $('#cp2').colorpicker({
            container: '.cp1',
            colorSelectors: {
                'black': '#000000',
                'white': '#ffffff',
                'red': '#FF0000',
                'default': '#777777',
                'primary': '#337ab7',
                'success': '#5cb85c',
                'info': '#5bc0de',
                'warning': '#f0ad4e',
                'danger': '#d9534f'
            }
        });
        //$('#cp2').colorpicker('show');
    });
</script>