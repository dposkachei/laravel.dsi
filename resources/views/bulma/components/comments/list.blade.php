@foreach($oComments as $oComment)
    @include('bulma.components.comments.block', [
        'oComment' => $oComment
    ])
@endforeach