<nav class="nav has-shadow nav-desta">
    <div class="container">
        <div class="nav-left" style="overflow: visible;">
            <a class="nav-item nav-logo" href="{{ url('/') }}">
                {{ $oComposerSite->app->title }}
                {{--@classIf(isset($oComposerSite), 'is-loading', 'dsd')--}}
            </a>
        </div>
        <span class="nav-toggle">
            <span></span>
            <span></span>
            <span></span>
        </span>
        <div class="nav-right nav-menu">
            @if(!Sentinel::guest())
                {{-- 
                <span class="nav-item">
                    <a class="button is-inverted tippy" title="Количество просмотров" >
                        <span class="icon is-small">
                           <i class="fa fa-users"></i>
                        </span>
                        <span>{{ YandexMetrika::getVisitsViewsUsers()->data['total_rows'] }}</span>
                    </a>
                    <a class="button is-inverted">
                        <span class="icon is-small">
                           <i class="fa fa-heart"></i>
                        </span>
                        <span>10</span>
                    </a>
                    <a class="button is-inverted">
                        <span class="icon is-small">
                           <i class="fa fa-envelope"></i>
                        </span>
                        <span>10</span>
                    </a>
                </span>

                <span class="nav-item">
                    <a class="button is-primary is-inverted trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route('loauth') }}">
                        <span class="icon is-small">
                           <i class="fa fa-envelope-o"></i>
                        </span>
                        <span>10</span>
                    </a>
                </span>
                --}}
            @endif



            @if (Sentinel::guest())
                <span class="nav-item">
                    <a class="button is-primary is-inverted trigger" data-dialog="#login">
                        <span class="icon is-small">
                            <i class="fa fa-sign-in"></i>
                        </span>
                        <span>Войти</span>
                    </a>
                    {{--
                    <a class="button is-primary is-inverted trigger" data-dialog="#register">
                        Регистрация
                    </a>
                    --}}
                </span>
            @else
                <a class="nav-item trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route('user'.'.image.post', ['id' => Sentinel::getUser()->id]) }}"
                   data-ajax-init="uploader"
                   data-id="123"
                   style="padding: 0; border: none;"
                >
                    <img data-user-image src="{{ ImagePath::cache()->main('user', 'original', Sentinel::getUser()) }}" width="25" style="border-radius: 100%;">
                </a>
                {{--
                <a class="nav-item nav-logo tippy-html trigger" data-html="tippy-template" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route('user.edit.modal.post', ['id' => Sentinel::getUser()->id]) }}" title="Редактировать" href="{{ url('/') }}">
                    {{ Sentinel::getUser()->email }}
                </a>
                <span id="tippy-template" style="display: none;">
                    <div class="box" style="margin: -10px -15px;">
                        <div>
                            {{ Sentinel::getUser()->first_name }}
                        </div>
                        <a class="button is-inverted ajax-link" action="{{ route('logout.post') }}" data-loading="1">
                        <span class="icon is-small">
                           <i class="fa fa-user"></i>
                        </span>
                            <span>Выйти</span>
                        </a>
                    </div>
                </span>
                --}}
                <a class="nav-item nav-logo tippy-popover trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route('user.edit.modal.post', ['id' => Sentinel::getUser()->id]) }}" title="Редактировать" href="{{ url('/') }}">
                    {{ Sentinel::getUser()->email }}
                </a>
                <span class="nav-item">
                    <a class="button is-inverted ajax-link" action="{{ route('logout.post') }}" data-loading="1">
                        <span class="icon is-small">
                           <i class="fa fa-user"></i>
                        </span>
                        <span>Выйти</span>
                    </a>
                </span>

            @endif
        </div>
    </div>
</nav>