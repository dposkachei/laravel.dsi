@hasSection('footer')
    @yield('footer')
@else
    <footer class="footer">
        <div class="container">
            <div class="content has-text-centered">
                <p>
                    <strong>{{ $oComposerSite->app->title }}</strong>
                </p>
                <p>
                    {{ \Tremby\LaravelGitVersion\GitVersionHelper::getVersion() }}
                    @include('git-version::version-comment')
                </p>
            </div>
        </div>
    </footer>
@endif

