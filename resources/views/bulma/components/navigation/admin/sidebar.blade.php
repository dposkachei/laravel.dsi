<div class="column is-3 nav-fixed-sidebar" style="padding-top: 0px;">
    <span class="nav-toggle">
        <span></span>
        <span></span>
        <span></span>
    </span>
    <div class="container box nav-desta" style="border: 1px solid #eee;border-bottom: none;border-top: none;border-radius: 0px;box-shadow: none;">
        <aside class="menu" style="margin-top: 30px;">
            {{--
            <p class="menu-label">
                Страницы
            </p>
            <ul class="menu-list">
                <li><a href="/login" @if(Route::current()->getName() === 'login') class="is-active" @endif>Вход</a></li>
                <li><a href="/register" @if(Route::current()->getName() === 'register') class="is-active" @endif>Регистрация</a></li>
            </ul>
            --}}

            <p class="menu-label">
                Контент
            </p>
            <ul class="menu-list">
                @foreach($aComposerContent as $key => $aContent)
                <li>
                    <a
                        @if(starts_with(Route::current()->getName(), $key))
                            class="menu-list__button is-active"
                        @else
                            class="menu-list__button"
                        @endif
                        href="{{ route($key.'.index') }}"
                        style="position: relative;"
                    >
                        <span @if(starts_with(Route::current()->getName(), $key)) class="icon is-small is-primary" @else class="icon is-small" @endif style="position: absolute;top: 0.6em;left: -0.75em;">
                            <i class="{{ $aContent['icon'] }}"></i>
                        </span>
                        {{ $aContent['title'] }}
                        {{--
                        <span class="icon is-small" style="position: absolute;top: 0.7em;right: 0.75em;">
                           <i class="fa fa-angle-down"></i>
                        </span>
                        --}}
                    </a>
                    {{--
                    <ul class="menu-sub-list">
                        @foreach($aContent['cruds'] as $keyCrud => $crud)
                            <li>
                                <a href="{{ route($key.'.'.$keyCrud) }}" @if(starts_with(Route::current()->getName(), $key.'.'.$keyCrud)) class="is-active" @endif>{{ $crud['title'] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    --}}
                </li>
                @endforeach
            </ul>
            {{--
            <p class="menu-label">
                Шаблоны
            </p>
            <ul class="menu-list">
                <li>
                    <a
                        @if(isset(Route::current()->parameterNames[0]) && Route::current()->parameterNames[0] === 'view')
                        class="menu-list__button is-active"
                        @else
                        class="menu-list__button"
                        @endif
                        style="position: relative;"
                    >
                        Pages
                        <span class="icon is-small" style="position: absolute;top: 0.7em;right: 0.75em;">
                           <i class="fa fa-angle-down"></i>
                        </span>
                    </a>
                    <ul class="menu-sub-list">
                        @foreach($aComposerPages as $key => $aComposerPage)
                            <li>
                                <a href="{{ url('/pages/'.$aComposerPage) }}" @if(isset(Route::current()->parameters['view']) && Route::current()->parameters['view'] === $aComposerPage) class="is-active" @endif>{{ $aComposerPage }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
            <p class="menu-label">
                Для разработки
            </p>
            <ul class="menu-list">
                <li>
                    <a href="{{ route('dev.decompose') }}">
                        Decompose
                    </a>
                </li>
                <li>
                    <a href="{{ route('dev.logs') }}">
                        Logs
                    </a>
                </li>
            </ul>
            <p class="menu-label">
                Модальные окна
            </p>
            <ul class="menu-list">
                <li>
                    <a class="trigger" data-dialog="#dialog-summernote">
                        Summernote
                    </a>
                </li>
            </ul>
            --}}
        </aside>
    </div>
</div>
<a class="button is-inverted nav-fixed-sidebar-hide-show is-active" style="position: absolute; left: 20px; margin: 5px;">
    <span class="icon is-small">
       <i class="fa fa-arrows-h"></i>
    </span>
</a>
