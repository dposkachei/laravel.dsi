<nav class="nav has-shadow nav-desta">
    <div class="container">
        <div class="nav-left" style="overflow: visible;">
            <a class="nav-item nav-logo" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <div class="tcon-checkbox">
                <input type="checkbox" id="option" aria-checked="false" role="checkbox">
                <label for="option">Label Text</label>
            </div>
        </div>
        <span class="nav-toggle">
            <span></span>
            <span></span>
            <span></span>
        </span>
        <div class="nav-right nav-menu">

            {{--<a href="/" class="nav-item is-tab">Главная</a>--}}

            @if (Auth::guest())
                <ul class="nav-item is-tab dropdown-link ">
                    <li>
                        <a href="#!">Страницы</a>
                    </li>
                    <li class="dropdown-menu">
                        @if(count($aComposerPages) > 5)
                            @foreach($aComposerPages as $key => $aComposerPage)
                                @if($key%2 === 0)
                                    <div class="columns" style="margin: 0;">
                                        <div class="column" style="padding: 0;min-width: 100px;">
                                            <a href="{{ url('/pages/'.$aComposerPage) }}" class="dropdown-menu__item">{{ $aComposerPage }}</a>
                                        </div>
                                @else
                                        <div class="column" style="padding: 0;min-width: 100px;">
                                            <a href="{{ url('/pages/'.$aComposerPage) }}" class="dropdown-menu__item">{{ $aComposerPage }}</a>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            @if(count($aComposerPages)%2 === 0)
                                {!! '</div>' !!}
                            @endif
                        @else
                            @foreach($aComposerPages as $key => $aComposerPage)
                                <div class="columns" style="margin: 0;">
                                    <div class="column" style="padding: 0;">
                                        <a href="{{ url('/pages/'.$aComposerPage) }}" class="dropdown-menu__item">{{ $aComposerPage }}</a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </li>
                </ul>
                <span class="nav-item">
                    <a class="button is-primary is-inverted" href="/login">
                        <span class="icon is-small">
                            <i class="fa fa-sign-in"></i>
                        </span>
                        <span>Войти</span>
                    </a>
                    <a class="button is-primary is-inverted" href="/register">
                        Регистрация
                    </a>

                    <a class="button is-primary is-inverted trigger" data-dialog="#login">
                        <span class="icon is-small">
                            <i class="fa fa-sign-in"></i>
                        </span>
                        <span>Войти Dialog</span>
                    </a>

                    <a class="button is-primary is-inverted trigger" data-dialog="#register">
                        Регистрация Dialog
                    </a>
                    <a class="button is-primary is-inverted trigger" data-dialog="#dialog-summernote">
                        Summernote Dialog
                    </a>
                    @include('bulma.components.navigation.app.search')
                </span>
            @else
                <a class="nav-item nav-logo" href="{{ url('/') }}">
                    {{ Auth::user()->name }}
                </a>
                <span class="nav-item">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="button is-primary is-inverted">
                        <span class="icon is-small">
                           <i class="fa fa-user"></i>
                        </span>
                        <span>Выйти</span>
                    </a>
                </span>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif
        </div>
    </div>
</nav>