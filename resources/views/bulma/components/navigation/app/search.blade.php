<form class="search-from" action="" style="display: none; margin: auto 10px; position: relative;">
    <p class="control has-icon" style="margin: auto;display: inline-block;">
        <input class="input" type="text" placeholder="Search" >
        <span class="icon is-small">
            <i class="fa fa-search"></i>
        </span>
    </p>
    <a class="button is-primary is-inverted trigger-search-submit"
       style="
        position: absolute;
        top: 4px;
        right: 4px;
        margin: auto;
        width: 25px;
        height: 24px;
        padding-left: .25rem;
        padding-right: .25rem;
    ">
        <span class="icon is-small">
            <i class="fa fa-search"></i>
        </span>
    </a>
</form>
<a class="button is-primary is-inverted trigger-search-open">
    <span class="icon is-small">
        <i class="fa fa-search"></i>
    </span>
</a>
<script>
    $('.trigger-search-open').click(function() {
        if ($(this).hasClass('search-is-active')) {
            $(this).removeClass('search-is-active');
            $(this).find('i').removeClass('fa-close').addClass('fa-search');
            $('.search-from').css('display', 'none');
        } else {
            $(this).addClass('search-is-active');
            $(this).find('i').removeClass('fa-search').addClass('fa-close');
            $('.search-from').css('display', 'block');
        }
    });
    $('.trigger-search-submit').click(function() {
        $('.search-from').submit();
    });
</script>