@extends('bulma.layouts.app')

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <h1 class="title">{{ config('app.name', 'Laravel') }}</h1>
        </div>
    </section>
@endsection
