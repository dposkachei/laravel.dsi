<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Bulma Mix</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Laravel & Angular & Bulma">
    <meta property="og:title" content="Laravel & Angular & Bulma">
    <meta property="og:description" content="Laravel & Angular & Bulma">
    <meta property="og:image" content="">
    <meta property="og:url" content="http://dposkachei.github.io/">
    <meta property="og:site_name" content="dposkachei.github.io">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">

</head>
<body style="background-color: #ffffff;">
    <div id="app">
        <!-- NAVIGATION -->
        <nav class="nav has-shadow nav-desta">
            <div class="container">
                <div class="nav-left">
                    <a class="nav-item nav-logo" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
                <span class="nav-toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
                <div class="nav-right nav-menu">

                    {{--<a href="/" class="nav-item is-tab">Главная</a>--}}

                    @if (Auth::guest())
                        <span class="nav-item">
                            <a class="button is-primary is-inverted" href="/login">
                                <span class="icon is-small">
                                    <i class="fa fa-sign-in"></i>
                                </span>
                                <span>Войти</span>
                            </a>
                            <a class="button is-primary is-inverted" href="/register">
                                Регистрация
                            </a>
                        </span>
                    @else
                        {{ Auth::user()->name }}
                        <span class="nav-item">
                            <a class="button is-primary is-inverted">
                                <span class="icon is-small">
                                   <i class="fa fa-user"></i>
                                </span>
                                <span>Выйти</span>
                            </a>
                        </span>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endif
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/jquery-3.1.0.min.js') }}"></script>--}}
    {{--<script src="{{ asset('js/components.js') }}"></script>--}}
</body>
</html>
