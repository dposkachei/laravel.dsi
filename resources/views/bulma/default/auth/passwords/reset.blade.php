@extends('bulma.layouts.app')

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <div class="columns">
                <div class="column is-half is-offset-one-quarter box" style="padding: 20px 30px 30px 30px;">
                    <h1 class="title">Reset Password</h1>

                    @if (session('status'))
                        <div class="container">
                            <div class="notification is-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    @endif

                    <form role="form" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <label for="email" class="label">E-Mail Address</label>
                        <p class="control">
                            <input id="email" type="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help is-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </p>

                        <label for="password" class="label">Password</label>
                        <p class="control">
                            <input id="password" type="password" class="input {{ $errors->has('password') ? 'is-danger' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help is-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </p>

                        <label for="password-confirm" class="label">Confirm Password</label>
                        <p class="control">
                            <input id="password-confirm" type="password" class="input {{ $errors->has('password_confirmation') ? 'is-danger' : '' }}" name="password_confirmation" required>
                            @if ($errors->has('password_confirmation'))
                                <span class="help is-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </p>

                        <div class="control is-grouped">
                            <p class="control">
                                <button type="submit" class="button is-primary">Reset Password</button>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
