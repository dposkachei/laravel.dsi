@extends('bulma.layouts.'.$sComposerLayout)

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <div class="box">
                <center>
                    {!! $chart->render() !!}
                </center>
            </div>
        </div>
    </section>
@endsection
