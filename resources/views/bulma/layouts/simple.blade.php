<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>{{ MetaTag::get('title') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
</head>
<body class="{{ $sBodyClass or '' }}" style="background-color: #ffffff;">

<div id="app" style="">
    <!-- NAVIGATION -->
    @include('bulma.components.navigation.admin.simple')

    @yield('content')

</div>

@include('bulma.components.dialogs.app')

<!-- MODALS -->
@yield('modals')

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/toastr.js') }}"></script>

@stack('scripts')

<script src="{{ asset('js/admin.js') }}"></script>
<script src="{{ asset('js/push.min.js') }}"></script>
<!-- Push notification -->
<push></push>


@include('bulma.components.toastr')

        <!-- API -->
@include('bulma.components.api.yandex.metrika')
</body>
</html>
