<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>{{ $oComposerSite->app->title }}</title>
    <meta name="description" content="{{ $oComposerSite->app->description }}">
    <meta name="keywords" content="{{ $oComposerSite->app->keywords }}">
    <link rel="shortcut icon" href="{{ $oComposerSite->app->favicon }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}?v=0.0.8">

    @if(!Sentinel::guest())
        <script>
            window.user = {!! json_encode([
                'id' => Sentinel::getUser()->id
            ]) !!};
        </script>
    @endif

    @stack('styles')

    @include('bulma.components.svg')
</head>
<body class="{{ $sBodyClass or '' }}">

    <chart-assets></chart-assets>

    <div id="app" style="min-height: 100vh;">

        <!-- NAVIGATION -->
        @include('bulma.components.navigation.admin.navigation')

        <section id="section" class="section" style="padding-top: 1rem;{{-- background-image: url('{{ asset('/imagecache/original/sky.jpg') }}'); background-size: 100%;background-position: 0px -138px;background-repeat: no-repeat;--}}">
            <div class="columns" style="min-height: calc(100vh - 84px);padding-bottom: 1rem;background-color: #fff;">
                @if(!Sentinel::guest())
                    @include('bulma.components.navigation.admin.sidebar')
                @endif
                <div class="column" id="admin-content" >

                    @include('bulma.components.loaders.square')

                    @yield('content.title')

                    @yield('content')

                </div>
            </div>
        </section>

        @include('bulma.components.navigation.admin.footer')

    </div>

    @include('bulma.components.dialogs.app')

    <!-- MODALS -->
    @yield('modals')

    {{--{{ Sam::pushCss(asset('css/app.css'), true); }}--}}
    <script>
        var jsVars = {!! $jsVars !!};
        console.log(jsVars);
        console.log(jsVars['success']);
    </script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/toastr.js') }}"></script>

    <script src="{{ asset('js/admin.js') }}?v=0.0.8"></script>

    @stack('scripts')

    <script src="{{ asset('js/push.min.js') }}"></script>
    <!-- Push notification -->
    <push></push>


    @include('bulma.components.toastr')

</body>
</html>
