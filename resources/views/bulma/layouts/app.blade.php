<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Bulma Mix</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Laravel & Angular & Bulma">
    <meta property="og:title" content="Laravel & Angular & Bulma">
    <meta property="og:description" content="Laravel & Angular & Bulma">
    <meta property="og:image" content="">
    <meta property="og:url" content="http://dposkachei.github.io/">
    <meta property="og:site_name" content="dposkachei.github.io">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">


    {{--{{ Sam::pushCss(asset('css/app.css'), true); }}--}}
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/toastr.js') }}"></script>

    @stack('styles')

    @include('bulma.components.svg')
</head>
<body style="background-color: #ffffff;">

    <chart-assets></chart-assets>

    <div id="app">
        <!-- NAVIGATION -->
        @include('bulma.components.navigation.app.navigation')

        @yield('content')
    </div>

    <specific>

    </specific>

    @include('bulma.components.dialogs.app')

    <!-- Scripts -->
    @yield('modals')



    @stack('scripts')

    <script src="{{ asset('js/admin.js') }}"></script>





    <!-- Push notification -->
    <push></push>


    @include('bulma.components.toastr')
</body>
</html>
