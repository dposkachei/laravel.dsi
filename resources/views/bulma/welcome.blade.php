@extends('bulma.layouts.'.$sComposerLayout)

@section('content')
    <div class="laravel-container">
        <div class="laravel-title">
            {{ $oComposerSite->app->title }}
        </div>
    </div>
    <section class="section" style="display: none;"></section>
@endsection

@section('footer')
    <section class="section" style="display: none;"></section>
@endsection