@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Фансибокс'
    ])
@endsection

@section('content')
    <section class="section">
        <div class="container">
            @for ($i = 0; $i < 10; $i++)
                <div class="columns">
                    <div class="column">
                        <a class="fancybox" rel="gallery1" href="http://laravel.mix.loc/imagecache/original/cover.png">
                            <img data-original="http://laravel.mix.loc/imagecache/original/cover.png"
                                 {{--class="lazy"--}}
                                 {{--src="http://laravel.mix.loc/imagecache/square/cover.png"--}}
                                 src="http://laravel.mix.loc/imagecache/original/cover.png"
                                 data-normal="http://laravel.mix.loc/imagecache/square/cover.png"
                                 data-retina="http://laravel.mix.loc/imagecache/original/cover.png"
                                 data-srcset="http://laravel.mix.loc/imagecache/original/cover.png 1024w"
                                 height="550" width="1360">
                        </a>
                    </div>
                    <div class="column">
                        <a class="fancybox" rel="gallery1" href="http://laravel.mix.loc/imagecache/original/cover2.png">
                            <img data-original="http://laravel.mix.loc/imagecache/original/cover2.png"
                                 {{--class="lazy"--}}
                                 {{--src="http://laravel.mix.loc/imagecache/square/cover2.png"--}}
                                 src="http://laravel.mix.loc/imagecache/original/cover2.png"
                                 height="550" width="1360" alt="Фотография услуги">
                        </a>
                    </div>
                    <div class="column">
                        <a class="fancybox" rel="gallery1" href="http://laravel.mix.loc/imagecache/original/cover1.png">
                            <img data-original="http://laravel.mix.loc/imagecache/original/cover1.png"
                                 {{--class="lazy"--}}
                                 {{--src="http://laravel.mix.loc/imagecache/square/cover1.png"--}}
                                 src="http://laravel.mix.loc/imagecache/original/cover1.png"
                                 height="550" width="1360" alt="Фотография услуги">
                        </a>
                    </div>
                    <div class="column">
                        <a class="fancybox" rel="gallery1" href="http://laravel.mix.loc/imagecache/original/previews/1.jpg">
                            <img data-original="http://laravel.mix.loc/imagecache/original/previews/1.jpg"
                                 {{--class="lazy"--}}
                                 {{--src="http://laravel.mix.loc/imagecache/square/previews/1.jpg"--}}
                                 src="http://laravel.mix.loc/imagecache/original/previews/1.jpg"
                                 height="550" width="1360" alt="Фотография услуги">
                        </a>
                    </div>
                </div>
            @endfor
        </div>
    </section>
@endsection
