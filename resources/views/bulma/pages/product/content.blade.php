@extends('bulma.layouts.'.$sComposerLayout)

@push('styles')

<link rel="stylesheet" href="{{ asset('assets/masterslider/masterslider.main.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/masterslider/skins/ms-skin-sample.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('assets/masterslider/masterslider.min.js') }}"></script>
@endpush

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Название продукта'
    ])
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <span class="title is-3">Product Name</span>
                    <span class="title is-3 has-text-muted">&nbsp;|&nbsp;</span>
                    <span class="title is-4 has-text-muted">Category</span>
                </div>
            </div>
            <hr>
            <div class="columns">
                <div class="column is-6">
                    <!-- masterslider -->
                    <div class="master-slider ms-skin-default" id="masterslider">
                        <!-- new slide -->
                        <div class="ms-slide">

                            <!-- slide background -->
                            <img src="{{ asset('assets/masterslider/blank.gif') }}" data-src="http://laravel.mix.loc/imagecache/original/cover.png" alt="lorem ipsum dolor sit"/>

                            <!-- slide text layer -->
                            <div class="ms-layer ms-caption" style="top:10px; left:30px;">
                                Lorem ipsum dolor sit amet
                            </div>

                        </div>
                        <!-- end of slide -->

                        <!-- new slide -->
                        <div class="ms-slide">

                            <!-- slide background -->
                            <img src="{{ asset('assets/masterslider/blank.gif') }}" data-src="http://laravel.mix.loc/imagecache/original/cover1.png" alt="lorem ipsum dolor sit"/>

                            <!-- slide text layer -->
                            <div class="ms-layer ms-caption" style="top:10px; left:30px;">
                                Lorem ipsum dolor sit amet
                            </div>

                            <!-- linked slide -->
                            <a href="http://codecanyon.net/user/averta">Averta</a>

                        </div>
                        <!-- end of slide -->

                        <!-- new slide -->
                        <div class="ms-slide">

                            <!-- slide background -->
                            <img src="{{ asset('assets/masterslider/blank.gif') }}" data-src="http://laravel.mix.loc/imagecache/original/cover2.png" alt="lorem ipsum dolor sit"/>

                            <!-- slide text layer -->
                            <div class="ms-layer ms-caption" style="top:10px; left:30px;">
                                Lorem ipsum dolor sit amet
                            </div>

                            <!-- youtube video -->
                            <a href="http://www.youtube.com/embed/YHWkro9-e9Q?hd=1&wmode=opaque&controls=1&showinfo=0" data-type="video">Youtube video</a>

                        </div>
                        <!-- end of slide -->

                    </div>
                    <!-- end of masterslider -->
                    {{--
                    <figure class="image is-square is-center">
                        <img src="http://bulma.io/images/placeholders/480x480.png">
                    </figure>
                    --}}
                </div>
                <div class="column is-6 ">
                    <div class="title is-2">Item Title</div>
                    <p class="title is-3 has-text-muted">$ 39.95</p>
                    <hr>
                    <br>
                    <p class="stars">
                        <i class="fa fa-star title is-5" style="color:#ed6c63"></i>
                        <i class="fa fa-star title is-5" style="color:#ed6c63"></i>
                        <i class="fa fa-star title is-5" style="color:#ed6c63"></i>
                        <i class="fa fa-star title is-5"></i>
                        <i class="fa fa-star title is-5"></i>
                        &nbsp; &nbsp;
                        <strong style="color: #95A5A6;">41 просмотров</strong>
                        &nbsp; &nbsp;
                        <a href="#">show all</a>
                    </p>
                    <br>
                    <p>
                        Suspendisse sodales metus justo, ullamcorper iaculis purus interdum in. Sed ultricies enim felis, in interdum urna malesuada a. Morbi id ligula vel leo elementum dignissim quis vel purus. Donec iaculis, est ac maximus vestibulum, sapien mi lacinia urna, at laoreet felis lectus nec urna. Fusce egestas, neque vitae blandit scelerisque, leo arcu pellentesque risus, et volutpat neque nunc id massa. Aenean dapibus leo vel purus malesuada, eu ultrices nulla consequat. Duis erat orci, lobortis sed dictum id, pretium a nibh. Mauris pharetra ligula consequat gravida ornare.
                    </p>
                    <br>
                    <br>
                    <p class="control">
                        <a class="button is-white">
                            <span class="icon is-small" style="height: .9rem;">
                                <i class="fa fa-minus"></i>
                            </span>
                        </a>
                        <input type="text" name="" class="input has-text-centered" value="1" data-role="js-mask-int" style="width:40px;">
                        <a class="button is-white">
                            <span class="icon is-small" style="height: .9rem;">
                                <i class="fa fa-plus"></i>
                            </span>
                        </a>
                        &nbsp; &nbsp; &nbsp;
                        <a class="button is-primary">Добавить в корзину</a>
                    </p>
                    <br>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td class="has-text-right">
                                <strong>Item ID</strong>
                            </td>
                            <td>1234</td>
                        </tr>
                        <tr>
                            <td class="has-text-right">
                                <strong>Seller</strong>
                            </td>
                            <td>jsmith</td>
                        </tr>
                        <tr>
                            <td class="has-text-right">
                                <strong>Added</strong>
                            </td>
                            <td>3 days ago</td>
                        </tr>
                        <tr>
                            <td class="has-text-right">
                                <strong>Views</strong>
                            </td>
                            <td>3</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <div class="tabs ajax-tabs">
                <ul>
                    <li class="is-active">
                        <a class="ajax-link" action="{{ url('tab/info') }}" data-tab="info" data-container=".tab-container">Информация</a>
                    </li>
                    <li>
                        <a class="ajax-link" action="{{ url('tab/detail') }}" data-tab="detail" data-container=".tab-container">Детали</a>
                    </li>
                    <li>
                        <a class="ajax-link" action="{{ url('tab/feedback') }}" data-tab="feedback" data-container=".tab-container">Отзывы</a>
                    </li>
                </ul>
            </div>
            <div class="tab-container">
                @include('bulma.pages.product.components.tabs.info')
            </div>
        </div>
    </section>
@endsection