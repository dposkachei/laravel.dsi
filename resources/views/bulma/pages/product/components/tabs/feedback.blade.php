<article class="media">
    <figure class="media-left">
        <p class="image is-64x64">
            <img src="http://placehold.it/128x128">
        </p>
    </figure>
    <div class="media-content">
        <div class="content">
            <p>
                <strong>Barbara Middleton</strong><small> · 3 hrs</small>
                <br>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros lacus, nec ultricies elit blandit non. Suspendisse pellentesque mauris sit amet dolor blandit rutrum. Nunc in tempus turpis.
                <br>
            </p>
        </div>
        <div class="level" style="margin-bottom: 5px;">
            <div class="level-left">
                <a class="level-item">
                    <span class="icon is-small"><i class="fa fa-reply"></i></span>
                </a>
                <a class="level-item">
                    <span class="icon is-small"><i class="fa fa-heart"></i></span>
                </a>
            </div>
        </div>
        <article class="media">
            <figure class="media-left">
                <p class="image is-48x48">
                    <img src="http://placehold.it/96x96">
                </p>
            </figure>
            <div class="media-content">
                <div class="content">
                    <p>
                        <strong>Sean Brown</strong><small> · 2 hrs</small>
                        <br>
                        Donec sollicitudin urna eget eros malesuada sagittis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam blandit nisl a nulla sagittis, a lobortis leo feugiat.
                    </p>
                </div>
                <div class="level" style="margin-bottom: 5px;">
                    <div class="level-left">
                        <a class="level-item">
                            <span class="icon is-small"><i class="fa fa-reply"></i></span>
                        </a>
                        <a class="level-item">
                            <span class="icon is-small"><i class="fa fa-heart"></i></span>
                        </a>
                    </div>
                </div>
                <article class="media">
                    Vivamus quis semper metus, non tincidunt dolor. Vivamus in mi eu lorem cursus ullamcorper sit amet nec massa.
                </article>
                <article class="media">
                    Morbi vitae diam et purus tincidunt porttitor vel vitae augue. Praesent malesuada metus sed pharetra euismod. Cras tellus odio, tincidunt iaculis diam non, porta aliquet tortor.
                </article>
            </div>
        </article>
        <article class="media">
            <figure class="media-left">
                <p class="image is-48x48">
                    <img src="http://placehold.it/96x96">
                </p>
            </figure>
            <div class="media-content">
                <div class="content">
                    <p>
                        <strong>Kayli Eunice </strong>
                        <br>
                        Sed convallis scelerisque mauris, non pulvinar nunc mattis vel. Maecenas varius felis sit amet magna vestibulum euismod malesuada cursus libero. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus lacinia non nisl id feugiat.
                    </p>
                </div>
                <div class="level" style="margin-bottom: 5px;">
                    <div class="level-left">
                        <a class="level-item">
                            <span class="icon is-small"><i class="fa fa-reply"></i></span>
                        </a>
                        <a class="level-item">
                            <span class="icon is-small"><i class="fa fa-heart"></i></span>
                        </a>
                    </div>
                </div>
                <article class="media">
                    <figure class="media-left">
                        <p class="image is-48x48">
                            <img src="http://placehold.it/96x96">
                        </p>
                    </figure>
                    <div class="media-content">
                        <div class="content">
                            <p>
                                <strong>Sean Brown</strong><small> · 2 hrs</small>
                                <br>
                                Donec sollicitudin urna eget eros malesuada sagittis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam blandit nisl a nulla sagittis, a lobortis leo feugiat.
                                <br>
                            </p>
                        </div>
                        <div class="level" style="margin-bottom: 5px;">
                            <div class="level-left">
                                <a class="level-item">
                                    <span class="icon is-small"><i class="fa fa-reply"></i></span>
                                </a>
                                <a class="level-item">
                                    <span class="icon is-small"><i class="fa fa-heart"></i></span>
                                </a>
                            </div>
                        </div>
                        <article class="media">
                            Vivamus quis semper metus, non tincidunt dolor. Vivamus in mi eu lorem cursus ullamcorper sit amet nec massa.
                        </article>
                        <article class="media">
                            Morbi vitae diam et purus tincidunt porttitor vel vitae augue. Praesent malesuada metus sed pharetra euismod. Cras tellus odio, tincidunt iaculis diam non, porta aliquet tortor.
                        </article>
                    </div>
                </article>
            </div>
        </article>
    </div>
</article>