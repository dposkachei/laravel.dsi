@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Пагинация'
    ])
@endsection

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container table-component-pagination">
            @include('bulma.components.pagination.table', [
                'oUsers' => $oUsers
            ])
        </div>
    </section>
    <section class="section">
        <div class="container">
            @include('bulma.pages.pagination.readme')
        </div>
    </section>
@endsection