Нужен контейнер для таблице <code>.table-component-pagination</code>, в котором отдельный шаблон.<br>
Дальше он будет ajax'ом подгрузаться в этот контейнер.<br><br>
В методе нужны одни и те же настройки при выборке <code>->paginate($count)->withPath($url)</code>.<br>
Метод в <code>$url</code> должен быть POST и возвращать <code>array['view' => view('отдельный шаблон с таблицей')->render()]</code>