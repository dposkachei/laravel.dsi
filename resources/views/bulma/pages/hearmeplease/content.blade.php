@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Hearmeplease style'
    ])
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column">
                    @for($i = 0; $i < 1; $i++)
                        @include('bulma.pages.hearmeplease.components.block')
                    @endfor
                </div>
                <div class="column is-5">
                    <p class="control has-icon">
                        <input class="input" type="text" placeholder="Search" >
                        <span class="icon is-small">
                            <i class="fa fa-search"></i>
                        </span>
                    </p>
                    <ul class="section table-of-contents">
                        <li><a class="button" href="#introduction">Introduction</a></li>
                        <li><a class="button" href="#structure">Structure</a></li>
                        <li><a class="button" href="#initialization">Intialization</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
