@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Копирование текста'
    ])
@endsection

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <p class="control has-addons">
                <input class="input" type="text" id="foo" value="https://github.com/zenorocha/clipboard.js.git">
                <a class="button btn-clipboard" data-clipboard-target="#foo">
                    <span class="icon is-small">
                        <i class="fa fa-clipboard"></i>
                    </span>
                </a>
            </p>
            <p class="control">
                <textarea class="textarea" id="bar">Mussum ipsum cacilds...</textarea>
            </p>
            <p class="control">
                <a class="button btn-clipboard" data-clipboard-action="cut" data-clipboard-target="#bar">
                    <span class="icon is-small">
                        <i class="fa fa-clipboard"></i>
                    </span>
                </a>
            </p>
            <p class="control">
                <a class="button btn-clipboard" data-clipboard-text="Just because you can doesn't mean you should — clipboard.js">
                    <span class="icon is-small">
                        <i class="fa fa-clipboard"></i>
                    </span>
                </a>
            </p>
        </div>
    </section>
@endsection
