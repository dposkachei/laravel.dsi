<nav class="level">
    <!-- Left side -->
    <div class="level-left">
        @if(isset($bCount) && $bCount)
            <div class="level-item">
                <p class="subtitle is-5">
                    <strong>{{ $nCount }}</strong> {{ $sCountText }}
                </p>
            </div>
        @endif
        @if(isset($bSearch) && $bSearch)
            <div class="level-item">
                <form class="ajax-form" role="form" method="POST" action="{{ $sSearchAction }}"
                      data-callback=""
                >
                    <p class="control has-addons">
                        <input class="input" type="text" placeholder="Find a post">
                        <button class="button inner-form-submit" type="submit">
                            Search
                        </button>
                    </p>
                </form>
            </div>
        @endif
    </div>

    <!-- Right side -->
    <div class="level-right">
        <p class="level-item"><strong>All</strong></p>
        <p class="level-item"><a>Published</a></p>
        <p class="level-item"><a>Drafts</a></p>
        <p class="level-item"><a>Deleted</a></p>
        <p class="level-item"><a class="button is-dark">New</a></p>
    </div>
</nav>