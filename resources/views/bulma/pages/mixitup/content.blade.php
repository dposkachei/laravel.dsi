@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Сортировка блоков'
    ])
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <p class="control">
                        <button type="button" class="button" data-filter="all">All</button>
                        <button type="button" class="button" data-filter=".category-a">Category A</button>
                        <button type="button" class="button" data-filter=".category-b">Category B</button>
                        <button type="button" class="button" data-filter=".category-c">Category C</button>
                    </p>
                </div>
            </div>
            <div class="mixitup" style="">
                <p class="control mix category-a" data-order="1">
                    <input class="input" type="text" placeholder="Primary input">
                </p>
                <p class="control mix category-b" data-order="2">
                    <input class="input" type="text" placeholder="Info input">
                </p>
                <p class="control mix category-b category-c" data-order="3">
                    <input class="input" type="text" placeholder="Success input">
                </p>
                <p class="control mix category-b category-c" data-order="3">
                    <input class="input" type="text" placeholder="Warning input">
                </p>
                <p class="control mix category-a category-d" data-order="4">
                    <input class="input" type="text" placeholder="Danger input">
                </p>
            </div>
            <style>
                .mixitup {
                    width: 200px;
                }
                .mixitup-button-is-active {
                    background-color: #00bcd4;
                }
            </style>

        </div>
    </section>
@endsection
