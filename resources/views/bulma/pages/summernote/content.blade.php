@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Summernote'
    ])
@endsection

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <textarea name="" cols="30" rows="10" class="froala">
            	<p>Preview the HTML code of the WYSIWYG HTML editor as you type in the box below.</p>
            </textarea>
        </div>	
    </section>
    <section class="section">
    	<div class="container">
    		<div id="preview" class="fr-view">
			  <p>Preview the HTML code of the WYSIWYG HTML editor as you type in the box below.</p>
			</div>
    	</div>
    </section>
@endsection
