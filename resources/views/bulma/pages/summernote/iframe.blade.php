<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Bulma Mix</title>

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <link href="{{ asset('assets/summernote/summernote.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/summernote/summernote.js') }}"></script>
    <script src="{{ asset('assets/summernote/lang/summernote-ru-RU.js') }}"></script>
    <style>
        #summernote + .note-editor {
            margin-bottom: -1px;
        }
    </style>
</head>
<body>
<section class="section">
    <div class="container" style="padding: 0;">
        <textarea id="summernote" class="textarea"></textarea>
    </div>
</section>


<script>
    $(function() {
        $('#summernote').summernote({
            lang: 'ru-RU',
            height: 250,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: true ,                // set focus to editable area after initializing summernote
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['insert', ['link', 'hr']],
                //['color', ['color']],
                //['height', ['height']],
                ['view', [
                    //'fullscreen',
                    'codeview'
                ]],
                ['para', ['ul', 'ol', 'paragraph']]
            ],
            callbacks: {
                onInit: function() {
                    var parent = window.parent.document.getElementById('parent');
                    if (parent) {
                        $('#summernote').summernote('code', parent.value);
                    }
                },
                onChange: function(contents, $editable) {
                    var parent = window.parent.document.getElementById('parent');
                    if (parent) {
                        parent.value = $('#summernote').val();
                    }
                    var visual = window.parent.document.getElementById('textblock');
                    if (visual) {
                        visual.innerHTML = $('#summernote').val();
                    }
                }
            },
            disableResizeEditor: true
        });
    });

</script>
</body>
</html>
