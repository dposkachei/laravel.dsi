@include('bulma.components.pages.title', [
    'title' => 'Загрузчик'
])
<section class="section {{-- is-medium --}}">
    <div class="container">
        <input type="file" name="file" id="file" class="is-file" data-url="/image/upload" multiple>
        <label for="file" class="button label-file">
                <span class="icon is-small">
                    <i class="fa fa-upload"></i>
                </span>
            <span class="label-file-name">Выберите файл</span>
        </label>
    </div>
</section>