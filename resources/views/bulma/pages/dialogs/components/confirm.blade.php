<div id="pages-dialogs-confirm" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content is-vcentered box" style="max-width: 400px;margin-left: 10px; margin-right: 10px;">
        {{--
        <h3 class="title">
            Подтвердите
        </h3>
        --}}

        <h5 class="subtitle">
            Действительно?
        </h5>
        <form class="ajax-form" role="form" method="POST" action="{{ route('confirm') }}"
              data-counter=".admin-table-counter"
              data-list=".admin-table"
              data-list-action=""
              data-form-data=".pagination-form"
              data-callback="closeModalAfterSubmit, refreshAfterSubmit"
        >
            <p class="control" style="text-align: center;">
                <button class="button is-primary inner-form-submit with-loading" type="submit">
                    Да
                </button>
                <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-confirm">
                    Нет
                </button>
            </p>
        </form>
        <div style="position: absolute; top: 10px; right: 10px;">
            <a class="dialog__close icon" data-dialog="#pages-dialogs-confirm">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
</div>