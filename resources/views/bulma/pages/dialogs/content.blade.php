@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Диалоговые окна'
    ])
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <p class="control">
                <a class="button trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route('loauth') }}" data-id="123">Ajax</a>
            </p>
            <p class="control">
                <a class="button trigger" data-dialog="#pages-dialogs-confirm">Confirm</a>
            </p>
            <p class="control">
                <a class="button trigger" data-dialog="#pages-dialogs-form">Form</a>
            </p>
            <p class="control">
                <a class="button trigger" data-dialog="#pages-dialogs-info" data-disabled-overlay>Info</a>
            </p>
        </div>
    </section>
@endsection

@section('modals')
    @include('bulma.pages.dialogs.components.ajax')
    @include('bulma.pages.dialogs.components.confirm')
    @include('bulma.pages.dialogs.components.form')
    @include('bulma.pages.dialogs.components.info')
@endsection