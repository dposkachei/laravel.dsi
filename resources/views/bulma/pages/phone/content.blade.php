@extends('bulma.layouts.'.$sComposerLayout)

@section('content.title')
    @include('bulma.components.pages.title', [
        'title' => 'Телефон'
    ])
@endsection

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <div class="columns">
                <div class="column is-3">
                    <p class="control has-icon" style="width: 190px;">
                        <input class="input" name="phone" type="text" data-role="js-mask-phone" placeholder="(xxx) xxx xx xx">
                        <span class="icon is-small">
                            <i class="fa fa-phone"></i>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('js/cleave.min.js') }}"></script>
    <script>

    </script>
@endsection
