@extends('email.layout')

@section('content')
    @if($oOrder->isCall())
        <p>Запрошен обратный звонок.</p>
    @else
        <p>Оформлен заказ №{{ $oOrder->id }}.</p>
    @endif

    @if(!is_null($oOrder->name))<p>Имя: {{ $oOrder->name }}</p>@endif
    @if(!is_null($oOrder->phone))<p>Телефон: {{ $oOrder->phone }}</p>@endif
    @if(!is_null($oOrder->email))<p>Email: {{ $oOrder->email }}</p>@endif

    @if($oOrder->isCall())

    @else
        <p>Список продукций: <br>
            @foreach($oOrder->products as $oProduct)
                <a href="{{ route('index.product', ['id' => $oProduct->id, 'slug' => $oProduct->url]) }}" target="_blank" title="Перейти на страницу">
                    {{ $oProduct->title }}
                </a>
                <br>
            @endforeach
        </p>
    @endif
@endsection
