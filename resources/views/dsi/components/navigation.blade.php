<div class="menu-top-line" style="height: 40px;">
    <div class="top-line-wrp" style="height: 40px;">
        <ul class="top-line">
            <li style="height: 40px;"><a href="{{ route('index.welcome') }}" style="border: none;">Главная</a></li>
            <li class="dropdown" style="height: 40px;">
                <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Категории Продукций
                </a>
                <ul class="dropdown-menu">
                    @foreach($oComposerCategories->where('parent_id', null)->where('status', 1)->sortByDesc('priority') as $oComposerCategory)
                        <li><a href="{{ route('index.category', ['id' => $oComposerCategory->id, 'slug' => $oComposerCategory->url]) }}">{{ $oComposerCategory->title }}</a></li>
                    @endforeach
                </ul>
            </li>
            <li style="height: 40px;"><a href="{{ route('index.products') }}" style="border: none;">Вся продукция</a></li>
            <li style="height: 40px;"><a href="{{ route('index.partners') }}" style="border: none;">Партнеры</a></li>
            <li style="height: 40px;"><a href="{{ route('index.certificates') }}" style="border: none;">Сертификаты</a></li>
            <li style="height: 40px;"><a href="{{ route('index.contacts') }}" style="border: none;">Контакты</a></li>
        </ul>
    </div>
</div>