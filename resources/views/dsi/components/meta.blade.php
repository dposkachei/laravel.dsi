@if(isset($metaController) && $metaController)
    <title>{{ MetaTag::get('title') }}</title>
    <meta name="description" content="{{ MetaTag::get('description') }}">
    <meta name="keywords" content="{{ MetaTag::get('keywords') }}">
    {!! MetaTag::tag('image') !!}
@else
    <title>{{ $oComposerSite->app->title }}</title>
    <meta name="description" content="{{ $oComposerSite->app->description }}">
    <meta name="keywords" content="{{ $oComposerSite->app->keywords }}">
@endif
<link rel="shortcut icon" href="{{ $oComposerSite->app->favicon }}">