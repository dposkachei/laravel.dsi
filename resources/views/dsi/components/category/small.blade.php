<article>
    {{--
    <div class="img-cont">
        <a href="{{ route('index.category', ['id' => $oCategory->id, 'slug' => $oCategory->url]) }}" class="image">
            <img src="{{ ImagePath::main('category', 'block', $oCategory) }}" alt="" />
        </a>
    </div>
    --}}
    <div class="desc-cont" style="margin-bottom: 0;">
        <h3>
            {{ $oCategory->title }}
        </h3>
        <p style="margin-bottom: 0;">
            {{ str_limit(strip_tags($oCategory->text), 160) }}
        </p>
    </div>
    <ul class="actions">
        <li>
            <a href="{{ route('index.category', ['id' => $oCategory->id, 'slug' => $oCategory->url]) }}" class="button">
                Подробнее
            </a>
        </li>
    </ul>
</article>