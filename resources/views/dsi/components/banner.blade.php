<!-- Banner -->
<section id="banner">
    <div class="content">
        <header>
            <h2>{{ $oPage->title }}</h2>
            <!--<p>A free and fully responsive site template</p>-->
        </header>
        <div rel="truncate2" data-name="dot-{{$oPage->name}}">
            {!! $oPage->text !!}
        </div>
        @if(isset($actions) && $actions)
            <ul class="actions">
                <li>
                    <a type="button" class="button special" rel="truncate-button" data-name="dot-{{$oPage->name}}">Подробнее</a>
                </li>
            </ul>
        @endif
    </div>
    @if(ImagePath::checkMain('page', 'original', $oPage))
        <span class="image object" style="height: 300px">
            <img src="{{ ImagePath::main('page', 'original', $oPage) }}" alt="" />
        </span>
    @endif
</section>