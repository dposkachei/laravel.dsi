<!-- Sidebar -->
<div id="sidebar" class="inactive">
    <div class="inner">

        <!-- Menu -->
        <nav id="menu">
            <div class="menu-cont">
                <span>
                    Производство и поставка дорожностроительных материалов по всей России
                </span>
            </div>
            <header class="major">
                <h2>Меню</h2>
            </header>
            <ul>
                <li><a href="{{ route('index.welcome') }}">Главная</a></li>
                <li>
                    <span class="opener">Категории Продукций</span>
                    <ul>
                        @foreach($oComposerCategories->where('parent_id', null)->where('status', 1)->sortByDesc('priority') as $oComposerCategory)
                            <li><a href="{{ route('index.category', ['id' => $oComposerCategory->id, 'slug' => $oComposerCategory->url]) }}">{{ $oComposerCategory->title }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li><a href="{{ route('index.products') }}">Вся продукция</a></li>
                <li><a href="{{ route('index.partners') }}">Партнеры</a></li>
                <li><a href="{{ route('index.certificates') }}">Сертификаты</a></li>
                <li><a href="{{ route('index.contacts') }}">Контакты</a></li>
            </ul>
        </nav>
        <section class="alt">
            <form class="search" method="get" action="{{ route('index.search') }}" style="width: 100%;">
                <input type="text" name="q" id="query" placeholder="Поиск" style="width: 70%;display: inline-block;"/>
                <button type="submit" class="btn __search" style="width: 25%;margin-bottom: 4px;background: #fff;display: inline-block;margin-left: 10px;padding: 10px;height: 40px;border: 1px solid rgba(127,136,143,1);box-shadow: none;">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
            </form>
            {{--
            <form method="post" action="#">
                <input type="text" name="query" id="query" placeholder="Поиск" />
            </form>

            <div class="find-bt">
                <a href="#" class="button special __search">Найти</a>
            </div>
            --}}
        </section>
        <!-- Section -->
        <!--<section>
            <header class="major">
                <h2>Ante interdum</h2>
            </header>
            <div class="mini-posts">
                <article>
                    <a href="#" class="image"><img src="images/pic07.jpg" alt="" /></a>
                    <p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore aliquam.</p>
                </article>
                <article>
                    <a href="#" class="image"><img src="images/pic08.jpg" alt="" /></a>
                    <p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore aliquam.</p>
                </article>
                <article>
                    <a href="#" class="image"><img src="images/pic09.jpg" alt="" /></a>
                    <p>Aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore aliquam.</p>
                </article>
            </div>
            <ul class="actions">
                <li><a href="#" class="button">More</a></li>
            </ul>
        </section>-->

        <!-- Section -->
        <section>
            <header class="major">
                <h2>Наши контакты</h2>
            </header>
            <!--<p>Текст</p>-->
            <ul class="contact">
                @if(!is_null($oComposerSite->contacts->email))
                    <li class="fa-envelope-o">
                        <a href="mailto:{{ $oComposerSite->contacts->email }}">{{ $oComposerSite->contacts->email }}</a>
                    </li>
                @endif
                @if(!is_null($oComposerSite->contacts->phone))
                    <li class="fa-phone">
                        {{ $oComposerSite->contacts->phone }}
                    </li>
                @endif
                @if(!is_null($oComposerSite->contacts->address))
                    <li class="fa-home">
                        {{ $oComposerSite->contacts->address }}
                    </li>
                @endif
            </ul>
        </section>

        <!-- Footer -->
        <footer id="footer">
            <p class="copyright">{{ $oComposerSite->app->copyright }}</p>
        </footer>

    </div>
</div>