@if($oProducts instanceof \Illuminate\Pagination\LengthAwarePaginator)
    @if(isset($aSearch))
        {{ $oProducts->appends($aSearch)->links('dsi.components.product.pagination') }}
    @else
        {{ $oProducts->links('dsi.components.product.pagination') }}
    @endif
@endif