<article>
    <div class="img-cont">
        <a href="{{ route('index.product', ['id' => $oProduct->id, 'slug' => $oProduct->url]) }}" class="image">
            <img src="{{ ImagePath::main('product', 'block', $oProduct) }}" alt="" />
        </a>
    </div>
    <div class="desc-cont" style="margin-bottom: 0;">
        <h3>{{ $oProduct->title }}</h3>
        <p style="margin-bottom: 0;">
            {{ str_limit(strip_tags($oProduct->text), 160) }}
        </p>
    </div>
    <ul class="actions">
        <li>
            <a href="{{ route('index.product', ['id' => $oProduct->id, 'slug' => $oProduct->url]) }}" class="button">
                Подробнее
            </a>
        </li>
    </ul>
</article>