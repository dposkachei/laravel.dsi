@if ($paginator->hasMorePages())
    <div class="more-bt" style="width: 100%;">
        <button class="button special ajax-link"
                action="{{ $paginator->nextPageUrl() }}"
                data-pagination="1"
                data-append="1"
                data-form-query=".pagination-form"
                data-pagination-view-list-button="1"
                data-pagination-button-container=".table-component-pagination-button"
                data-pagination-container=".table-component-pagination"
        >
            Загрузить ещё
        </button>
        <form class="pagination-form">
            <input type="hidden" name="page" value="{{ $paginator->currentPage() }}">
            @if(isset($aSearch) && !empty($aSearch))
                @foreach($aSearch as $key => $value)
                    <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                @endforeach
            @endif
        </form>
    </div>
@endif
