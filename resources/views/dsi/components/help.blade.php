<div class="menu-top-btns">
    <div class="top-line-section">
        <div class="cart-n-search">
            <div class="cart">
                <a href="#getBasket" data-toggle="modal" data-action="{{ route('index.basket.modal.post') }}">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    @if($nBasketCount !== 0)
                        <span>
                            Корзина <span class="basket-count">({{$nBasketCount}})</span>
                        </span>
                    @else
                        <span>
                            Корзина <span class="basket-count"></span>
                        </span>
                    @endif
                </a>
            </div>
            <form class="search" method="get" action="{{ route('index.search') }}" style="display: inherit">
                <input type="text" name="q" id="query" placeholder="Поиск" />
                <button type="submit" class="btn __search find-bt" style="height: 36px;box-shadow: none;">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
            </form>
        </div>
        <div class="quick-bts">

            @foreach($oComposerArticles->where('status', 1) as $oArticle)
                <div class="top-btn-{{$oArticle->icon}} item">
                    <div class="item-wrp">
                        <span class="icon fa-{{$oArticle->icon}}"></span>
                        <a href="#article-{{$oArticle->id}}" data-toggle="modal">{{ $oArticle->title }}</a>
                    </div>
                </div>
            @endforeach
            {{--
            @foreach($oComposerArticles->where('status', 1) as $oArticle)
                <div class="top-btn-{{$oArticle->icon}} item">
                    <div class="item-wrp">
                        <span class="icon fa-{{$oArticle->icon}}" style="color: #353535;"></span>
                        <a href="#article-{{$oArticle->id}}" data-toggle="modal" style="color: #6CA6D5;font-weight: bold;">{{ $oArticle->title }}</a>
                    </div>
                </div>
            @endforeach
            --}}

            <div class="top-btn-call item">
                <div class="item-wrp">
                    <a href="#getCall" data-toggle="modal">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>