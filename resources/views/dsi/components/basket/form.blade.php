<form class="ajax-form" role="form" method="POST" action="{{ route('order.store') }}"
      data-modal="#getCall"
      data-quote-counter=".basket-count"
      data-before="openModalBodyBeforeSubmit"
      data-callback="showBodyMessageAfterSubmit, updateBasketCountAfterSubmit"
      style="margin: 0;"
>
    <input type="hidden" name="type" value="1">
    <!-- Заголовок модального окна -->
    <div class="modal-header">
        <h4 class="modal-title">Корзина</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <!-- Основное содержимое модального окна -->
    <div class="modal-body" style="padding: 0;">
        @if(!is_null($oItems) && !empty($oItems[0]))
            <table class="table" style="margin: 0; max-width: 100%;">
                <thead>
                <tr>
                    <th style="width:50px">#</th>
                    <th>Название</th>
                    <th style="width:50px">Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach($oItems as $oItem)
                    <tr>
                        <th style="width:50px">{{ $oItem->id }}</th>
                        <th>{{ $oItem->title }}</th>
                        <th style="width:50px; text-align: center;padding-bottom: .25rem;padding-top: .25rem;">
                            <button class="ajax-link icon fa-trash" action="{{ route('index.basket.destroy.post', ['id' => $oItem->id]) }}"
                                    style="font-size: 18px;cursor: pointer;padding: 10px 0px 10px 10px;height: 32px;line-height: 10px;"
                                    {{--data-before="clearModalBeforeSubmit"--}}
                                    data-list=".modal-table"
                                    data-quote-counter=".basket-count"
                                    data-loading="1"
                                    data-list-loader=".dialog__loading"
                                    data-list-action="{{ route('index.basket.modal.post') }}"
                                    data-callback="refreshAfterSubmit"
                            >
                            </button>
                        </th>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="3" style="text-align: right;">
                        <button class="ajax-link small" action="{{ route('index.basket.clear.post') }}" style="cursor: pointer;font-weight: bold;padding-top: 1px;"
                                {{--data-before="clearModalBeforeSubmit"--}}
                                data-list=".modal-table"
                                data-loading="1"
                                data-quote-counter=".basket-count"
                                data-list-loader=".dialog__loading"
                                data-list-action="{{ route('index.basket.modal.post') }}"
                                data-callback="refreshAfterSubmit"
                        >
                            Очистить корзину
                        </button>
                    </th>
                </tr>
                </tbody>
            </table>
        @else
            <p style="text-align: center;margin-top: 20px">Корзина пуста.</p>
        @endif
    </div>
    <div class="modal-body --contact hidden">
        <div class="row uniform">
            <div class="12u$">
                <label style="margin-bottom: 0;font-size: 14px;">ФИО</label>
                <input class="input" type="text" name="name" value="" placeholder="ФИО" style="font-size: 14px;">
            </div>
            <div class="12u$">
                <label style="margin-bottom: 0;font-size: 14px;">Телефон</label>
                <input class="input" data-role="js-mask-phone" type="text" name="phone" placeholder="Телефон" value="" style="font-size: 14px;">
            </div>
            <div class="12u$">
                <label style="margin-bottom: 0;font-size: 14px;">E-mail</label>
                <input class="input" type="email" name="email" value="" placeholder="E-mail" style="font-size: 14px;">
            </div>
        </div>
    </div>
    @if(!is_null($oItems) && !empty($oItems[0]))
        <!-- Футер модального окна -->
        <div class="modal-footer">
            <button type="button" class="special small show-modal-body" style="padding-top: 1px;">Оставить заявку</button>
            <button type="submit" class="special small inner-form-submit hidden" style="padding-top: 1px;">Оставить заявку</button>
            <button type="button" class="small" data-dismiss="modal" style="padding-top: 1px;font-weight: bold;">Продолжить покупку</button>
        </div>
    @endif
</form>


