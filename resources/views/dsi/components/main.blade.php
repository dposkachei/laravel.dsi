<div class="menu-top">
    <div class="menu-inner">
        <div class="desc-wrap desctop">
            <div class="menu-top-contacts">
                @if(!is_null($oComposerSite->contacts->email))
                    <div class="mail">
                        <span>E-mail:</span>
                        <span> {{ $oComposerSite->contacts->email }}</span>
                    </div>
                @endif
                @if(!is_null($oComposerSite->contacts->phone))
                    <div class="telephone">
                        <span>тел:</span>
                        <span> {{ $oComposerSite->contacts->phone }}</span>
                    </div>
                @endif
            </div>
            <img src="{{ asset('images/bg/bg-top1.png') }}">
        </div>
        <div class="desc-wrap mobile">
            <img src="{{ asset('images/bg/bg-top2.png') }}">
        </div>
        <div class="btns-menu-wrap">

        </div>
    </div>
</div>