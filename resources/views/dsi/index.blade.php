@extends('dsi.layouts.app')

@section('content')
    <div class="inner">

        <!-- Header -->
        <header id="header">
            <div class="head-cont">
                <a href="/" class="logo">Главная</a>
            </div>
            @include('dsi.components.social')
        </header>

        @if(!is_null($oPage))
            @include('dsi.components.banner', [
                'oPage' => $oPage,
            ])
        @endif

        <!-- Banner -->
        {{--
        <section id="banner">
            <div class="content">
                <header>
                    <h2>{{ $oPage->title }}</h2>
                    <!--<p>A free and fully responsive site template</p>-->
                </header>
                <p><strong>"Дорстройиндустрия"</strong> - динамично развивающаяся российская компания.<br><br>Мы специализируемся на рынке <strong>дорожно-строительных материалов</strong>, создания и комплексного оснащения аналитических, испытательных и дорожно-строительных лабораторий, контролирующих качество проводимых работ и используемых материалов.</p>
                <ul class="actions">
                    <li><a href="#" class="button special">Подробнее</a></li>
                </ul>
            </div>
            <span class="image object">
                <img src="{{ ImagePath::main('page', 'original', $oPage) }}" alt="" />
            </span>
        </section>
        --}}

        <!-- Section -->
        <section>
            <header class="major">
                <h2>Преимущества</h2>
            </header>
            <div class="features">
                @foreach($oComposerArticles->where('status', 2) as $oArticle)
                    <article>
                        <span class="icon fa-{{$oArticle->icon}}"></span>
                        <div class="content __is-articles">
                            <h3>{{ $oArticle->title }}</h3>
                            <p>
                                {!! $oArticle->text !!}
                            </p>
                        </div>
                    </article>
                @endforeach
            </div>
        </section>

        <!-- Section -->
        <section class="main-cont popular_goods">
            <header class="major">
                <h2>Популярные товары</h2>
            </header>
            <div class="posts">
                @foreach($oPopularProducts as $oProduct)
                    @include('dsi.components.product.small', [
                        'oProduct' => $oProduct
                    ])
                @endforeach
            </div>
        </section>

        <!-- Section -->
        <section class="main-cont">
            <header class="major">
                <h2>Список продукции</h2>
            </header>
            <div class="posts table-component-pagination">
                @foreach($oProducts as $oProduct)
                    @include('dsi.components.product.small', [
                        'oProduct' => $oProduct
                    ])
                @endforeach
            </div>
        </section>
        <div class="table-component-pagination-button">
            @if($oProducts instanceof \Illuminate\Pagination\LengthAwarePaginator)
                {{ $oProducts->links('dsi.components.product.pagination') }}
            @endif
        </div>

        {{--
        @if ($paginator->hasMorePages())
            <div class="more-bt">
                <button class="button special ajax-link"
                        action="{{ $paginator->nextPageUrl() }}"
                        data-pagination="1"
                        data-pagination-container=".table-component-pagination"
                >
                    Загрузить ещё
                </button>
            </div>
        @endif
        --}}


    </div>
@endsection


