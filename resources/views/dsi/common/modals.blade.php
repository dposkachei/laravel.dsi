@foreach($oComposerArticles->where('status', 1) as $oArticle)
    <div id="article-{{$oArticle->id}}" class="modal {{-- top-call --}}fade">
        <div class="modal-dialog modalData">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <h4 class="modal-title">{{$oArticle->title}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body __is-articles">
                    <span>
                        {!! $oArticle->text !!}
                    </span>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button type="button" class="special small" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
@endforeach
<div id="getCall" class="modal fade">
    <div class="modal-dialog modalData" style="max-width: 400px;">
        <div class="modal-content">
            <form class="ajax-form" role="form" method="POST" action="{{ route('order.store') }}"
                  data-callback="showBodyMessageAfterSubmit"
                  style="margin: 0;"
            >
                <input type="hidden" name="type" value="2">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <h4 class="modal-title">Оставьте свои контактные данные</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row uniform">
                        <div class="12u$">
                            <label style="margin-bottom: 0;font-size: 14px;">ФИО</label>
                            <input class="input" type="text" name="name" value="" placeholder="ФИО" style="font-size: 14px;">
                        </div>
                        <div class="12u$">
                            <label style="margin-bottom: 0;font-size: 14px;">Телефон</label>
                            <input class="input" data-role="js-mask-phone" type="text" name="phone" placeholder="Телефон" value="" style="font-size: 14px;">
                        </div>
                        <div class="12u$">
                            <label style="margin-bottom: 0;font-size: 14px;">E-mail</label>
                            <input class="input" type="email" name="email" value="" placeholder="E-mail" style="font-size: 14px;">
                        </div>
                    </div>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button type="submit" class="special small inner-form-submit" style="padding-top: 1px;">Заказать звонок</button>
                    <button type="button" class="small" data-dismiss="modal" style="padding-top: 1px;font-weight: bold;">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="getBasket" class="modal fade">
    <div class="modal-dialog modalData">
        <div class="modal-content">
            <div class="dialog__loading" style="width: 100%;height: 200px;"></div>
            <div class="modal-table">

            </div>
        </div>
    </div>
</div>

<div id="pages-dialogs-ajax" class="dialog">
    <div class="dialog__overlay"></div>

</div>