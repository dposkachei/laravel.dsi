@extends('dsi.layouts.app')

@section('content')
    <div class="inner">
        <!-- Header -->
        <header id="header">
            <div class="head-cont">
                <span class="logo">Контакты</span>
            </div>
            @include('dsi.components.social')
        </header>

        @if(!is_null($oPage))
            @include('dsi.components.banner', [
                'oPage' => $oPage
            ])
        @endif

        <section class="main-cont" style="border-top: none;">
            <div class="desc-cont">
                {{--<hr class="major" />--}}
                <a class="button special" href="#getCall" data-toggle="modal">Связаться с нами</a>
            </div>
            <table style="padding-top: 20px;margin: inherit; margin-top: 20px; margin-bottom: 20px;">
                <thead>
                </thead>
                <tbody>
                @foreach($oContacts as $oContact)
                    <tr>
                        <td>{{ $oContact->title }}:</td>
                        <td>{{ $oContact->value }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <table style="padding-top: 20px;margin: inherit; margin-top: 20px; margin-bottom: 20px;">
                <thead>
                    <tr>
                        <th style="padding:0.75em">Реквизиты:</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($oRequisites as $oRequisite)
                    <tr>
                        <td>{{ $oRequisite->title }}</td>
                        <td>{{ $oRequisite->value }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="map_cont">
                <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0755a0ee06e7ff664dd7bd1d30d662ec4d543d3fd8e58336a38456c177427999&amp;width=100%25&amp;height=583&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </section>

    </div>
@endsection


