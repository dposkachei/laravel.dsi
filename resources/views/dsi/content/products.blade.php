@extends('dsi.layouts.app')

@section('content')
    <div class="inner">

        <!-- Header -->
        <header id="header">
            <div class="head-cont">
                <span class="logo">Продукции</span>
            </div>
            @include('dsi.components.social')
        </header>

        @if(!is_null($oPage))
            @include('dsi.components.banner', [
                'oPage' => $oPage
            ])
        @endif

        @if(!empty($oProducts) && !empty($oProducts[0]))
            <!-- Section -->
            <section class="main-cont">
                <header class="major">
                    <h2>Список продукции</h2>
                </header>
                <div class="posts table-component-pagination">
                    @foreach($oProducts as $oProduct)
                        @include('dsi.components.product.small', [
                            'oProduct' => $oProduct
                        ])
                    @endforeach
                </div>
            </section>
            <div class="table-component-pagination-button">
                @if($oProducts instanceof \Illuminate\Pagination\LengthAwarePaginator)
                    {{ $oProducts->links('dsi.components.product.pagination') }}
                @endif
            </div>
        @endif

    </div>
@endsection


