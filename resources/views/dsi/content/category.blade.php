@extends('dsi.layouts.app')

@section('content')
    <div class="inner">
        <!-- Header -->
        <header id="header">
            <div class="head-cont">
                @foreach($oCategory->getParents('category') as $oParent)
                    <a href="{{ route('index.category', ['id' => $oParent->id, 'slug' => $oParent->url ]) }}">{{ $oParent->title }}</a> /
                @endforeach
                <span class="logo">{{ $oCategory->title }}</span>
            </div>
            @include('dsi.components.social')
        </header>
        <section class="main category-header">
            <div class="row tab-img">
                @if(ImagePath::checkMain('category', 'original', $oCategory))
                    <div class="col-md-6">
                        <img src="{{ ImagePath::main('category', 'original', $oCategory) }}" alt="{{$oCategory->title}}" />
                    </div>
                    <div class="col-md-6">
                        {!! $oCategory->text !!}
                    </div>
                @else
                    <div class="col-md-12">
                        {!! $oCategory->text !!}
                    </div>
                @endif
            </div>
        </section>
        @if($oCategory->hasChildren())
            <section class="main-cont">
                <header class="major">
                    <h2>Подкатегории</h2>
                </header>
                <div class="posts">
                    @foreach($oCategory->children as $oCategory)
                        @include('dsi.components.category.small', [
                            'oCategory' => $oCategory
                        ])
                    @endforeach
                </div>
            </section>
        @endif
        <section class="main-cont">
            <header class="major">
                <h2>Продукции</h2>
            </header>
            <div class="posts">
                @foreach($oProducts as $oProduct)
                    @include('dsi.components.product.small', [
                        'oProduct' => $oProduct
                    ])
                @endforeach
            </div>
        </section>
    </div>
@endsection


