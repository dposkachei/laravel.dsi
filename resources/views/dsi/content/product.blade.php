@extends('dsi.layouts.app')

@section('content')
    <div class="inner" id="product">
        <!-- Header -->
        <header id="header">
            <div class="head-cont">
                @foreach($oCategory->getParents() as $oParent)
                    <a href="{{ route('index.category', ['id' => $oParent->id, 'slug' => $oParent->url ]) }}">{{ $oParent->title }}</a> /
                @endforeach
                <span class="logo">{{ $oProduct->title }}</span>
            </div>
            @include('dsi.components.social')
        </header>
        <section class="main-cont for_item">
            <header class="main">
                <h2>{{ $oProduct->title }}</h2>
            </header>
            <div class="desc-cont">
                <!--<hr class="major" />
                    <a class="button special" href="#getData" data-toggle="modal">Заказать</a>
                <hr class="major" />-->
                <div class="text-cont">
                    <div class="text-cont-left">
                        <div class="text-cont-left-description">
                            @if(ImagePath::checkMain('product', 'original', $oProduct))
                                <div class="image for-content-img" style="padding-right: 1em;">
                                    <img src="{{ ImagePath::main('product', 'original', $oProduct) }}" alt="{{$oProduct->title}}" />
                                </div>
                            @endif
                            {!! $oProduct->text !!}
                        </div>
                    </div>
                    <div class="serts-cont">
                        <h2>Список сертификатов</h2>
                        @foreach($oProduct->certificates as $oCertificate)
                            <div class="serts-cont-wrp" style="text-align: center;">
                                <a data-fancybox="gallery" href="{{ ImagePath::main('certificate', 'original', $oCertificate) }}"
                                   @if(!is_null($oCertificate->files->first()))
                                   data-caption='{{ $oCertificate->text }}<br/>Просмотр сертификата <a href="{{ FilePath::image('certificate', 'original', $oCertificate->files->first()) }}" target="_blank">{{ $oCertificate->title }}</a>'
                                   {{--data-caption='{{ $oCertificate->text }}<br/>Быстрый просмотр сертификата <a data-fancybox data-src="{{ FilePath::image('certificate', 'original', $oCertificate->files->first()) }}" data-type="iframe" href="javascript:;">{{ $oCertificate->title }}</a>'--}}
                                   @else
                                   data-caption='{{ $oCertificate->text }}'
                                   @endif
                                >
                                    <span style="color: #3d3d3d; font-weight: bold;">{{ $oCertificate->title }}</span>
                                    <img src="{{ ImagePath::main('certificate', 'original', $oCertificate) }}">
                                </a>
                            </div>
                            <hr style="margin: .5em 0;">
                        @endforeach
                        {{--
                        <div class="serts-cont-wrp">
                            <a data-fancybox="gallery" href="images/items/serts/1.jpg">
                                <img src="images/items/ra.png">
                            </a>
                        </div>
                        <div class="serts-cont-wrp">
                            <a data-fancybox="gallery" href="images/items/serts/1.jpg">
                                <img src="images/items/rst.png">
                            </a>
                        </div>
                        <div class="serts-cont-wrp">
                            <a data-fancybox="gallery" href="images/items/serts/1.jpg">
                                <img src="images/items/ra.png">
                            </a>
                        </div>
                        <div class="serts-cont-wrp">
                            <a data-fancybox="gallery" href="images/items/serts/1.jpg">
                                <img src="images/items/rst.png">
                            </a>
                        </div>
                        <div class="serts-cont-wrp">
                            <a data-fancybox="gallery" href="images/items/serts/1.jpg">
                                <img src="images/items/ra.png">
                            </a>
                        </div>
                        --}}
                    </div>

                </div>

                <hr class="major" />
                <a class="button special" href="#getBasket" data-toggle="modal" data-action="{{ route('index.basket.store.post', ['id' => $oProduct->id]) }}"
                   data-after-action="{{ route('index.basket.modal.post') }}"
                >
                    Заказать
                </a>
            </div>

        </section>
    </div>
@endsection


