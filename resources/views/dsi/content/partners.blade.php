@extends('dsi.layouts.app')

@section('content')
    <div class="inner">
        <!-- Header -->
        <header id="header">
            <div class="head-cont">
                <span class="logo">Партнеры</span>
            </div>
            @include('dsi.components.social')
        </header>

        @if(!is_null($oPage))
            @include('dsi.components.banner', [
                'oPage' => $oPage,
            ])
        @endif

        <section class="main-cont" style="border-top: none;padding-top: 0;">
            <div class="row tab-img">
                <div class="col-md-6">
                    <ul>
                        @foreach($oPartners as $oPartner)
                            <li>
                                @if(!is_null($oPartner->url))
                                    <a href="{{ $oPartner->url }}" target="_blank">{{ $oPartner->title }}</a>
                                @else
                                    {{ $oPartner->title }}
                                @endif
                                @if(ImagePath::checkMain('partner', 'original', $oPartner))
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a data-fancybox="gallery" href="{{ ImagePath::main('partner', 'original', $oPartner) }}">
                                                <img src="{{ ImagePath::main('partner', 'original', $oPartner) }}">
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                        <li>и др.</li>
                    </ul>
                    <p></p>
                </div>
            </div>
            {{--<p>Имея определенный опыт распространения продукции разных производителей в Южном Федеральном  Округе, Северо-Кавказском Федеральном Округе мы предлагаем Вам свои услуги по распространению Вашей продукции в качестве официального дилера.<br><br>Убедительная просьба рассмотреть наше предложение и выслать в наш адрес предложения по совместной работе.</p>--}}
        </section>
    </div>
@endsection


