@extends('dsi.layouts.app')

@section('content')
    <div class="inner">

        <!-- Header -->
        <header id="header">
            <div class="head-cont">
                @if(method_exists($oProducts, 'total'))
                    {!! Helper::wordWithCounts($oProducts->total(), ['продукция', 'продукции', 'продукций'], false) !!}
                @else
                    {!! Helper::wordWithCounts(count($oProducts), ['продукция', 'продукции', 'продукций'], false) !!}
                @endif
            </div>
            @include('dsi.components.social')
        </header>
        @if(!empty($oProducts) && !empty($oProducts[0]))
            <!-- Section -->
            <section class="main-cont">
                <header class="major">
                    <h2>Список продукции</h2>
                </header>
                <div class="posts table-component-pagination">
                    @foreach($oProducts as $oProduct)
                        @include('dsi.components.product.small', [
                            'oProduct' => $oProduct
                        ])
                    @endforeach
                </div>
            </section>
            <div class="table-component-pagination-button">
                @if($oProducts instanceof \Illuminate\Pagination\LengthAwarePaginator)
                    {{ $oProducts->links('dsi.components.product.pagination') }}
                @endif
            </div>
        @endif

    </div>
@endsection


