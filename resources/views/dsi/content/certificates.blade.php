@extends('dsi.layouts.app')

@section('content')
    <div class="inner">
        <!-- Header -->
        <header id="header">
            <div class="head-cont">
                <span class="logo">Сертификаты</span>
            </div>
            @include('dsi.components.social')
        </header>

        @if(!is_null($oPage))
            @include('dsi.components.banner', [
                'oPage' => $oPage
            ])
        @endif

        <section class="main-cont" style="border-top: none;padding-top: 0;">
            <div class="row tab-img">
                @foreach($oCertificates as $oCertificate)
                    <div class="col-md-3">
                        <span style="display: block; font-weight: bold; min-height: 42px; line-height: 1.1;">{{ $oCertificate->title }}</span>
                        <a data-fancybox="gallery" href="{{ ImagePath::main('certificate', 'original', $oCertificate) }}"
                            @if(FilePath::check('certificate', 'original', $oCertificate->files->first()))
                            data-caption='{{ $oCertificate->text }}<br/>Просмотр сертификата <a href="{{ FilePath::image('certificate', 'original', $oCertificate->files->first()) }}" target="_blank">{{ $oCertificate->title }}</a>'
                                {{--data-caption='{{ $oCertificate->text }}<br/>Быстрый просмотр сертификата <a data-fancybox data-src="{{ FilePath::image('certificate', 'original', $oCertificate->files->first()) }}" data-type="iframe" href="javascript:;">{{ $oCertificate->title }}</a>'--}}
                            @else
                                data-caption='{{ $oCertificate->text }}'
                            @endif
                        >
                            <img src="{{ ImagePath::main('certificate', 'original', $oCertificate) }}">
                        </a>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection


