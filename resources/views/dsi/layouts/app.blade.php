<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<meta name="yandex-verification" content="16d330adff5eb025" />--}}
    <meta name="yandex-verification" content="c0a29e81fed6567f" />
    @include('dsi.components.meta')
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>
<body class="enlarged">
    <!-- Wrapper -->
    <div id="wrapper" class="__3">

        <!-- Main -->
        <div id="main">
            <div class="menu-top">
                <div class="menu-inner">
                    <div class="desc-wrap desctop">
                        <div class="menu-top-contacts">
                            @if(!is_null($oComposerSite->contacts->email))
                                <div class="mail">
                                    <span>E-mail:</span>
                                    <span> {{ $oComposerSite->contacts->email }}</span>
                                </div>
                            @endif
                            @if(!is_null($oComposerSite->contacts->phone))
                                <div class="telephone">
                                    <span>тел:</span>
                                    <span> {{ $oComposerSite->contacts->phone }}</span>
                                </div>
                            @endif
                        </div>
                        <img src="/images/bg/bg-top1.png">
                    </div>
                    <div class="desc-wrap mobile">
                        <img src="/images/bg/bg-top2.png">
                    </div>
                    <div class="btns-menu-wrap">

                    </div>
                </div>
            </div>

            @include('dsi.components.navigation')

            @include('dsi.components.help')

            @yield('content')
        </div>

        @include('dsi.components.sidebar')

    </div>

    <div id="modals">
        @include('dsi.common.modals')
    </div>
    <div id="scripts">
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/toastr.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>

        <script>
            toastr.options = {!! json_encode(config('toastr.options')) !!};
        </script>
        {!! Toastr::render() !!}
    </div>
    <div id="api">
        @include('bulma.components.api.yandex.metrika')
        @include('bulma.components.api.google.analytics')
    </div>
</body>
</html>
