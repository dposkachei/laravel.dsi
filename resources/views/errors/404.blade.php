<!DOCTYPE html>
<html>
<head>
    <title>Страница не найдена.</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Roboto', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 60px;
            margin-bottom: 40px;
        }
        .description {
            font-size: 30px;
            text-align: right;
        }
        a {
            text-decoration: none;
            color: #999;
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Страница не найдена</div>
        <div class="description">
            {{ config('app.name', 'Laravel') }}
        </div>
        <div class="description">
            <a href="{{ url('/') }}" style="font-size: 20px;">
                Перейти на главную страницу
            </a>
        </div>
    </div>
</div>
</body>
</html>
