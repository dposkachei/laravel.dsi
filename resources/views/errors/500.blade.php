<!DOCTYPE html>
<html>
    <head>
        <title>Ошибка сервера.</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Roboto', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 60px;
                margin-bottom: 40px;
            }
            .description {
                font-size: 30px;
                text-align: right;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Ошибка сервера. 500.</div>
                <div class="description">{{ config('app.name', 'Laravel') }}</div>
            </div>
        </div>
        {{--
        @unless(empty($sentryID))
        <!-- Sentry JS SDK 2.1.+ required -->
        <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

        <script>
            Raven.showReportDialog({
                eventId: '{{ $sentryID }}',

                // use the public DSN (dont include your secret!)
                dsn: 'https://eb733c097e484c56a09df412326ee544@sentry.io/168031'
            });
        </script>
        @endunless
        --}}
    </body>
</html>
