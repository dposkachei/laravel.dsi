<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    @include('dsi.components.meta')
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body class="enlarged">
    <!-- Wrapper -->
    <div id="wrapper" class="__3">

        <!-- Main -->
        <div id="main">
            <div class="menu-top">
                <div class="menu-inner">
                    <div class="desc-wrap desctop">
                        <img src="images/bg/bg-top1.png">
                    </div>
                    <div class="desc-wrap mobile">
                        <img src="images/bg/bg-top2.png">
                    </div>
                    <div class="btns-menu-wrap">

                    </div>
                </div>
            </div>

            @include('dsi.components.navigation')

            <div class="menu-top-btns">
                <div class="top-line-section">
                    <div class="cart-n-search">
                        <div class="cart">
                            <a href="#">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                <span>Корзина</span>
                            </a>
                        </div>
                        <div class="search">
                            <form method="post" action="#">
                                <input type="text" name="query" id="query" placeholder="Поиск" />
                            </form>

                            <div class="find-bt">
                                <a href="#" class="__search">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="quick-bts">
                        <div class="top-btn-delivery item">
                            <div class="item-wrp">
                                <span class="icon fa-truck"></span>
                                <a href="#getDelivery" data-toggle="modal">Условия доставки</a>
                            </div>
                        </div>
                        <div class="top-btn-garanties item">
                            <div class="item-wrp">
                                <span class="icon fa-shield"></span>
                                <a href="#getGaranties" data-toggle="modal">Наши гарантии</a>
                            </div>
                        </div>
                        <div class="top-btn-payment item">
                            <div class="item-wrp">
                                <span class="icon fa-credit-card"></span>
                                <a href="#getCondition" data-toggle="modal">Условия оплаты</a>
                            </div>
                        </div>
                        <div class="top-btn-call item">
                            <div class="item-wrp">
                                <a href="#getCall" data-toggle="modal">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @yield('content')
        </div>

        @include('dsi.components.sidebar')

    </div>

    @include('dsi.common.modals')

    <script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
