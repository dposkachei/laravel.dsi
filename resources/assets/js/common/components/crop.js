// Check for jQuery.
if (typeof(jQuery) === 'undefined') {
    var jQuery;
    // Check if require is a defined function.
    if (typeof(require) === 'function') {
        jQuery = $ = require('jquery');
        // Else use the dollar sign alias.
    } else {
        jQuery = $;
    }
}

(function($){
    $.fn.bulkCrop = function(options) {
        var settings = $.extend({
            element: '#cropper',
            close: '#crop-delete',
            inputs: {
                x: '#cropX',
                y: '#cropY',
                w: '#cropW',
                h: '#cropH',
                cropImageWith: '#cropImageWith',
                cropImageHeight: '#cropImageHeight'
            },
            after: '.control.__crop-active',
            before: '.control.__crop-init'
        }, options);

        $(settings.close).click(function(){
            JcropAPI = $(settings.element).data('Jcrop');
            JcropAPI.destroy();
            hide();
        });


        var update = function(c) {
            $(settings.inputs.x).val(c.x);
            $(settings.inputs.y).val(c.y);
            $(settings.inputs.w).val(c.w);
            $(settings.inputs.h).val(c.h);
            $(settings.inputs.cropImageWith).val($(settings.element).css('width'));
            $(settings.inputs.cropImageHeight).val($(settings.element).css('height'));
        };

        var show = function() {
            $(settings.after).css('display','block');
            $(settings.before).css('display','none');
        };

        var hide = function() {
            $(settings.element).css('height','auto');
            $(settings.element).css('width','100%');
            $(settings.before).css('display','block');
            $(settings.after).css('display','none');
        };

        var init = function() {
            $(this)
                .click(function() {
                    show();
                    $(settings.element).Jcrop({
                        onSelect: update
                    });
                });
        };

        return this.each(init);
    };
}(jQuery));

$(document).ready(function() {
    $('#crop-image').bulkCrop({
        element: '#cropper',
        close: '#crop-delete',
        inputs: {
            x: '#cropX',
            y: '#cropY',
            w: '#cropW',
            h: '#cropH',
            cropImageWith: '#cropImageWith',
            cropImageHeight: '#cropImageHeight'
        },
        before: '.control.__crop-active',
        after: '.control.__crop-init'
    });
});


