$(document).ready(function() {
    /*
     |----------------------------------------
     | Table delete loader
     | td.is-icon>a
     |----------------------------------------
     */
    $('.is-icon a').click(function() {
        var element = $(this);
        element.addClass('is-loading');
        setTimeout(function() {
            element.removeClass('is-loading');
            //$('#notification-trigger').trigger('click');
        }, 3000);
    });

    /*
     |----------------------------------------
     | Table select loader
     | .select>select
     |----------------------------------------
     */
    $('.select select').change(function(event) {
        var element = $(this).parent();
        element.addClass('is-loading');
        setTimeout(function() {
            element.removeClass('is-loading');
            //$('#notification-trigger').trigger('click');
        }, 2000);
    });
});


