// Check for jQuery.
if (typeof(jQuery) === 'undefined') {
    var jQuery;
    // Check if require is a defined function.
    if (typeof(require) === 'function') {
        jQuery = $ = require('jquery');
        // Else use the dollar sign alias.
    } else {
        jQuery = $;
    }
}

$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});

(function($){
    $.fn.bulkModal = function(options) {
        var settings = $.extend({
            openDialogClass: 'dialog--open',

            ajax: false,
            confirm: false,
            ajaxClass: 'dialog-ajax',
            disableOverlayClass: 'dialog-disabled_overlay',
            loadingClass: '.dialog__loading',
            loading: '<div class="dialog__loading"><div></div></div>'

        }, options);

        $('.dialog').on('click', '.dialog__overlay', function() {
            var $dialog = $(this).closest('.dialog');

            if (!$dialog.hasClass(settings.disableOverlayClass)) {
                $dialog.removeClass(settings.openDialogClass);
                if (_.isFunction(window['closeDialogCallback'])){
                    window['closeDialogCallback']($dialog);
                }

                if ($dialog.hasClass(settings.ajaxClass)) {
                    $dialog.removeClass(settings.ajaxClass);
                    Ajax.remove($dialog);
                }
            }
        });
        $('.dialog').on('click', '.dialog__close', function() {
            var $dialog = $($(this).attr('data-dialog'));
            $dialog.removeClass(settings.openDialogClass);
            if (_.isFunction(window['closeDialogCallback'])){
                window['closeDialogCallback']($dialog);
            }
            if ($dialog.hasClass(settings.ajaxClass)) {
                $dialog.removeClass(settings.ajaxClass);
                Ajax.remove($dialog);
            }
        });

        var Ajax = {
            add: function ($dialog, $target) {
                Ajax.remove($dialog);
                $dialog.addClass(settings.ajaxClass);
                $dialog.append(settings.loading);
                $dialog.addClass(settings.disableOverlayClass);

                setTimeout(function() {
                    $dialog.removeClass(settings.disableOverlayClass);
                }, 7000);

                $.ajax({
                    url: $target.attr('data-action'),
                    type: "POST",
                    data: Ajax.data($target),
                    success: function (result) {
                        $dialog.find(settings.loadingClass).remove();
                        $dialog.removeClass(settings.disableOverlayClass);
                        if (result.view) {
                            Ajax.append($dialog, result.view);
                        } else {
                            Ajax.append($dialog, result);
                        }
                    },
                    error: function(data, status, headers, config) {
                        $dialog.removeClass(settings.disableOverlayClass);
                        Ajax.remove($dialog);
                    }
                });
            },
            remove: function ($dialog) {
                $dialog.children()
                    .filter(function() {
                        return (!$(this).hasClass('dialog__overlay'));
                    }).remove();
            },
            append: function($dialog, view) {
                Ajax.remove($dialog);
                $dialog.append(view);
            },
            data: function($target) {
                return $target.data();
            }
        };

        var Confirm = {
            add: function($dialog, $target) {
                if ($target.data('text') !== undefined) {
                    $dialog.find('.subtitle').text($target.data('text'));
                }
                if ($target.data('action') !== undefined) {
                    $dialog.find('.ajax-form').attr('action', $target.data('action'));
                }
                if ($target.data('list-action') !== undefined) {
                    $dialog.find('.ajax-form').attr('data-list-action', $target.data('list-action'));
                }
            }
        };

        var init = function() {
            $('body')
                .on('click', $(this), function() {
                    console.log('click');
                    var $dialog = $($(this).attr('data-dialog'));
                    $dialog.addClass(settings.openDialogClass);

                    if (settings.ajax) {
                        Ajax.add($dialog, $(this));
                    }

                    if (settings.confirm) {
                        Confirm.add($dialog, $(this));
                    }

                    if ($(this).attr('data-disabled-overlay') !== undefined) {
                        $dialog.addClass(settings.disableOverlayClass);
                    }
                });
        };
        return this.each(init);
    };
}(jQuery));

window['closeDialogCallback'] = function($dialog) {
    $dialog.find('input.is-danger').removeClass('is-danger');
    $dialog.find('.help.is-danger').remove();
    $dialog.find('input').empty();
    var $body = $('html');
    $body.removeClass('--fixed');
    $body.css('margin-right', 0);
};

var ajaxDialogs = {

    settings : {
        openDialogClass: 'dialog--open',

        ajax: false,
        confirm: false,
        ajaxClass: 'dialog-ajax',
        disableOverlayClass: 'dialog-disabled_overlay',
        loadingClass: '.dialog__loading',
        loading: '<div class="dialog__loading"><div></div></div>'
    },

    target : null,

    bind : function (sElem, sDelegateFrom, sAction, oSettings) {
        sDelegateFrom = sDelegateFrom || '';
        sAction = sAction || 'submit';
        var fn = function (event) {
            event.preventDefault();
            this.target = $(event.currentTarget);
            this.send();
            return false;
        };
        fn = _.bind(fn, this);
        if(sDelegateFrom){
            $(sDelegateFrom).on(sAction, sElem, fn);
        } else {
            $(sElem).on(sAction, fn);
        }
        var self = this;
        _.each(oSettings, function(field, key) {
            self.settings[key] = field;
        });
        console.log(ajaxDialogs.settings);
    },

    send : function () {
        console.log('send click');
        var $dialog = $(this.target.attr('data-dialog'));
        $dialog.addClass(this.settings.openDialogClass);

        if (this.settings.ajax) {
            console.log(this.settings.ajax);
            this.addAjax($dialog, this.target);
        }

        if (this.settings.confirm) {
            console.log(this.settings.confirm);
            this.addConfirm($dialog, this.target);
        }

        if (this.target.attr('data-disabled-overlay') !== undefined) {
            $dialog.addClass(this.settings.disableOverlayClass);
        }
        var $body = $('html');
        $body.addClass('--fixed');
        $body.css('margin-right', document.getScrollbarWidth() + 'px');
    },

    addAjax: function ($dialog, $target) {
        var self = this;
        this.removeAjax($dialog);
        $dialog.addClass(this.settings.ajaxClass);
        $dialog.append(this.settings.loading);
        $dialog.addClass(this.settings.disableOverlayClass);

        setTimeout(function() {
            $dialog.removeClass(self.settings.disableOverlayClass);
        }, 7000);


        $.ajax({
            url: $target.attr('data-action'),
            type: "POST",
            data: self.dataAjax($target),
            success: function (result) {
                $dialog.find(self.settings.loadingClass).remove();
                $dialog.removeClass(self.settings.disableOverlayClass);
                if (result.view) {
                    self.appendAjax($dialog, result.view);
                } else {
                    self.appendAjax($dialog, result);
                }
                self.afterAppend();
            },
            error: function(data, status, headers, config) {
                $dialog.removeClass(self.settings.disableOverlayClass);
                self.removeAjax($dialog);
            }
        });
    },
    afterAppend: function() {
        if (this.target.attr('data-ajax-init') !== undefined) {
            var aInit = _.split(this.target.attr('data-ajax-init'), ',');
            if(_.first(aInit) == '@'){
                aInit = _.drop(aInit);
            }
            _.each(aInit, function (val) {
                console.log(val);
                var sFuncName = _.trim(val);
                if (_.isFunction(window[sFuncName])){
                    window[sFuncName]();
                }
            });
        }
    },
    removeAjax: function ($dialog) {
        $dialog.children()
            .filter(function() {
                return (!$(this).hasClass('dialog__overlay'));
            }).remove();
    },
    appendAjax: function($dialog, view) {
        this.removeAjax($dialog);
        $dialog.append(view);
    },
    dataAjax: function($target) {
        return $target.data();
    },
    addConfirm: function($dialog, $target) {
        if ($target.data('text') !== undefined) {
            $dialog.find('.subtitle').text($target.data('text'));
        }
        if ($target.data('action') !== undefined) {
            $dialog.find('.ajax-form').attr('action', $target.data('action'));
        }
        if ($target.data('list-action') !== undefined) {
            $dialog.find('.ajax-form').attr('data-list-action', $target.data('list-action'));
        }
    }
};



/*
 |----------------------------------------
 | Dialogs
 |
 | Template:
 |
 | Button
 | <a class="trigger" data-dialog="#register">Регистрация</a>
 | <a class="trigger" data-dialog="#register" data-ajax data-action="url" data-ajax-init="callback, callback">Регистрация</a>
 | <a class="trigger" data-dialog="#register" data-disabled-overlay>Регистрация</a>
 |----------------------------------------
 */
$(document).ready(function() {
    window['bulkInit']();
});

window['bulkInit'] = function() {


    $('.dialog').on('click', '.dialog__overlay', function() {
        var $dialog = $(this).closest('.dialog');

        if (!$dialog.hasClass(ajaxDialogs.settings.disableOverlayClass)) {
            $dialog.removeClass(ajaxDialogs.settings.openDialogClass);
            if (_.isFunction(window['closeDialogCallback'])){
                window['closeDialogCallback']($dialog);
            }

            if ($dialog.hasClass(ajaxDialogs.settings.ajaxClass)) {
                $dialog.removeClass(ajaxDialogs.settings.ajaxClass);
                ajaxDialogs.removeAjax($dialog);
            }
        }
    });
    $('.dialog').on('click', '.dialog__close', function() {
        var $dialog = $($(this).attr('data-dialog'));
        $dialog.removeClass(ajaxDialogs.settings.openDialogClass);
        if (_.isFunction(window['closeDialogCallback'])){
            window['closeDialogCallback']($dialog);
        }
        if ($dialog.hasClass(ajaxDialogs.settings.ajaxClass)) {
            $dialog.removeClass(ajaxDialogs.settings.ajaxClass);
            ajaxDialogs.removeAjax($dialog);
        }
    });


    var simpleDialog = $.extend(true, {}, ajaxDialogs);
    var ajaxDialog = $.extend(true, {}, ajaxDialogs);
    var confirmDialog = $.extend(true, {}, ajaxDialogs);

    simpleDialog.bind('.trigger', 'body', 'click', {
        openDialogClass: 'dialog--open'
    });
    ajaxDialog.bind('.trigger[data-ajax]', 'body', 'click', {
        openDialogClass: 'dialog--open',
        ajax: true
    });
    confirmDialog.bind('.trigger[data-confirm]', 'body', 'click', {
        openDialogClass: 'dialog--open',
        confirm: true
    });


    /*
    $('.trigger').bulkModal({
        openDialogClass: 'dialog--open'
    });
    $('.trigger[data-ajax]').bulkModal({
        openDialogClass: 'dialog--open',
        ajax: true
    });
    $('.trigger[data-confirm]').bulkModal({
        openDialogClass: 'dialog--open',
        confirm: true
    });
    */
};