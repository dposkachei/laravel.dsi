$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});

var ajaxForm = {

    options : {
        ajaxLink: true,
        ajaxForm: true,
        ajaxTabs: true,
        validation: {
            helpClass: 'help',
            errorClass: 'is-danger',
            helpErrorClass: '.help.is-danger'
        },
        innerFormSubmit: '.inner-form-submit',
        loadingClass: 'is-loading'
    },


    data : {

    },

    form : null,
    tagName : null,

    bind : function (sElem, sDelegateFrom, sAction) {
        sDelegateFrom = sDelegateFrom || '';
        sAction = sAction || 'submit';
        var fn = function (event) {
            event.preventDefault();
            this.form = $(event.currentTarget);
            this.tagName = this.form.get(0).tagName;
            this.send();
            return false;
        };
        fn = _.bind(fn, this);
        if(sDelegateFrom){
            $(sDelegateFrom).on(sAction, sElem, fn);
        } else {
            $(sElem).on(sAction, fn);
        }
    },

    send : function () {
        if(this.form.attr('method') === 'get'){

        } else {
            this.post();
        }
    },

    post : function () {
        var data;
        this.before();
        if (this.form.data('search')) {
            if (this.form.val().length !== 0 && this.form.val().length < 3) {
                return;
            } else {
                data = {search : this.form.val()};
            }
        } else {
            data = this.getFormData();
        }
        this.clearValidate();
        if(!this.validate()) {
            return;
        }
        this.startLoading();
        var self = this;
        if (!self.before(data)) {
            return;
        }
        $.ajax({
            url: this.form.attr('action'),
            type: "POST",
            data: data,
            success: function (result) {
                self.stopLoading();

                if (result.toastr) {
                    self.notification(result.toastr);
                }
                if(result.success) {
                    self.after(result);
                } else if(result.error) {
                    self.showError(result.message);
                }
                if (result.item) {
                    self.setItemError(result.item);
                }
                if (self.form.data('pagination') && result.view) {
                    self.pagination(result);
                }
                if (self.form.data('search') && result.view) {
                    $(self.form.data('pagination-container')).html(result.view);
                }
                if (self.form.data('tab') && result.view) {
                    $('.ajax-tabs').find('li').removeClass('is-active');
                    self.form.closest('li').addClass('is-active');
                    $(self.form.data('container')).html(result.view);
                }
                if (result.push) {
                    self.push(result.push);
                }
                window['tippy-tooltip']('.admin-table');

            },
            error: function (data, status, headers, config) {
                if (data.toastr) {
                    self.notification(data.toastr);
                }
                self.stopLoading();

                self.validateServer(data);
            }
        });
    },

    notification : function(notification) {
        toastr.options.closeButton = true;
        toastr.options.closeDuration = 10;
        switch(notification.type) {
            case 'warning':
                toastr.warning(notification.text, notification.title, notification.options);
                break;
            case 'success':
                toastr.success(notification.text, notification.title, notification.options);
                break;
            case 'error':
                toastr.error(notification.text, notification.title, notification.options);
                break;
            case 'info':
                toastr.info(notification.text, notification.title, notification.options);
                break;
            default:
                break;
        }
    },

    push : function(data) {
        var title = data.title ? data.title : 'Новое сообщение';
        var text = data.text ? data.text : '';
        var icon = data.icon ? data.icon : '';
        var timeout = data.timeout ? data.timeout : 10000;

        Push.create(title, {
            body: text,
            icon: icon,
            timeout: timeout,
            vibrate: [200,100],
            onClick: function () {
                window.focus();
                this.close();
            }
        });
    },

    get : function () {

    },

    submit : function () {

    },

    /**
     * data-pagination="1"                          -- включить пагинацию
     * data-pagination-container="selector"         -- куда будет вставляться шаблок
     * data-append="1"                              -- если true, то шаблон будет добавлять в контейнер
     * data-form-query="selector"                   -- скрытая форма для пагинации
     * <form class="pagination-form">
            <input type="hidden" name="page" value="{{ $paginator->currentPage() }}">
            @if(isset($aSearch) && !empty($aSearch))
                @foreach($aSearch as $key => $value)
                    <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                @endforeach
            @endif
        </form>
     * data-pagination-view-list-button="1"         -- означает, что кнопка находится отдельно от контейнера,
     *                                                 ожидается для контейнера result.view.list
     * data-pagination-button-container="selector"  -- контейнер для кнопки, ожидается result.view.button
     * @param result
     */
    pagination : function(result) {
        if (this.form.data('pagination-view-list-button')) {
            if (this.form.data('append')) {
                $(this.form.data('pagination-container')).append(result.view.list);
                $(this.form.data('pagination-button-container')).html(result.view.button);
            } else {
                $(this.form.data('pagination-container')).html(result.view.list);
                $(this.form.data('pagination-button-container')).html(result.view.button);
            }
        } else {
            if (this.form.data('append')) {
                this.form.closest(this.form.data('pagination-button-container')).remove();
                $(this.form.data('pagination-container')).append(result.view);
            } else {
                $(this.form.data('pagination-container')).html(result.view);
            }
        }
    },

    stopLoading : function() {

        this.form.find(this.options.innerFormSubmit).removeClass(this.options.loadingClass);
        this.form.removeClass(this.options.loadingClass);
        this.form.closest('.select').removeClass(this.options.loadingClass);
    },

    startLoading : function() {
        this.form.find(this.options.innerFormSubmit).addClass(this.options.loadingClass);

        this.form.closest('.select').addClass(this.options.loadingClass);

        if (this.form.data('pagination')) {
            this.form.addClass(this.options.loadingClass);
        }
        if (this.form.data('loading')) {
            this.form.addClass(this.options.loadingClass);
        }
    },

    after : function (result) {
        var sCallbacks = this.form.data('callback') || result.callback;
        var bDefaultsCall = true;
        var self = this;
        if(sCallbacks) {
            var aCallbacks = _.split(sCallbacks, ',');
            if(_.first(aCallbacks) === '@'){
                bDefaultsCall = false;
                aCallbacks = _.drop(aCallbacks);
            }
            _.each(aCallbacks, function (val) {
                var sFuncName = _.trim(val);
                if (_.isFunction(window['after-' + sFuncName])){
                    window['after-' + sFuncName](result, self.form);
                }
            });
        }
        this.ajaxInitPlugins();

        if(bDefaultsCall) {
            this.afterDefault(result);
        }
        if(result.redirect) {
            window.location.replace(result.redirect);
        }
        if(result.post) {
            if (result.post.form) {
                $('body').append(result.post.form);
                var form = $('#ext_auth_form');
                form.submit();
                form.remove();
            }
        }
        if (result._blank) {
            var linkToDownload = result._blank;
            var downloadLink = document.createElement('a');
            downloadLink.id = "link-to-download";
            downloadLink.href = linkToDownload;
            downloadLink.setAttribute("target", "_blank");
            document.body.appendChild(downloadLink);
            downloadLink.click();
            setTimeout(function() {
                $('#link-to-download').remove();
            }, 1000);
        }
        if (this.form.data('has-active')) {
            var aItems = _.split(this.form.data('items'), ',');
            _.each(aItems, function (val) {
                var sItem = _.trim(val);
                $(sItem).removeClass('is-active');
                if (!self.form.hasClass('is-active')) {
                    self.form.addClass('is-active');
                    if (self.form.get(0).tagName === 'SELECT') {
                        self.form.find('option').removeAttr('selected')
                            .filter('[value='+self.form.val()+']')
                            .attr('selected', true)
                    }
                }
            });
            if (self.form.get(0).tagName !== 'SELECT') {
                console.log('!== SELECT');
                _.each(aItems, function (val) {
                    var $sItem = $(_.trim(val));
                    if ($sItem.length) {
                        if ($sItem.get(0).tagName === 'SELECT') {
                            console.log('=== SELECT');
                            /*
                            $(sItem).find('option').removeAttr('selected')
                                .filter('[value=0]')
                                .attr('selected', true);
                            */
                            $sItem.find('option').prop("selected", false);
                        }
                    }
                });
            }
        }
    },

    afterDefault : function (result) {
        this.form.find('input, div').removeClass('__error');
        $('.text-error[data-name="error"]').empty();
    },

    before : function () {
        var sCallbacks = this.form.data('before');
        var goToAjax = true;
        if (sCallbacks !== undefined) {
            var self = this;
            if(sCallbacks) {
                var aCallbacks = _.split(sCallbacks, ',');
                _.each(aCallbacks, function (val) {
                    var sFuncName = _.trim(val);
                    if (_.isFunction(window['before-' + sFuncName])){
                        goToAjax = window['before-' + sFuncName](self.form);
                    }
                });
            }
        }
        return goToAjax;
    },

    beforeDefault : function () {

    },

    clearValidate: function () {
        this.form.find('.help.' + this.options.validation.errorClass).remove();
        this.form.find('input.' + this.options.validation.errorClass).removeClass(this.options.validation.errorClass);
    },
    validate : function () {
        var $required = this.form.find('input[required]');
        //var $required = this.form.find('[data-required="1"]');
        if (this.form.data('required-by-radio')) {
            var labelRadio = this.form.find('.label-radio.active');
            var self = this;
            $.each($required, function(key) {
                var $closestContainer = $(this).closest(self.form.data('required-by-radio-blocks'));
                if ($closestContainer.length) {
                    if ($closestContainer.data('id') !== labelRadio.data('id')) {
                        delete $required[key];
                    }
                }
            });
            $required = $required.filter(function(n){
                return n !== undefined;
            });
            console.log($required);
        }
        var bResult = true;
        $required.each(function (i, e) {
            var $elem = $(e);
            var sElemType = $elem.get(0).tagName;
            var bFilled = ($elem.is(':checkbox')) ? $elem.prop('checked') : $elem.val();
            if(!bFilled){
                $elem.addClass(this.options.validation.errorClass);
                bResult = false;
                switch (sElemType){
                    case 'SELECT':
                        if($elem.hasClass('combobox') || $elem.parent().hasClass('__combobox')){
                            $elem.closest('.form-group').addClass('__error');
                        }
                        if($elem.data('role') === 'chosen-select'){
                            $elem.closest('.form-group').addClass('__error');
                        }
                        break;
                    case 'INPUT':
                        if($elem.is(':checkbox')){
                            $elem.parent().addClass('__error');
                        }else{
                            $elem.addClass('__error');
                        }
                        break;
                    default:
                        $elem.addClass('__error');
                        break;
                }
            }
        });
        return bResult;
    },

    validateServer : function (result) {
        /**
         * Для валидации.
         */
        /*
        var self = this;
        _.each(result.responseJSON, function(field, index) {
            var input = self.form.find('.input[name="' + index + '"]');
            var message = field.responseJSON[index][0];
            input.addClass('is-danger');
            input.parent().append('<span class="help is-danger">' + message + '</span>');
        });
        */

        var resultJson = result.responseJSON;
        for (var key in resultJson) {
            var input = this.form.find('.input[name="' + key + '"]');
            input.addClass(this.options.validation.errorClass);
            var message;
            if (_.isArray(resultJson[key])) {
                message = resultJson[key][0];
            } else {
                message = resultJson[key];
            }
            input.parent().append(this.validationTemplate(message, false));
            //this.form.find('.text-error[data-name="' + key + '"]').text(result.responseJSON[key][0]);
            if (key === 'error') {
                this.showError(message);
            }
        }
    },

    getFormData : function () {
        var data = {};
        var name;
        if (this.tagName === 'FORM') {
            data = this.form.serializeArray();
        } else if(this.tagName === 'SELECT') {
            name = this.form.attr('name');
            data['' + name] = this.form.val();
        } else if(this.tagName === 'INPUT' && this.form.attr('type') === 'checkbox') {
            data = this.form.data();
            name = this.form.attr('name');
            if (this.form.is(':checked')) {
                data['' + name] = 1;
            } else {
                data['' + name] = 0;
            }
        } else if (this.tagName === 'A' && this.form.attr('value')) {
            name = this.form.attr('name');
            data['' + name] = this.form.attr('value');
        } else {
            data = this.form.data();
        }
        if(!data._token || data._token === undefined) {
            data._token = window.Laravel;
        }
        if (data['bs.tooltip'] !== undefined) {
            delete data['bs.tooltip'];
        }
        if (this.form.data('form-data')) {
            var sData = this.form.data('form-data');
            if(sData){
                sData = _.split(sData, ',');
                _.each(sData, function (val) {
                    var $element = $(_.trim(val));
                    if ($element.get(0).tagName === 'INPUT') {
                        if ($element.is(':checkbox')) {
                            data[$element.attr('name')] = $element.is(':checked') ? 1 : 0;
                        } else {
                            data[$element.attr('name')] = $element.val();
                        }
                    }
                    if ($element.get(0).tagName === 'SELECT') {
                        data[$element.attr('name')] = $element.val();
                    }
                    if ($element.get(0).tagName === 'FORM') {
                        _.each($element.serializeArray(), function (val) {
                            data[val.name] = val.value;
                        });
                    }
                });
            }
        }
        console.log(data);
        var $globalData = $('#global-data');
        if($globalData.length){
            var keyData = '_global-data';
            var out = '';
            var json = $.parseJSON($globalData.val());
            _.each(json, function(field, key) {
                out += '&' + keyData + '%5B' + key + '%5D=' + field;
            });
            data = data + out;
        }
        return data;
    },

    showError : function(sMessage) {
        this.form.prepend(this.validationTemplate(sMessage, true));
    },

    setItemError : function(item) {
        console.log(item);
        console.log('[data-item-id="' + item + '"]');
        this.form.find('[data-item-id="' + item + '"]').addClass('__error');
    },

    validationTemplate : function(text, center) {
        var classes = this.options.validation.helpClass + ' ' + this.options.validation.errorClass;
        if (center) {
            classes = classes + '  has-text-centered';
        }
        return '<span class="' + classes + '" style="margin-bottom: 10px;">' + text + '</span>';
    },

    ajaxInitPlugins : function() {
        var sInit = this.form.data('ajax-init');
        if (sInit) {
            var aInit = _.split(sInit, ',');
            _.each(aInit, function (val) {
                var sFuncName = _.trim(val);
                if (_.isFunction(window[sFuncName])){
                    window[sFuncName]();
                }
            });
        }
    }
};


$(document).ready(function () {
    ajaxForm.bind('.ajax-form', 'body');
    ajaxForm.bind('.ajax-link', 'body', 'click');
    ajaxForm.bind('.ajax-select', 'body', 'change');
    ajaxForm.bind('.ajax-search', 'body', 'keyup');
    ajaxForm.bind('.ajax-checkbox', 'body', 'change');

    $('body').on('click', 'button.inner-form-submit[type="submit"]', function (event) {
        event.preventDefault();
        $(this).closest('form.ajax-form').submit();
    });

    $('.radio-checked-group').on('change', 'input[data-role="radio-checkbox"]', function (event) {
        event.preventDefault();
        var isChecked = $(this).prop('checked');
        $('.radio-checked-group input[data-role="radio-checkbox"]').prop('checked', false);
        $(this).prop('checked', isChecked);
    });

    hiddenCheckboxUnchecked.init();
});


/**
 * Добавляет к неактивным чекбоксам  input[type="hidden"] с value="0", для активных удаляет его.
 *
 * @type {{each: hiddenCheckboxUnchecked.each, event: hiddenCheckboxUnchecked.event}}
 */
var hiddenCheckboxUnchecked = {

    settings: {
        value: 0,
        parent: '#RequestTab, #modal-parameters-request-add-new', // область видимости : selector, selector2
        selector: '.checkbox.__hidden-unchecked' // селектор перед чекбоксом input
    },

    init: function() {
        var aParents = _.split(this.settings.parent, ',');
        var self = this;
        _.each(aParents, function (val) {
            var parent = _.trim(val);
            $(parent).on('change', self.settings.selector + ' input[type="checkbox"]', function() {
                self.event($(this));
            });
        });
        this.each();
    },

    each: function() {
        var self = this;
        $(this.settings.selector + ' input[type="checkbox"]').each(function() {
            self.event($(this));
        });
    },

    event: function($element) {
        if (!$element.prop('checked')) {
            $element.append('<input type="hidden" name="' + $element.attr('name') + '" value="' + this.settings.value + '">');
        } else {
            $element.empty();
        }
    }
};


/**
 * Delay
 *
 *
 *  delay(function() {
        updateDatatableBySearch($element.val())
    }, 500);
 */
var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();