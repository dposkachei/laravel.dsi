window.$ = window.jQuery = require('jquery');

//import Tether from 'tether';
window.Tether = require('tether');

import Popper from 'popper.js';
window.Popper = Popper;


require('bootstrap');

require('dotdotdot');


require('respondjs/lib/respond.js');
window.skel = require('./package/assets/skel.min.js');

/**
 * -------------------------------------------
 * Lodash
 * -------------------------------------------
 *
 */
require('lodash/lodash.min.js');

window.Laravel = $('meta[name="csrf-token"]').attr('content');

$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});