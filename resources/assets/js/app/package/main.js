$(document).ready(function(){

	$('[rel="truncate"]').dotdotdot({
		ellipsis     : '… ',
		watch        : true,
		wrap         : 'letter',
		height: 300,
		lastCharacter: {
			remove: [ ' ', ',', ';', '.', '!', '?' ],
			noEllipsis: []
		},
	});

	$('a[rel="truncate-button"]').on('click', function(event) {
		event.preventDefault();
		var $element = $(this);
		if ($element.hasClass('truncate-open')) {
			$(this).removeClass('truncate-open');
			$('[rel="truncate"][data-name="' + $(this).data('name') + '"]').trigger("update.dot");
		} else {
			$(this).addClass('truncate-open');
			$('[rel="truncate"][data-name="' + $(this).data('name') + '"]').trigger("destroy.dot");
		}
	});

	console.log('on load');

	var tm_height = $('.menu-top').height() + 15;
	console.log('tm_height',tm_height);
	var z_mrg = 0;

	$(window).scroll(function(){
		var top = $(this).scrollTop();
		var elem = $('.top-line-wrp');
		var sb = $('#sidebar .toggle');
		if(top + z_mrg < tm_height){
			elem.removeClass('fxd');
			sb.removeClass('fxd_sb')
			elem.css('top',(tm_height - top));
		}else{
			elem.css('top',z_mrg);
			sb.addClass('fxd_sb');
			elem.addClass('fxd');
		}
	});

	$("#getBasket").on("show.bs.modal", function(e) {
		$('#getBasket').find('.dialog__loading').css('display', 'block');
		var target = $(e.relatedTarget);
		var action = target.data('action');
		var data = {};
		getBasketItems(action, data, $('#getBasket'));
	});
	$("#getBasket").on('hidden.bs.modal', function () {
		$('#getBasket .modal-table').empty();
	});
	$('body').on('click', '.show-modal-body', function() {
		var $form = $(this).closest('form');
		$form.find('.modal-body').removeClass('hidden');
		$(this).addClass('hidden');
		$form.find('.inner-form-submit').removeClass('hidden');
	});
    $('[data-toggle="tooltip"]').tooltip();
});

var getBasketItems = function(action, data, $target) {
	console.log(action, data, $target);
	$.ajax({
		url: action,
		type: "POST",
		data: data,
		success: function (result) {
			if (result.success) {
				$target.find('.dialog__loading').css('display', 'none');
				$target.find('.modal-table').html(result.view);

				var $count = $('.basket-count');
				if (result.count !== 0) {
					$count.text('(' + result.count + ')');
				} else {
					$count.text('');
				}
			} else {

			}

		},
		error: function (data, status, headers, config) {
			$target.find('.dialog__loading').css('display', 'none');
		}
	});
};
document.getBasketItems = getBasketItems;

