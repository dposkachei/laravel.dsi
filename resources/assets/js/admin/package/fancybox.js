/**
 * -------------------------------------------
 * Jquery fancybox
 * -------------------------------------------
 *
 */
require('jquery-fancybox/source/js/jquery.fancybox.pack.js');

$(document).ready(function() {
    //initFancybox();
});

var initFancybox = function() {
    $('.fancybox').fancybox({
        scrolling   : 'hidden',
        nextEffect  : 'none',
        prevEffect  : 'none',
        helpers: {
            overlay: {
                locked: false
            }
        },
        afterLoad: function() {
            var $fancyboxHeader = $('<div style="position: fixed; top: 5px; right: 70px;color: #fff; padding: 10px;"></div>');
            var $fancyboxNumberRight = $('<div style="text-align: right;">' + (this.index + 1) + ' из ' + this.group.length + '</div>');
            var $fancyboxImage = $('<div></div>');
            var $fancyboxName = $('<div></div>');

            var $fancyboxType = $('<div style="position: absolute; top: 0px; left: 100%; color: #fff; width: 100px; padding: 10px;"></div>');

            var $type = $('<div class="columns"><div class="column"><p><span class="icon"> <i class="fa fa-camera"></i> </span> <span>Canon PoserShot S95</span></p></div></div><div class="columns"><div class="column"><p><span class="icon"> <i class="fa fa-bolt"></i> </span> <span>10</span></p></div></div><div class="columns"><div class="column"><p><span class="icon"> <i class="fa fa-beer"></i> </span> <span>2l</span></p></div></div>');
            $fancyboxHeader
                .append( $fancyboxNumberRight )
                .append( $fancyboxImage )
                .append( $fancyboxName );

            $fancyboxType
                .append( $type );

            this.inner.prepend( $fancyboxHeader );
            //this.inner.prepend( $fancyboxType );
            // this.content = '<h1>2. My custom title</h1>' + this.content.html();
        },
        tpl: {
            error       : '<p class="fancybox-error">I want to display a different error meesage here.</p>',
            closeBtn    : '<a class="fancybox-item fancybox-close"></a>',
            //next        : '<svg class="nav__icon"><use xlink:href="#icon-triangle"></use></svg>',
            //prev        : '<svg class="nav__icon"><use xlink:href="#icon-triangle"></use></svg>'
            next        : '<a class="fancybox-nav fancybox-next"><span></span></a>',
            prev        : '<a class="fancybox-nav fancybox-prev"><span></span></a>'
        }
    });
};