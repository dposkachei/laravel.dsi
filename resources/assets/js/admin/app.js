/**
 * -------------------------------------------
 * Lodash
 * -------------------------------------------
 *
 */
require('lodash/lodash.min.js');

/**
 * -------------------------------------------
 * Bootstrap colorpicker
 * -------------------------------------------
 *
 */
require('bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js');

/**
 * -------------------------------------------
 * Jquery fancybox
 * -------------------------------------------
 *
 */
require('jquery-fancybox/source/js/jquery.fancybox.pack.js');

/**
 * -------------------------------------------
 * Jquery Blueimp File Uploader
 * -------------------------------------------
 *
 */
require('blueimp-file-upload/js/vendor/jquery.ui.widget.js');
require('blueimp-file-upload/js/jquery.iframe-transport.js');
require('blueimp-file-upload/js/jquery.fileupload.js');

/**
 * -------------------------------------------
 * Clipboard, copy text in area
 * -------------------------------------------
 *
 */
const Clipboard = require('clipboard/dist/clipboard.js');

new Clipboard('.btn-clipboard');

/**
 * -------------------------------------------
 * Mixitup, sort blocks
 * -------------------------------------------
 *
 */
const mixitup = require('mixitup');

if ($('.mixitup').length) {
    var config = {
        controls: {
            toggleDefault: 'all'
        },
        classNames: {
            block: '',
            elementFilter: 'is',
            modifierActive: 'primary'
        },
        selectors: {
            target: '.mix'
        }
    };
    var mixer = mixitup('.mixitup', config);

    mixer.toggleOn('.category-b')
        .then(function() {
            // Deactivate all active toggles

            //return mixer.toggleOff('.category-b')
        })
        .then(function(state) {
            console.log(state.activeFilter.selector); // 'all'
            console.log(state.totalShow); // 12
        });
}

/*
require('froala-editor/js/languages/ru.js');

require('froala-editor/js/plugins/char_counter.min.js');
require('froala-editor/js/plugins/link.min.js');
require('froala-editor/js/plugins/table.min.js');

$(function() { 
    $('.froala').froalaEditor({
        language: 'ru',
        heightMin: 200,
        heightMax: 400,
        charCounterCount: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', '-', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent', '-', 'insertImage', 'insertLink', 'insertFile', 'insertTable', 'clearFormatting', 'undo', 'redo']
    });
    $('.froala').on('froalaEditor.contentChanged', function (e, editor) {
        console.log('hi');
      $('#preview').html(editor.html.get());
    });
    $('.froala').on('froalaEditor.input', function (e, editor, inputEvent) {
      $('#preview').html(editor.html.get());
    });
}); 

*/
/**
 * -------------------------------------------
 * Tippy.js Tooltips
 * -------------------------------------------
 *
 */



var tippy = require('tippy.js/dist/tippy.min.js');

window['tippy-tooltip'] = function(str) {
    if (!str) {
        str = '';
    }
    /*
    var t = tippy(str + ' .tippy-html', {
        html: $(this).data('html'),
        animation: 'fade',
        interactive: true,
        theme: 'light'
    });
    */

    //t.destroyAll();

    console.log('tippy-tooltip');
    new tippy('.tippy', {
        arrow: true,
        animation: 'fade'
    });
    new tippy('.tippy-popover', {
        arrow: true,
        animation: 'fade',
        interactive: true,
        theme: 'light'
    });
};

window['tippy-tooltip']('body');


/**
 * -------------------------------------------
 * Simple color picker
 * -------------------------------------------
 *
 */

/*
const SimpleColorPicker = require('simple-color-picker');

var colorPicker = new SimpleColorPicker({
    color: '#FF0000',
    background: '#000000',
    el: document.body,
    width: 180,
    height: 180
});
$('.Scp').css({
    'width': '215px',
    'position': 'absolute',
    'top': '50%',
    'left': '50%'
});
colorPicker.appendTo(document.getElementById('app'));
colorPicker.setColor('#000000');
$('#app').css('background-color', colorPicker.getHexString());
colorPicker.on('update', function() {
    $('#app').css('background-color', colorPicker.getHexString());
});

*/



/**
 * -------------------------------------------
 * Sticky kit , прилипание меню
 * -------------------------------------------
 *
 */
require('sticky-kit/dist/sticky-kit.min.js');

$("#sticky-sidebar").stick_in_parent();

/**
 * -------------------------------------------
 * Move To
 * -------------------------------------------
 *
 * <a href="#app" class="js-trigger" data-mt-duration="300">Trigger</a>
 */
const MoveToPlugin = require('moveto/dist/moveTo.min.js');
const moveTo = new MoveToPlugin();

var trigger = document.getElementsByClassName('js-trigger')[0];
moveTo.registerTrigger(trigger);

/**
 * -------------------------------------------
 * Append laravel token
 * -------------------------------------------
 *
 */
window.Laravel = $('meta[name="csrf-token"]').attr('content');

$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});



/*
import Echo from 'laravel-echo'

const Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'a6e9846eb5184ba192b3',
    cluster: "eu"
});

console.log(window.user);
if (typeof window.user.id !== 'undefined') {
    window.Echo.private('App.Models.User.' + window.user.id)
        .notification((notification) => {
            console.log(notification.type);
    });
}

window.Echo.channel('chat-room').listen('.event', (data) => {
    console.log(data.message + ' 1');
});
*/
/*
Pusher.logToConsole = true;

var pusher = new Pusher('a6e9846eb5184ba192b3', {
    cluster: 'eu',
    encrypted: true
});

var channel = pusher.subscribe('chat-room');
channel.bind('event', function(data) {
    console.log(data.message);
});
*/



//const DG = require('2gis-maps');

//require('jquery-lazyload/jquery.lazyload.js');
//require('script!../resources/assets/js/bootstrap.js');
//require('layzr.js/dist/layzr.js');










/**
 * 2 GIS MAP
 */
/*
var map = DG.map('map', {
    'center': [54.98, 82.89],
    'zoom': 13
});
DG.control.location({position: 'bottomright'}).addTo(map);
DG.control.scale().addTo(map);
DG.control.ruler({position: 'bottomleft'}).addTo(map);
DG.contro
*/