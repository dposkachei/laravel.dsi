<?php

return [

    'options' => [
        "tapToDismiss"=> true,
        "toastClass"=> 'toast',
        "containerId"=> 'toast-container',
        "debug"=> false,

        "showMethod"=> 'fadeIn', //fadeIn, slideDown, and show are built into jQuery
        "showDuration"=> 300,
        "showEasing"=> 'swing', //swing and linear are built into jQuery
        "onShown"=> 'undefined',
        "hideMethod"=> 'fadeOut',
        "hideDuration"=> 1000,
        "hideEasing"=> 'swing',
        "onHidden"=> 'undefined',

        "extendedTimeOut"=> 1000,
        "iconClasses"=> [
            "error"=> 'toast-error',
            "info"=> 'toast-info',
            "success"=> 'toast-success',
            "warning"=> 'toast-warning'
        ],
        "iconClass"=> 'toast-info',
        "positionClass"=> 'toast-top-right',
        "timeOut" => 5000, // Set timeOut and extendedTimeOut to 0 to make it sticky
        "titleClass"=> 'toast-title',
        "messageClass"=> 'toast-message',
        "target"=> 'body',
        "closeHtml"=> '<a class="ns-close icon" ><i class="fa fa-times"></i></a>',
        "newestOnTop"=> true,
        "preventDuplicates"=> false,
        'closeOnHover' => false,
    ]

    /*
    'options' => [

        "toastClass" => 'ns-box-inner',
        "containerId" => 'ns-growl',
        "iconClasses" => [
            "error" => 'is-error',
            "info" => 'is-info',
            "success" => 'is-primary',
            "warning" => 'is-warning'
        ],
        "iconClass" => 'toast-info',
        "titleClass" => 'ns-box-title',
        "messageClass" => 'ns-box-message',
        "closeHtml" => '<span class="ns-close"></span>',
        "closeClass" => 'ns-close',
        "closeButton" => true,
        'tapToDismiss' => false,

        "progressBar" => false,
        "positionClass" => 'ns-box ns-growl ns-effect-scale ns-type-notice ns-show is-left',
        "preventDuplicates"=> false,
        "showDuration" => 0,
        "hideDuration" => 0,
        "timeOut" => 5000,
        "extendedTimeOut" => 0,
        "showEasing" => "swing",
        "hideEasing"=> "linear",
        "showMethod" => "fadeIn",
        "hideMethod" => "fadeOut",

        'closeOnHover' => false,

    ]
    */
];