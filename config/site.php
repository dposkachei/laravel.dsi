<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Roles for project
    |--------------------------------------------------------------------------
    |
    | Usage for seeding column roles in /database/seeds/...
    |
    */
    'roles' => [
        [
            'slug' => 'admin',
            'name' => 'Администратор'
        ],
        [
            'slug' => 'user',
            'name' => 'Пользователь'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Site Layout
    |--------------------------------------------------------------------------
    | app admin
    |
    */

    'layout' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Меню для сайдбара у парнера и у админа
    | Меню кэшируется
    |--------------------------------------------------------------------------
    */
    'menu' => [
        'partner' => [
            'dashboard'         => [ 'title' => 'Главная',          'iconCls' => 'icon-menu-main'       ],
            'statistic'         => [ 'title' => 'Статистика',       'iconCls' => 'icon-menu-statistics' ],
            'cashback'          => [ 'title' => 'Кэшбэк',           'iconCls' => 'icon-menu-rouble'     ],
            'finance'           => [ 'title' => 'Финансы',          'iconCls' => 'icon-menu-wallet'     ],
            'branches'          => [ 'title' => 'Филиалы',          'iconCls' => 'icon-menu-branches'   ],
            'help'              => [ 'title' => 'Помощь',           'iconCls' => 'icon-menu-help'       ],
        ],
        'admin' => [
            'dashboard'         => [ 'title' => 'Главная',          'iconCls' => 'icon-menu-main'       ],
            'finance'           => [ 'title' => 'Финансы',          'iconCls' => 'icon-menu-wallet'     ],
            'payments'          => [ 'title' => 'История покупок',  'iconCls' => 'icon-menu-wallet'     ],
            'clients'           => [ 'title' => 'Клиенты',          'iconCls' => 'icon-menu-wallet'     ],
            'companies'         => [ 'title' => 'Компании',         'iconCls' => 'icon-menu-wallet'     ],
            'moderation'        => [ 'title' => 'Модерация',        'iconCls' => 'icon-menu-wallet'     ],
            'categories'        => [ 'title' => 'Категории',        'iconCls' => 'icon-menu-statistics' ],
            'cities'            => [ 'title' => 'Города',           'iconCls' => 'icon-menu-wallet'     ],
            'articles'          => [ 'title' => 'Список статей',    'iconCls' => 'icon-menu-wallet'     ],
            'administrators'    => [ 'title' => 'Администраторы',   'iconCls' => 'icon-menu-wallet'     ],
        ]
    ],

    'options' => [
        'app' => [
            'title' => 'dsi-ug.ru',
            'description' => '',
            'keywords' => '',
            'favicon' => '/favicon.png',
            'copyright' => '© ООО "ДорСтройИндустрия"',
        ],
        'contacts' => [
            'phone' => '+7 (863) 229-59-74',
            'address' => '344019, г. Ростов-на-Дону, ул. 17 линия 2/8',
            'email' => 'dsi@dsi-ug.ru',
        ],
    ]

];