<?php

return [
    /*
     * This is the class responsible for providing the URLs which must be redirected.
     * The only requirement for the redirector is that it needs to implement the
     * `Spatie\MissingPageRedirector\Redirector\Redirector`-interface
     */
    'redirector' => \Spatie\MissingPageRedirector\Redirector\ConfigurationRedirector::class,

    /*
     * When using the `ConfigurationRedirector` you can specify the redirects in this array.
     * You can use Laravel's route parameters here.
     */
    'redirects' => [
        '/users'            => '/user',
        '/contact.html'     => '/contacts',

        '/cp123.html'       => '/product/101/komplekt-sharov-dlya-polochnogo-barabana-kp-123',
        '/3fb40.html'       => '/product/243/nasadka-k-forme-3fb-40-trekhsektsionnaya',
        '/analit1.html'     => '/',
        '/armogrunt.html'   => '/product/2/geosetka-armogrunt-dlya-armirovaniya-slabykh-gruntovykh-osnovaniy',

        '/itpmg.html'       => '/product/235/izmeritel-teploprovodnosti-itp-mg4',
        '/labln.html'       => '/',
        '/985a.html'        => '/product/233/bachok-985a-dlya-ispytaniy-tsementa-s-elektronagrevom',
        '/acom.html'        => '/',
        '/cmj539m.html'     => '/product/229/vibroploshchadka-laboratornaya-smzh-539m-1',
        '/termometr.html'   => '/product/195/termometr-bimetallicheskiy-dlya-kontrolya-tverdeniya-betona',
        '/vt1.html'         => '/product/182/vanna-termostat-vt-1',
        '/File/armdorrek1.doc' => '/',
        '/File/cd-akt.pdf'  => '/',
        '/citocznc.html'    => '/product/197/sito-sts-ns',
        '/dkbf.html'        => '/product/191/dilatometricheskiy-kompleks-beton-frost',
        '/kp6012.html'      => '/product/117/koltsa-kalibry-dlya-shchebnya-graviya-kp-6012',
        '/plbppn40.html'    => '/product/241/plastiny-plb-ppn40',
        '/prnilolsk.html'   => '/product/62/sdvigovoy-pribor-nikolskogo-komplekt-505-714-mm',
        '/puhina.html'      => '/product/90/izmeritel-puchinistosti-grunta-upg-mg4-grunt',
        '/pvk.html'         => '/',

        '/dad1.html'        => '/product/4/adgezionnaya-dobavka-dad-1-k-neftyanym-bitumam',
        '/cd1.html'         => '/product/5/stabiliziruyushchaya-dobavka-dlya-shchma-stilobit',
        '/armdor.html'      => '/product/1/geosetka-armogrunt-dlya-armirovaniya-slabykh-gruntovykh-osnovaniy',
        //'/armdor.html'      => '/product/2/geosetka-armogrunt-dlya-armirovaniya-slabykh-gruntovykh-osnovaniy',



        '/serCT.html'       => '/',
        '/upp10.html'       => '/product/173/ustroystvo-upp-10',
        '/uri.html'         => '/product/200/prisposoblenie-uri-dlya-ispytaniya-na-rastyazhenie-pri-izgibe-betonnykh-balochek',
        '/SerPW.html'       => '/',
        '/cp232.html'       => '/product/34/ustroystvo-dlya-kontrolya-geometricheskikh-parametrov-dorog-kp-232',
        '/konuspgr.html'    => '/product/247/konus-pgr-konus-stroy-tsnil',///product/248/konus-pgr-konus-stroy-tsnil
        '/kp133.html'       => '/product/240/poromer-obemomer-kp-133',
        '/mmp.html'         => '/',//Модификатор минерального порошка
        '/odo.html'         => '/product/91/odometr-60-model-5a',
        '/pvn.html'         => '/product/78/pribor-pvn',
        '/tvo.html'         => '/product/154/apparat-tvo-dlya-opredeleniya-temperatury-vspyshki-v-otkrytom-tigle',
        '/ctrldor.html'     => '/',//Контроль параметров дорог
        '/geol.html'        => '/product/112/ruchnoy-burovoy-komplekt-geologa',
        '/984pk.html'       => '/product/152/penetrometr-dlya-bituma-984-pk',
        '/iaz04m.html'      => '/product/223/pribor-iats-04m',
        '/pulsar11.html'    => '/product/266/ultrazvukovoy-pribor-ultrazvukovoy-tester-pulbsar-11',
        '/serST.html'       => '/',
        '/2fk100.html'      => '/product/218/forma-kuba-2fk-100',
        '/SerSI.html'       => '/',//Платформенные весы серии SI-100W
        '/fk100.html'       => '/product/245/nasadka-dlya-formy-kuba-fk-100',
        '/izxp.html'        => '/product/23/izmeritel-koeffitsienta-stsepleniya-iksp',
        '/kp131.html'       => '/product/115/komplekt-sit-kp-131-dlya-gruntov',
        '/labhj.html'       => '/',//Лабораторные весы серии HJ
        '/p100.html'        => '/product/134/mashina-razryvnaya-r-100',
        '/pehat.html'       => '/',//Весы с печатью этикеток
        '/pv300.html'       => '/product/238/pribor-vika-pv-300',
        '/zementprognoz.html' => '/',//Цемент-прогноз для определения активности цемента
        '/File/armdorakt2.doc' => '/',
        '/vl1ut.html'       => '/product/43/vibroploshchadka-vl-1ut',
        '/vgt220.html'      => '/product/146/vibrogrokhot-vgt-220-s-taymerom-i-ustroystvom-krepleniya-sit-vysotoy-do-800-mm',
        '/pg7.html'         => '/product/105/obolochka-elastichnaya-dlya-ballonykh-plotnomerov-pbd-km-i-pg-7',
        '/t3.html'          => '/product/204/pribor-t-3-tovarova',
        '/uniplast1.html'   => '/',
        '/bg.html'          => '/product/88/byuksa-gruntovaya-bg',
        '/bitum.html'       => '/',
        '/asfaltbeton.html' => '/',
        '/praimer.html'     => '/product/279/praymer-bitumnyy',
        '/vims21.html'      => '/product/122/vlagomery-stroymaterialov-vims-221-vims-222-vims-223',
        '/vzriv.html'       => '/product/477/vzryvobezopasnye-platformennye-vesy-serii-gzii-b-i-gzh-b',
        '/okf.html'         => '/product/108/pribor-dlya-opredeleniya-koeffitsienta-filtratsii-gruntov-okf',
        '/yachik.html'      => '/product/129/yashchik-mernyy-100-l',
        '/cp231.html'       => '/product/30/reyka-dorozhnaya-kp-231',



//        '/non-existing-page' => '/existing-page',
//        '/old-blog/{url}' => '/new-blog/{url}',
    ],

];
