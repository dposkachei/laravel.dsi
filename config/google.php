<?php
return [
    /**
     * View ID can be found in `http://google.com/analytics` under the `Admin` tab on navigation.
     *
     * Select `Account`, `Property` and `View`. You will see a `View Settings` link.
     */
    'analytics' => [
        'viewId' => 'ga:149326232',
    ],
];