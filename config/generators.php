<?php

return [

    /*
    |--------------------------------------------------------------------------
    | The singular resource words that will not be pluralized
    | For Example: $ php artisan generate:resource admin.bar
    | The url will be /admin/bars and not /admins/bars
    |--------------------------------------------------------------------------
    */

    'reserve_words' => ['app', 'website', 'admin'],

    /*
    |--------------------------------------------------------------------------
    | The default keys and values for the settings of each type to be generated
    |--------------------------------------------------------------------------
    */

    'defaults' => [
        'namespace'           => '',
        'path'                => '.app/',
        'prefix'              => '',
        'postfix'             => '',
        'file_type'           => '.php',
        'dump_autoload'       => false,
        'directory_format'    => '',
        'directory_namespace' => false,
    ],

    /*
    |--------------------------------------------------------------------------
    | Types of files that can be generated
    |--------------------------------------------------------------------------
    */
    /*
    'settings' => [
        'view'       => ['path' => './resources/views/', 'file_type' => '.blade.php', 'directory_format' => 'strtolower', 'directory_namespace' => true],
        'model'      => [
            'namespace' => '\Models',
            'path' => './app/Models/'
        ],
        'controller' => [
            'namespace'           => '\Http\Controllers',
            'path'                => './app/Http/Controllers/',
            'postfix'             => 'Controller',
            'directory_namespace' => true,
            'dump_autoload'       => true
        ],
        'seed'       => ['path' => './database/seeds/', 'postfix' => 'TableSeeder'],
        'migration'  => ['path' => './database/migrations/'],
    ],
    */

    /*
    |--------------------------------------------------------------------------
    | Custom
    |--------------------------------------------------------------------------
    */

    'settings' => [
        'view'       => [
            'path' => './resources/views/',
            'file_type' => '.blade.php',
            'directory_format' => 'strtolower',
            'directory_namespace' => true
        ],
        'model'      => [
            'namespace' => '\Models',
            'path' => './app/Cmf/Project/',
            'postfix' => 'SettingsTrait',
        ],
        'controller' => [
            'namespace'           => '\Http\Controllers',
            'path'                => './app/Cmf/Project/',
            'postfix'             => 'Controller',
            'directory_namespace' => true,
            'dump_autoload'       => true
        ],
        'seed'       => [
            'path' => './app/Cmf/Project/',
            'postfix' => 'SeoProvider'
        ],
        'migration'  => ['path' => './database/migrations/'],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resource Views [stub_key | name of the file]
    |--------------------------------------------------------------------------
    */

    'resource_views' => [
        'view_index'    => 'index',
        'view_add_edit' => 'add_edit',
        'view_show'     => 'show',

        'view_table'                => 'table',
        'view_modal_create'         => 'create',
        'view_modal_edit'           => 'edit',
        'view_modal_images'         => 'images',
        'view_modal_images_block'   => 'images.block',
        'view_modal_files'          => 'files',
        'view_modal_files_block'    => 'files.block',
    ],

    /*
    |--------------------------------------------------------------------------
    | Where the stubs for the generators are stored
    |--------------------------------------------------------------------------
    */

    'example_stub'          => base_path() . '/resources/stubs/example.stub',
    //'model_stub'            => base_path() . '/resources/stubs/model.stub',
    'model_plain_stub'      => base_path() . '/resources/stubs/model.plain.stub',
    'migration_stub'        => base_path() . '/resources/stubs/migration.stub',
    'migration_plain_stub'  => base_path() . '/resources/stubs/migration.plain.stub',
    //'controller_stub'       => base_path() . '/resources/stubs/controller.stub',
    'controller_plain_stub' => base_path() . '/resources/stubs/controller.plain.stub',
    //'controller_plain_stub' => base_path() . '/resources/stubs/cmf.settings.stub',
    'pivot_stub'            => base_path() . '/resources/stubs/pivot.stub',
    //'seed_stub'             => base_path() . '/resources/stubs/seed.stub',
    'seed_plain_stub'       => base_path() . '/resources/stubs/seed.plain.stub',
    'view_stub'             => base_path() . '/resources/stubs/view.stub',
    //'view_index_stub'       => base_path() . '/resources/stubs/view.index.stub',
    'view_add_edit_stub'    => base_path() . '/resources/stubs/view.add_edit.stub',
    'view_show_stub'        => base_path() . '/resources/stubs/view.show.stub',
    'schema_create_stub'    => base_path() . '/resources/stubs/schema-create.stub',
    'schema_change_stub'    => base_path() . '/resources/stubs/schema-change.stub',

    'model_stub'            => base_path() . '/resources/stubs/cmf.settings.stub',
    'controller_stub'       => base_path() . '/resources/stubs/cmf.controller.stub',
    'seed_stub'             => base_path() . '/resources/stubs/cmf.seo.stub',
    'custom_stub'           => base_path() . '/resources/stubs/cmf.custom.stub',
    'view_index_stub'       => base_path() . '/resources/stubs/cmf.view.index.stub',
    'view_table_stub'       => base_path() . '/resources/stubs/cmf.view.table.stub',
    'view_modal_create_stub'=> base_path() . '/resources/stubs/cmf.view.modal.create.stub',
    'view_modal_edit_stub'  => base_path() . '/resources/stubs/cmf.view.modal.edit.stub',
    'view_modal_images_stub'  => base_path() . '/resources/stubs/cmf.view.modal.images.stub',
    'view_modal_images_block_stub'  => base_path() . '/resources/stubs/cmf.view.modal.images.block.stub',
    'view_modal_files_stub'  => base_path() . '/resources/stubs/cmf.view.modal.files.stub',
    'view_modal_files_block_stub'  => base_path() . '/resources/stubs/cmf.view.modal.files.block.stub',

];
