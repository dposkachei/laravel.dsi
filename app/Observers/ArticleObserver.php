<?php

namespace App\Observers;


use App\Events\ChangeCacheEvent;
use App\Models\Article;

class ArticleObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  Article  $oArticle
     * @return void
     */
    public function saved (Article $oArticle)
    {
        event(new ChangeCacheEvent('article'));
        event(new ChangeCacheEvent('articles'));
    }
}