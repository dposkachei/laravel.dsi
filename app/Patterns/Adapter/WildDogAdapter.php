<?php

namespace App\Patterns\Adapter;


class WildDogAdapter implements Lion
{
    /**
     * $wildDog = new WildDog();
     * $wildDogAdapter = new WildDogAdapter($wildDog);

     * $hunter = new Hunter();
     * $hunter->hunt($wildDogAdapter);
     */


    protected $dog;

    public function __construct(WildDog $dog)
    {
        $this->dog = $dog;
    }

    public function roar()
    {
        $this->dog->bark();
    }





}