<?php

namespace App\Patterns\Adapter;


interface Lion
{
    public function roar();
}