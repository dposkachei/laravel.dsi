<?php

namespace App\Patterns\Facade;


class ComputerFacade
{
    /**
     * use
     * $computer = new ComputerFacade(new Computer());
     * $computer->turnOn(); // Ouch! Beep beep! Loading.. Ready to be used!
     * $computer->turnOff(); // Bup bup buzzz! Haah! Zzzzz
     */
    protected $computer;

    public function __construct(Computer $computer)
    {
        $this->computer = $computer;
    }

    public function turnOn()
    {
        $this->computer->getElectricShock();
        $this->computer->makeSound();
        $this->computer->showLoadingScreen();
        $this->computer->bam();
    }

    public function turnOff()
    {
        $this->computer->closeEverything();
        $this->computer->pullCurrent();
        $this->computer->sooth();
    }
}