<?php

namespace App\Patterns\Singleton;


final class President
{
    /**
     * use
     * $president1 = President::getInstance();
     * $president2 = President::getInstance();

     * var_dump($president1 === $president2); // true
     */


    private static $instance;

    private function __construct()
    {
        // Hide the constructor
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __clone()
    {
        // Disable cloning
    }

    private function __wakeup()
    {
        // Disable unserialize
    }
}