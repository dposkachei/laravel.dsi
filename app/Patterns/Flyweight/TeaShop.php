<?php

namespace App\Patterns\Flyweight;


class TeaShop
{
    /**
     * use
     * $teaMaker = new TeaMaker();
     * $shop = new TeaShop($teaMaker);

     * $shop->takeOrder('less sugar', 1);
     * $shop->takeOrder('more milk', 2);
     * $shop->takeOrder('without sugar', 5);

     * $shop->serve();
     * // Serving tea to table# 1
     * // Serving tea to table# 2
     * // Serving tea to table# 5
     */

    protected $orders;
    protected $teaMaker;

    public function __construct(TeaMaker $teaMaker)
    {
        $this->teaMaker = $teaMaker;
    }

    public function takeOrder(string $teaType, int $table)
    {
        $this->orders[$table] = $this->teaMaker->make($teaType);
    }

    public function serve()
    {
        foreach ($this->orders as $table => $tea) {
            echo "Serving tea to table# " . $table;
        }
    }
}