<?php

namespace App\Patterns\Composite;


interface Employee
{
    public function __construct(string $name, float $salary);
    public function getName();
    public function setSalary(float $salary);
    public function getSalary();
    public function getRoles();
}