<?php

namespace App\Patterns\Composite;


class Organization
{
    /**
     * // Prepare the employees
     * $john = new Developer('John Doe', 12000);
     * $jane = new Designer('Jane', 10000);

     * // Add them to organization
     * $organization = new Organization();
     * $organization->addEmployee($john);
     * $organization->addEmployee($jane);

     * echo "Net salaries: " . $organization->getNetSalaries(); // Net Salaries: 22000
     */


    protected $employees;

    public function addEmployee(Employee $employee)
    {
        $this->employees[] = $employee;
    }

    public function getNetSalaries()
    {
        $netSalary = 0;

        foreach ($this->employees as $employee) {
            $netSalary += $employee->getSalary();
        }

        return $netSalary;
    }
}