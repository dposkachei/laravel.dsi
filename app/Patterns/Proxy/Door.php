<?php

namespace App\Patterns\Proxy;


interface Door
{
    public function open();
    public function close();
}