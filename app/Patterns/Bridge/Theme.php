<?php

namespace App\Patterns\Bridge;


interface Theme
{
    public function getColor();
}