<?php

namespace App\Patterns\Bridge;


class DarkTheme implements Theme
{
    public function getColor()
    {
        return 'Dark Black';
    }
}