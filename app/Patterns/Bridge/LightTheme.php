<?php

namespace App\Patterns\Bridge;


class LightTheme implements Theme
{
    public function getColor()
    {
        return 'Off white';
    }
}