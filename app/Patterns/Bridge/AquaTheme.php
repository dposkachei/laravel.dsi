<?php

namespace App\Patterns\Bridge;


class AquaTheme implements Theme
{
    public function getColor()
    {
        return 'Light blue';
    }
}