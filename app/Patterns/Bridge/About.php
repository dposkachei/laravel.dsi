<?php

namespace App\Patterns\Bridge;


class About implements WebPage
{
    /**
     * use
     * $darkTheme = new DarkTheme();

     * $about = new About($darkTheme);
     * $careers = new Careers($darkTheme);

     * echo $about->getContent(); // "About page in Dark Black";
     * echo $careers->getContent(); // "Careers page in Dark Black";
     */



    protected $theme;

    public function __construct(Theme $theme)
    {
        $this->theme = $theme;
    }

    public function getContent()
    {
        return "About page in " . $this->theme->getColor();
    }
}