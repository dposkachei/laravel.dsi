<?php

namespace App\Patterns\Bridge;


interface WebPage
{
    public function __construct(Theme $theme);
    public function getContent();
}