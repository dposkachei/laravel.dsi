<?php

namespace App\Patterns\Factory\Simple;

class DoorFactory
{
    /**
     * use
     * $door = DoorFactory::makeDoor(100, 200);
     * echo 'Width: ' . $door->getWidth();
     * echo 'Height: ' . $door->getHeight();
     * @param $width
     * @param $height
     * @return WoodenDoor
     */
    public static function makeDoor($width, $height)
    {
        return new WoodenDoor($width, $height);
    }

}