<?php

namespace App\Patterns\Factory\Method;


interface Interviewer
{
    public function askQuestions();
}