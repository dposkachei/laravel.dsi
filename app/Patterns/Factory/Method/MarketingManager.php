<?php

namespace App\Patterns\Factory\Method;


class MarketingManager extends HiringManager
{
    /**
     * use
     * $devManager = new DevelopmentManager();
     * $devManager->takeInterview(); // Output: Asking about design patterns
     *
     * $marketingManager = new MarketingManager();
     * $marketingManager->takeInterview(); // Output: Asking about community building.
     * @return CommunityExecutive
     */
    public function makeInterviewer()
    {
        return new CommunityExecutive();
    }
}