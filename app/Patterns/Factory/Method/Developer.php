<?php

namespace App\Patterns\Factory\Method;


class Developer implements Interviewer
{
    public function askQuestions()
    {
        echo 'Asking about design patterns!';
    }
}