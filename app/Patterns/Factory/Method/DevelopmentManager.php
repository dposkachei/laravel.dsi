<?php

namespace App\Patterns\Factory\Method;


class DevelopmentManager extends HiringManager
{
    public function makeInterviewer()
    {
        return new Developer();
    }
}