<?php

namespace App\Patterns\Factory\Method;


abstract class HiringManager
{
    // Factory method
    abstract public function makeInterviewer();

    public function takeInterview()
    {
        $interviewer = $this->makeInterviewer();
        $interviewer->askQuestions();
    }
}