<?php

namespace App\Patterns\Factory\Method;


class CommunityExecutive implements Interviewer
{
    public function askQuestions()
    {
        echo 'Asking about community building';
    }
}