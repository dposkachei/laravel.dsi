<?php

namespace App\Registries;

class MemberRegistry
{
    private static $instance = null;
 
    private $registry = array();
    
    private $aMemberData = array();
    
    public $nAgentId = 0;

    /**
     * MemberRegistry constructor.
     */
    private function __construct(){ /* ... @return MemberRegistry */ }

    private function __clone()    { /* ... @return MemberRegistry */ }  

    private function __wakeup()   { /* ... @return MemberRegistry */ }

    /**
     * @return MemberRegistry|null
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }
 
    public function set($key, $object)
    {
        $this->registry[$key] = $object;
    }
 
    public function get($key, $default = null)
    {
        $mResult = array_get($this->registry, $key, $default);
        return (is_array($mResult) && !$default) ? collect($mResult) : $mResult;
    }
    
    public function getUser()
    {
        return array_get($this->registry, 'user');
    }
    
    public function getRole()
    {
        return $this->get('role.slug', 'quest');
    }

    public function hasRole($sRole)
    {
        return ($sRole === $this->get('role.slug'));
    }

    public function getPerson($sFieldName = '')
    {
        return (!$sFieldName) 
                ? collect( array_get($this->registry, 'person') )
                : array_get($this->registry, 'person.'.$sFieldName);
    }
    
    public function getCompany($sFieldName = '')
    {
        return (!$sFieldName)
            ? collect (array_get($this->registry, 'company') )
            : array_get($this->registry, 'company.'.$sFieldName);
    }

    
    public function getMember()
    {
        return collect($this->registry);
    }

    public function setMember($aMemberData)
    {
        $this->aMemberData = $aMemberData;        
        foreach($aMemberData as $key=>$value) {
            $this->set($key, $value);
        }
        //$this->nAgentId = $this->get('agent.id');
    }
}
