<?php

namespace App\Registries;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth as Sentinel;
//use Sentinel;
use Illuminate\Support\Facades\Config;

class Member
{
    /**
     * Пользователь с которым работает данный экземпляр класса
     * @var User
     */
    public $user;
    /**
     * @var integer
     */
    public $nUserId;
    /**
     * Роль пользователя
     * @var object
     */
    private $role;

    /**
     * Member constructor.
     * @param User $user
     */
    public function __construct()
    {
        $this->user = Sentinel::user();
        $this->role = Cache::tags('members')->remember('user_'.$this->user->id.'-role', 600, function () {
           return $this->user->id;//->toArray();
        }); 
    }

    /**
     * Возвращает экземпляр класса для текущего авторизованного юзера.
     * @return Member
     */
    public static function current()
    {
        $user = Sentinel::getUser();
        $self = new self($user);
        return $self;
    }

    /**
     * Возвращает экземпляр класса для юзера по id
     * @param $nId
     * @return Member
     */
    public static function getByUser($nId)
    {
        $user = Sentinel::getUser($nId);
        $self = new self($user);
        return $self;
    }


    /**
     * Возвращает всю информацию о пользователе данного экземпляра класса
     * @return \Illuminate\Support\Collection
     */
    public function get()
    {
        if (Config::get('app.no_member_cache') == true) {
            $aMember = $this->getMember();
        } else {
            $aMember = $this->getMember();
            /*
            $aMember = Cache::tags('members')->remember('user_'.$this->user->id, 60, function () {
                return $this->getMember();
            });
            */
        }
        return collect($aMember);
    }

    /**
     * собирает и возращает информацию о пользователе по id
     * @return array
     */
    private function getMember()
    {
        $aResult = $this->user->toArray();
        return $aResult;
    }

    /**
     * меню для разных пользователей в зависимости от типа.
     * @param string $sMenuType
     * @return array|mixed
     */
    public function getUserMenu($sMenuType = '')
    {
        $sRoleName = $this->role['slug'];
        $aMenu = [
            'partner'=>[],
            'client'=>[],
            'admin'=>[]
        ];
        switch($sRoleName) {
            case 'partner':
                $aMenu['partner'] = Config::get('lepta.menu.partner');
                break;
            case 'client':

                break;
            case 'admin':
                $aMenu['admin'] = Config::get('lepta.menu.admin');
                break;
            case 'cashier':

                break;
            default :
                break;
        }
        return ($sMenuType) ? array_get($aMenu, $sMenuType, []) : $aMenu;
    }

    
    
}
