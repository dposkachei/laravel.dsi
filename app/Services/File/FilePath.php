<?php

namespace App\Services\File;

use App\Services\File\FileService;
use Illuminate\Support\Facades\File;

class FilePath extends FileService
{

    /**
     * Изображение по умолчанию
     * @return string
     */
    public function default()
    {
        return asset('img/cover.png');
    }

    public function image($key, $size, $model, $filter = null)
    {
        if (is_null($model)) {
            return $this->default();
        }
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return $this->default();
        }
        return asset('files/'.$key.'/'.$model->fileable_id.'/'.$size.'/'.$filename);
    }

    public function check($key, $size, $model, $filter = null)
    {
        if (is_null($model)) {
            return false;
        }
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return false;
        }
        if (!File::exists(public_path('files/'.$key.'/'.$model->fileable_id.'/'.$size.'/'.$filename))) {
            return false;
        }
        return true;
    }
}