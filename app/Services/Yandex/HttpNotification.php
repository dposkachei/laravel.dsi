<?php

namespace App\Services\Yandex;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Services\Cashier;

class HttpNotification extends Cashier
{
    private $aRecParams = [
        'notification_type',
        'operation_id',
        'amount',
        'currency',
        'datetime',
        'sender',
        'codepro',
        'notification_secret',
        'label'
    ];

    public function genSign($aParams)
    {
        $aParams['notification_secret'] = $this->notification_secret;
        $strParams = implode('&', array_values($aParams));
        return sha1($strParams);
    }

    public function check($aRequest){
        $aParams = $this->getRecParams($aRequest);
        $nPaymentId = (!empty($aRequest['label'])) ? $aRequest['label'] : null;
        if (!isset($aRequest['sha1_hash'])) {
            return $this->error($nPaymentId, 'error sha1_hash not found');
        }
        if ($aRequest['sha1_hash'] !== $this->genSign($aParams)) {
            return $this->error($nPaymentId, 'error sha1_hash');
        }
        if ($aRequest['codepro'] !== 'false') {
            return $this->error($nPaymentId, 'error codepro');
        }
        if (is_null($nPaymentId)) {
            return $this->error($nPaymentId, 'error payment id');
        }
        $oPayment = $this->getPayment($nPaymentId, 0);
        if (is_null($oPayment)) {
            return $this->error($nPaymentId, 'error find payment');
        }
        if (floatval($oPayment->price) === floatval($aRequest['withdraw_amount'])) {
            $this->log($nPaymentId, $aRequest);
            return [
                'payId'     => $nPaymentId,
                'success'   => true,
                'result'    => '200',

            ];
        } else {
            return $this->error($nPaymentId, 'error price and withdraw_amount');
        }
    }

    public function error($nPaymentId, $message)
    {
        Log::info('--------- Http Yandex Notification Error ----------');
        Log::info('Payment id: '.$nPaymentId);
        Log::info($message);
        Log::info('--------- Http Yandex Notification Error ----------');
        return [
            'payId'     => $nPaymentId,
            'success'   => false,
            'result'    => '400',
            'message'   => $message
        ];
    }

    private function getRecParams($aRequest)
    {
        $aResult = [];
        foreach ($this->aRecParams as $aRecParam) {
            $aResult[$aRecParam] = (!empty($aRequest[$aRecParam])) ? $aRequest[$aRecParam] : '';
        }
        return $aResult;
    }

    public function log($nPaymentId, $aRequest)
    {
        Log::info('--------- Http Yandex Notification Success ----------');
        Log::info('Payment id: '.$nPaymentId);
        Log::info(json_encode($aRequest));
        Log::info('--------- Http Yandex Notification Success ----------');
    }
}