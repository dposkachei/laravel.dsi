<?php

namespace App\Services\Modelable;


trait Statusable
{

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->statuses[$this->status];
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusIconAttribute()
    {
        return $this->statusIcons[$this->status];
    }
}