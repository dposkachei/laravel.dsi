<?php

namespace App\Services\Image\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;
use App\Services\Image\Filters\FilterTrait;
use Intervention\Image\ImageManagerStatic as ImageStatic;

class SquareFilter implements FilterInterface
{
    use FilterTrait;

    private $options = [];
    private $image = null;

    public function __construct($options)
    {
        $this->options = $options;
    }

    public function applyFilter(Image $image)
    {
        if (isset($this->options['dimension'])) {
            $width = $image->width();
            $height = $image->height();
            $dimension = explode(':', $this->options['dimension']);
            $q = $dimension[0] / $dimension[1];
            if ($width > $height) {
                $image = $image->fit(intval($height), intval($height * $q));
            } else {
                $image = $image->fit(intval($width), intval($width * $q));
            }
        } else {
            $image = $image->fit(120, 120);
        }
        return $image;
    }

    public function resize($sFileName, $originalPath, $path, $key)
    {
        $this->image = ImageStatic::make($originalPath.$sFileName);
        $this->image = $this->applyFilter($this->image);
        $sPath = $path.'/'.$key.'/';
        $this->checkDirectory($sPath);
        $this->image->save($sPath.$sFileName);
    }
}