<?php

namespace App\Services\Image;

use App\Services\Image\ImageService;
use Illuminate\Support\Facades\File;

class ImagePath extends ImageService
{

    private $imagecache = false;
    private $filter = 'square';

    private $checkByCurl = false;


    public function cache()
    {
        $this->imagecache = false;
        return $this;
    }

    /**
     * Изображение по умолчанию
     * @return string
     */
    public function default($key = null)
    {
        if ($key === 'product' || $key === 'category' || $key === 'page') {
            $key = 'product';
        } else {
            $key = 'user';
        }
        return $this->imagecache ?
            $this->asset('imagecache/original/default/'.$key.'.png') :
            $this->asset('img/default/'.$key.'.png');
    }

    public function image($key, $size, $model, $filter = null)
    {
        if (is_null($model)) {
            return $this->default($key);
        }
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return $this->default($key);
        }
        if ($this->imagecache) {
            $outFilter = is_null($filter) ? $this->filter : $filter;
            return $this->asset('imagecache/'.$outFilter.'/'.$key.'/'.$model->imageable_id.'/'.$size.'/'.$filename);
        } else {
            return $this->asset('images/'.$key.'/'.$model->imageable_id.'/'.$size.'/'.$filename);
        }
    }

    public function main($key, $size, $model, $filter = null)
    {
        if (is_null($model)) {
            return $this->default($key);
        }
        $images = $model->images;
        if (is_null($images) || empty($images) || empty($images[0])) {
            return $this->default($key);
        }
        $oFile = $images->where('is_main', 1)->first();
        if (is_null($oFile)) {
            return false;
        }
        $filename = $oFile->filename;
        if ($this->imagecache) {
            $outFilter = is_null($filter) ? $this->filter : $filter;
            return $this->asset('imagecache/'.$outFilter.'/'.$key.'/'.$model->id.'/'.$size.'/'.$filename);
        } else {
            $path = 'images/'.$key.'/'.$model->id.'/'.$size.'/'.$filename;
            if ($this->checkFile($path)) {
                return $this->asset($path);
            } else {
                return $this->default($key);
            }
        }
    }

    public function checkMain($key, $size, $model, $filter = null)
    {
        if (is_null($model)) {
            return false;
        }
        $images = $model->images;
        if (is_null($images) || empty($images) || empty($images[0])) {
            return false;
        }
        $oFile = $images->where('is_main', 1)->first();
        if (is_null($oFile)) {
            return false;
        }
        $filename = $oFile->filename;
        $path = 'images/'.$key.'/'.$model->id.'/'.$size.'/'.$filename;
        if (!$this->checkFile($path)) {
            return false;
        }
        return true;
    }

    public function publicPath($key, $size, $model, $filter = null)
    {
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return $this->default();
        }
        return public_path('images/'.$key.'/'.$model->imageable_id.'/'.$size.'/'.$filename);
    }

    private function asset($path)
    {
        return config('app.env') === 'production' ? asset($path) : $this->remoteUrl($path);
    }

    private function checkFile($path)
    {
        if (!$this->checkByCurl) {
            return true;
        }
        return File::exists(public_path($path)) || $this->curl($path);
    }

    private function curl($path)
    {
        if (config('app.env') === 'production') {
            return true;
        }
        $path = $this->remoteUrl($path);
        $ch = curl_init($path);
        curl_exec($ch);
        if (!curl_errno($ch)) {
            $info = curl_getinfo($ch);
            $status = isset($info['http_code']) && $info['http_code'] === 200;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    private function remoteUrl($path)
    {
        return config('image.url.remote').'/'.$path;
    }

}