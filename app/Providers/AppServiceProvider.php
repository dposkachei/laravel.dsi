<?php

namespace App\Providers;

use App\Models\Article;
use App\Observers\ArticleObserver;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

use App\Services\DomainService;
use Illuminate\Support\Facades\Schema;
use App\Helpers\BladeDirectives;
//use LaravelTrailingSlash\RoutingServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * -------------------------------------
     * Schema::defaultStringLength(191);
     * https://laravel-news.com/laravel-5-4-key-too-long-error
     * -------------------------------------
     * Error migrations for laravel 5.4 for long unique key.
     * Error: "Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes".
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        BladeDirectives::switchCase();
        BladeDirectives::custom();


        Article::observe(ArticleObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->rebinding('request', function ($app, $request) {
            $request->setUserResolver(function () use ($app) {
                // return \Auth::user();
                return $app['sentinel']->getUser();
            });
        });
        DomainService::handle();



        if ($this->app->environment() == 'local') {
            $this->app->register(\Bpocallaghan\Generators\GeneratorsServiceProvider::class);
        }
    }
}
