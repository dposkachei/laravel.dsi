<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class SendOrder extends Mailable
{
    use Queueable, SerializesModels;

    private $fromAddress = null;
    private $fromName = null;

    private $oOrder = [];

    private $title = '';

    /**
     * SendOrder constructor.
     * @param $oModel
     */
    public function __construct($oModel)
    {
        $this->fromAddress = Config::get('mail.from.address');
        $this->fromName = Config::get('mail.from.name');

        $this->oOrder = $oModel;

        if ($this->oOrder->isCall()) {
            $this->title = 'Заказ обратного звонка';
        } else {
            $this->title = 'Заказ продукций';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.admin.order')->with([
            'oOrder' => $this->oOrder,
        ])->subject($this->title);
    }
}
