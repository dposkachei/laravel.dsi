<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Checkpoints\ActivationCheckpoint;
use Illuminate\Support\Facades\Route;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class AuthenticateSentinel
{
    private $exceptRoutes = [
        'activate',
        'activate.post',
        'logout.post',
    ];
    /**
     * The guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $sentinel = $this->auth;
        //return $sentinel::guest() ? redirect()->route('admin.index.welcome') : $next($request);

        try {
            return $sentinel::guest() ? redirect()->route('admin.index.welcome') : $next($request);
        } catch (NotActivatedException $e) {
            if (in_array(Route::current()->getName(), $this->exceptRoutes)) {
                return $next($request);
            } else {
                return redirect()->route('activate');
            }
        }
        $guest = $sentinel::bypassCheckpoints(function() use ($sentinel) {
            return $sentinel::guest();
        });
        if ($guest) {
            return redirect()->route('admin.index.welcome');
        } else {
            try {
                dd($sentinel::getUser());
                return $next($request);
            } catch (\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e) {
                dd('hi');
            }
            if ($sentinel::forceCheck()) {
                if (!is_null($sentinel::getUser(false))) {
                    return $next($request);
                } else {
                    if (Route::current()->getName() === 'activate' || Route::current()->getName() === 'activate.post') {
                        $sentinel::bypassCheckpoints(function() use ($sentinel) {
                            //$sentinel::logout();
                        });
                        //dd($sentinel::logout());
                        //$sentinel::disableCheckpoints();
                        //dd($sentinel::checkpointsStatus());
                        return $next($request);
                    } else {
                        return redirect()->route('activate');
                    }
                    //dd($sentinel::forceCheck());
                }
            } else {
                return $next($request);
            }
        }



    }
}