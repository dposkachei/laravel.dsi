<?php

namespace App\Http\Composers;

use App\Helpers\Model;
use App\Models\Article;
use App\Models\Contact;
use App\Models\Option;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use App\Models\User;
use App\Models\Category;
use Illuminate\Support\Facades\Redis;
use App\Registries\MemberRegistry;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Image\Facades\ImagePath;

class AllComposer
{
    private $member = null;
    private $users;
    private $pages;

    private $layout;

    private $view = null;

    private $categories;
    private $options;

    private $roles;

    private $generateTable = false;
    private $generateForm = false;
    private $site;



    private $modelColumns;

    /**
     * @var array
     */
    private $requiredOptions = [
        'title' => ''
    ];


    private $basketCount = 0;



    public function __construct(Route $route)
    {
        //$oUser = Sentinel::getRoleRepository();
        //dd($oUser);

        $this->member = MemberRegistry::getInstance();

        $this->users = Cache::remember('users', 60, function() {
            return User::all();
        });
        $this->pages = Cache::remember('pages', 60, function() {
            return $this->getFiles(storage_path('../resources/views/bulma/pages'), 'pages');
        });

        $this->content = Cache::remember('content', 60, function() {
            return $this->getAliases($this->getFiles(storage_path('../resources/views/bulma/content'), 'content'));
        });
        $this->modelColumns = Cache::remember('model_columns', 60, function() {
            return $this->getModels($this->getFiles(storage_path('../resources/views/bulma/content'), 'content'));
        });
        $this->layout = Cache::remember('layout', 60, function() {
            return Config::get('site.layout');
        });
        $this->categories = Cache::remember('category', 60, function() {
            return Category::with('children')->orderBy('priority', 'desc')->get();
        });

        $this->roles = Cache::remember('roles', 60, function() {
            return EloquentRole::all();
        });

        $this->options = Cache::remember('options', 60, function() {
            return Option::all();
        });
        $this->articles = Cache::remember('articles', 60, function() {
            return Article::all();
        });

        //dd($this->content);

        if (!is_null(Route::current())) {
            $this->view = stristr(Route::current()->getName(),'.', true);
        }


        $this->site = Cache::remember('site', 60, function() {
            $oContacts = Contact::where('status', 2)->get();
            $aCurrentOptions = [];
            foreach($this->options->where('status', 1) as $oOption) {
                $key = strripos($oOption->key, '.') ? stristr($oOption->key,'.', true) : $oOption->key;
                $value = $oOption->value;
                if ($oOption->isImage) {
                    $value = ImagePath::cache()->main('option', 'original', $oOption);
                }
                $subKey = substr(stristr($oOption->key,'.'), 1);
                if (isset($aOptions[$key][$subKey])) {
                    $aCurrentOptions[$key][$subKey][] = $value;
                } else {
                    $aCurrentOptions[$key][$subKey] = $value;
                }
            }
            $defaultOptions = Config::get('site.options');
            foreach($defaultOptions as $key => $aOptions) {
                if ($key === 'contacts') {
                    foreach($aOptions as $subKey => $aOption) {
                        $oContactOption = $oContacts->where('key', $subKey)->first();
                        if (!is_null($oContactOption)) {
                            $aCurrentOptions[$key][$subKey] = $oContactOption->value;
                        } else {
                            $aCurrentOptions[$key][$subKey] = null;
                        }
                    }
                } else {
                    foreach($aOptions as $subKey => $aOption) {
                        if (!isset($aCurrentOptions[$key][$subKey])) {
                            $aCurrentOptions[$key][$subKey] = $aOption;
                        }
                    }
                }
            }
            $oOptions = [];
            foreach($aCurrentOptions as $key => $aCurrentOption) {
                $oOptions[$key] = (object)$aCurrentOption;
            }
            return (object)$oOptions;
        });

        if (Session::exists('basket') && Session::has('basket')) {
            $this->basketCount = count(Session::get('basket'));
        }

        //$model = Model::init('product')->getColumns('form');
        //dd($model);
        /*
        dd(trans('validation.accepted', [
            'attribute' => '123'
        ]));
        */


        //dd(redirect()->back()->url());

        //dd(Route::current()->getName());
        //dd(Route::current()->getName());
        //option(['someKey' => 'someValue']);
        //dd(option('someKey'));
        //$ip = GeoIP::setIp('178.76.219.48');
        //dd($ip->get());
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('oComposerMember',      $this->member);
        $view->with('oComposerUsers',       $this->users);
        $view->with('aComposerPages',       $this->pages);
        $view->with('aComposerContent',     $this->content);
        $view->with('sComposerLayout',      $this->layout);
        $view->with('sComposerRouteView',   $this->view);
        $view->with('oComposerCategories',  $this->categories);
        $view->with('oComposerRoles',       $this->roles);
        $view->with('oComposerGenerateTable',$this->generateTable);
        $view->with('oComposerGenerateForm',$this->generateForm);
        $view->with('oComposerModelColumns',$this->modelColumns);

        $view->with('oComposerOptions',     $this->options);
        $view->with('oComposerArticles',    $this->articles);
        $view->with('oComposerSite',        $this->site);

        $view->with('nBasketCount',        $this->basketCount);
    }

    public function getFiles($dir, $slug)
    {
        $files = File::allFiles($dir);
        $paths = [];
        $envServer = env('APP_SERVER', true);
        $slash = $envServer ? '/' : '\\';
        foreach ($files as $file) {
            $paths[] = stristr(stristr(File::dirname((string)$file), $slug), $slash);
            //$paths[] = strrchr(stristr(stristr(File::dirname((string)$file), 'page'), '\\'), '\\');
        }
        $paths = array_unique($paths);
        $pages = [];
        foreach($paths as $path) {
            if ($path !== '\components\index') {
                $pages[] = explode($slash, $path)[1];
            }
        }
        $pages = array_unique($pages);
        sort($pages);
        return $pages;
    }

    public function getAliases($content)
    {
        $array = [];
        foreach($content as $key => $value) {
            $tValue = title_case($value);
            $sClass = $tValue.studly_case('_controller');
            $sClass = 'App\Cmf\Project\\'.$tValue.'\\'.$sClass;
            if (class_exists($sClass)) {
                $oController = new $sClass();
                if (isset($oController->menu)) {
                    $array[$value] = $oController->menu;
                }
            }
        }
        return $array;
    }


    public function getModels($content)
    {
        $array = [];
        foreach($content as $key => $value) {
            $tValue = title_case($value);
            $sClass = 'App\Models\\'.$tValue;
            if (class_exists($sClass)) {
                $oController = new $sClass();
                if (method_exists($oController, 'getColumns')) {
                    $array[$value]['table'] = $oController->getColumns('table');
                    $array[$value]['form'] = $oController->getColumns('form');
                    $array[$value]['fillable'] = $oController->getColumns('fillable');
                }
            }
        }
        return $array;
    }

}