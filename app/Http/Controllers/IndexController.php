<?php

namespace App\Http\Controllers;

use App\Jobs\SendLog;
use App\Models\Category;
use App\Models\Certificate;
use App\Models\Contact;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Requisite;
use App\Models\User;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use KekoApp\LaravelMetaTags\Facades\MetaTag;
use narutimateum\Toastr\Facades\Toastr;
use ConsoleTVs\Charts\Facades\Charts;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Events\TestPusher;
use App\Services\DomainService;
use App\Models\Page;
use Spatie\Sitemap\SitemapGenerator;

class IndexController extends Controller
{
    /**
     * Pagination url
     * @var string
     */
    private $indexPaginationUrl;

    /**
     * Table limit item
     * @var int
     */
    private $indexPaginationLimit = 6;

    public function __construct()
    {
        /**
         * Route for pagination
         */
        $this->indexPaginationUrl = route('index.products.pagination');

        //View::share('nBasketCount', $this->getBasketCount());
    }

    public function pagination(Request $request)
    {
        $oProducts = Product::where('status', 1)->paginate($this->indexPaginationLimit)->withPath($this->indexPaginationUrl);
        return [
            'success' => true,
            'view' => [
                'list' => view('dsi.components.product.pagination.list', [
                    'oProducts' => $oProducts
                ])->render(),
                'button' => view('dsi.components.product.pagination.button', [
                    'oProducts' => $oProducts
                ])->render(),
            ],
            'count' => $oProducts->total()
        ];
    }

    public function getBasketCount()
    {
        $count = 0;
        if (Session::exists('basket') && Session::has('basket')) {
            $count = count(Session::get('basket'));
        }
        return $count;
    }


    public function index()
    {
        $oPopularProducts = Product::where('status', 1)->orderBy('pay_count', 'desc')->take(6)->get();
        $oProducts = Product::where('status', 1)->orderBy('priority', 'desc')->paginate($this->indexPaginationLimit)->withPath($this->indexPaginationUrl);

        $oPage = Page::where('name', 'main')->where('status', 1)->first();
        return view('dsi.index', [
            'oPopularProducts' => $oPopularProducts,
            'oProducts' => $oProducts,
            'oPage' => $oPage,
        ]);
    }

    public function contacts()
    {
        $oRequisites = Requisite::where('status', 1)->orderBy('priority', 'desc')->get();
        $oContacts = Contact::whereIn('status', [1, 2])->orderBy('priority', 'desc')->get();

        $oPage = Page::where('name', 'contacts')->where('status', 1)->first();
        return view('dsi.content.contacts', [
            'oRequisites' => $oRequisites,
            'oContacts' => $oContacts,
            'oPage' => $oPage,
        ]);
    }

    public function certificates()
    {
        $oCertificates = Certificate::where('status', 1)->where('type', 0)->orderBy('priority', 'desc')->get();
        $oCertificates = $oCertificates->reject(function($item) {
            return is_null($item->files->first()) || is_null($item->images->first());
        });

        $oPage = Page::where('name', 'certificates')->where('status', 1)->first();
        return view('dsi.content.certificates', [
            'oCertificates' => $oCertificates,
            'oPage' => $oPage,
        ]);
    }

    public function partners()
    {
        $oPartners = Partner::where('status', 1)->get();

        $oPage = Page::where('name', 'partners')->where('status', 1)->first();
        return view('dsi.content.partners', [
            'oPartners' => $oPartners,
            'oPage' => $oPage,
        ]);
    }

    public function categories()
    {
        $oCategories = Category::where('status', 1)->get();

        $oPage = Page::where('name', 'categories')->where('status', 1)->first();
        return view('dsi.content.category', [
            'oCategories' => $oCategories,
            'oPage' => $oPage,
        ]);
    }

    public function products()
    {
        $oProducts = Product::where('status', 1)->orderBy('priority', 'desc')->paginate($this->indexPaginationLimit)->withPath($this->indexPaginationUrl);

        $oPage = Page::where('name', 'products')->where('status', 1)->first();
        return view('dsi.content.products', [
            'oProducts' => $oProducts,
            'oPage' => $oPage,
        ]);
    }

    public function category($id)
    {
        $oCategory = Category::find($id);
        if ($oCategory->hasChildren()) {
            $aCategoriesId = Category::where('parent_id', $oCategory->id)->pluck('id')->toArray();
            $aCategoriesId[] = $oCategory->id;
            $oProducts = Product::whereIn('category_id', $aCategoriesId)->where('status', 1)->orderBy('priority', 'desc')->get();
        } else {
            $oProducts = $oCategory->activeProducts->sortByDesc('priority');
        }
        MetaTag::set('title', $oCategory->title);
        MetaTag::set('description', !is_null($oCategory->text) ? $oCategory->text : $oCategory->title);
        if(ImagePath::checkMain('category', 'original', $oCategory)) {
            MetaTag::set('image', ImagePath::main('category', 'original', $oCategory));
        }
        return view('dsi.content.category', [
            'oCategory' => $oCategory,
            'oProducts' => $oProducts,
            'metaController' => true
        ]);
    }

    public function product($id)
    {
        $oProduct = Product::with('seo')
            ->where('id', $id)
            ->where('status', 1)
            ->first();

        if (is_null($oProduct)) {
            return redirect()->route('index.products');
        }
        MetaTag::set('title', $oProduct->title);
        MetaTag::set('description', $oProduct->title);

        $oSeo = $oProduct->seo;
        if (!is_null($oSeo)) {
            MetaTag::set('description', $oSeo->description);
            MetaTag::set('keywords', $oSeo->keywords);
        }
        if(ImagePath::checkMain('product', 'original', $oProduct)) {
            MetaTag::set('image', ImagePath::main('product', 'original', $oProduct));
        }

        $oCategory = Category::find($oProduct->category_id);

        return view('dsi.content.product', [
            'oProduct' => $oProduct,
            'metaController' => true,
            'oCategory' => $oCategory
        ]);
    }

    public function basket()
    {
        return view('dsi.components.basket.modal');
    }

    public function search(Request $request)
    {
        $oProducts = Product::where('title', 'like', '%'.$request->get('q').'%')->where('status', 1)->get();

        return view('dsi.content.search', [
            'oProducts' => $oProducts
        ]);
    }

    public function sitemap()
    {
        $path = public_path('sitemap.xml');
        SitemapGenerator::create(url('/'))->writeToFile($path);
    }




    public function welcome()
    {
        return view('bulma.welcome');
    }

    public function welcomeAdmin()
    {


        Toastr::success(
            'Большое сообщение, показывающее насколько большой может быть <a href="#">блок</a>.',
            $title = 'Заголовок',
            $options = [
                'timeOut' => 100000
            ]);

        /*
        $faker = Faker::create();

        for($i=0; $i<10; $i++){
            $user = ['name' => $faker->name, 'email' => $faker->email, 'password' => \Hash::make('password')];
            User::create($user);
        }
        */


        $this->dispatch(new SendLog('** Сообщение с контроллера **'));
        Queue::push(new SendLog('** Сообщение с очереди **'));

        $date = Carbon::now()->addMinutes(5);

        Queue::later($date, new SendLog('** Сообщение с зачержкой в 5 минут **'));


        if (DomainService::isAdmin()) {
            return view('bulma.welcome', [
                //'country' => $country
            ]);
        } else {
            return view('welcome', [
                //'country' => $country
            ]);
        }
    }

    public function chart()
    {
        $chart = Charts::create('line', 'highcharts')
            ->title('My nice chart')
            ->labels(['First', 'Second', 'Third'])
            ->values([5,10,20])
            ->dimensions(0,500)
            ->loader(true)
            ->loaderColor('#FF3860')
            ->template('bulma');

        return view('bulma.chart', [
            'chart' => $chart
        ]);
    }

    public function chartJson()
    {
        return json_encode(['value' => 40]);
    }

    public function loauth(Request $request)
    {
        //abort(404);
        //dd($request->all());
        /*
        return response()->json([
            'error' => 'Переключитесь на английскую раскладку.'
        ], 401);
        */
        event(new TestPusher('Тестовое сообщение'));

        return [
            'push' => [
                'title' => 'Js testim',
                'icon' => asset('img/cover.png')
            ]
        ];

        /*
        return [
            'success' => true,
            'redirect' => '/',
            'toastr' => [
                'title' => 'Заголовок',
                'text' => 'In your terminal to install a project\'s dependencies.',
                'type' => 'success'
            ]
        ];
        */
        return view('bulma.pages.dialogs.components.ajax.content');



    }

    /*
    public function pagination()
    {
        return $this->paginationView();
    }
    */

    public function paginationView()
    {
        $oUsers = User::whereNotNull('name')->paginate(2)->withPath(route('pagination.search.post'));
        return view('bulma.pagination', [
            'oUsers' => $oUsers
        ]);
    }

    /*
    public function search()
    {
        $oUsers = User::whereNotNull('name')->paginate(2)->withPath(route('pagination.search.post'));

        return [
            'view' => view('bulma.components.pagination.table', [
                'oUsers' => $oUsers
            ])->render()
        ];
    }
    */

    public function view($view)
    {
        //$config = config('toastr.options');
        //dd(json_encode($config));

        //$oUsers = User::whereNotNull('name')->paginate(2)->withPath(route('pagination.search.post'));
        //View::share('oUsers', $oUsers);

        if (View::exists('bulma.pages.'.$view.'.content')) {
            $template = new TemplatesController('bulma.pages.'.$view.'.content');
            if(method_exists($template, $view)) {
                return $template->$view();
            } else {
                return view('bulma.pages.'.$view.'.content');
            }

        } else {
            return view('bulma.welcome');
        }
    }

    public function ajaxView($view)
    {
        if (View::exists('bulma.pages.'.$view.'.block')) {
            return view('bulma.pages.'.$view.'.block')->render();
        } else {
            return view('bulma.pages.dialogs.block')->render();
        }
    }

    public function tab($tab)
    {
        if (View::exists('bulma.pages.product.components.tabs.'.$tab)) {
            return [
                'success' => true,
                'view' => view('bulma.pages.product.components.tabs.'.$tab)->render()
            ];
        } else {
            return [
                'success' => true,
                'view' => view('bulma.pages.product.components.tabs.info')->render()
            ];
        }
    }

    public function iframe($name)
    {
        return view('bulma.components.iframe.'.$name)->render();
    }

    public function confirm(Request $request)
    {
        return [
            'success' => true
        ];
    }
}
