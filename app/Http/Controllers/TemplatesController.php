<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TemplatesController extends Controller
{
    private $view;


    public function __construct($view)
    {
        $this->view = $view;
    }

    public function gallery()
    {
        $aImages = [
            asset('imagecache/original/templates/set1/1.jpg'),
            asset('imagecache/original/templates/set1/2.jpg'),
            asset('imagecache/original/templates/set1/3.jpg'),
            asset('imagecache/original/templates/set1/4.jpg'),
            asset('imagecache/original/templates/set1/5.jpg'),
            asset('imagecache/original/templates/set1/6.jpg'),
            asset('imagecache/original/templates/set1/7.jpg'),
            asset('imagecache/original/templates/set1/8.jpg'),
            asset('imagecache/original/templates/set1/9.jpg'),
            asset('imagecache/original/templates/set1/10.jpg'),
            asset('imagecache/original/templates/set1/11.jpg'),
            asset('imagecache/original/templates/set1/2.jpg'),
            asset('imagecache/original/templates/set1/4.jpg'),
            asset('imagecache/original/templates/set1/8.jpg'),
            asset('imagecache/original/templates/set1/10.jpg'),
            asset('imagecache/original/templates/set1/1.jpg'),
            asset('imagecache/original/cover.png'),
            asset('imagecache/original/cover2.png'),
            asset('imagecache/original/cover1.png'),
            asset('imagecache/original/cover.png'),
        ];

        return view($this->view, [
            'aImages' => $aImages
        ]);
    }
}
