<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\Sentinel\AuthenticatesUsers;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('bulma.auth.login');
    }

    public function showActivateForm()
    {
        return view('bulma.auth.activate');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $oUser = Sentinel::bypassCheckpoints(function() { return Sentinel::getUser(); });
        Sentinel::logout($oUser, true);

        return [
            'success' => true,
            'redirect' => url('/')
        ];
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @return array
     */
    protected function authenticated(Request $request)
    {
        return [
            'success' => true,
            'redirect' => url('/home')
        ];
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        return isset($this->authenticate($request)->email) ? $this->authenticated($request) : $this->authenticate($request);
    }

    public function activate(Request $request)
    {
        $oUser = Sentinel::bypassCheckpoints(function() { return Sentinel::getUser(); });
        if (Activation::complete($oUser, $request->get('code'))) {
            return [
                'success' => true,
                'redirect' => url('/home')
            ];
        } else {
            return response()->json([
                $this->keyMessage => ['Неверный код.']
            ], 401);
        }
    }
}
