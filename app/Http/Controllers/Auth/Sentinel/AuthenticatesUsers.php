<?php

namespace App\Http\Controllers\Auth\Sentinel;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

trait AuthenticatesUsers
{
    private $keyMessage = 'error';



    protected function validateUser($oUser)
    {
        if ($oUser->status) {
            return $oUser;
        } else {
            Sentinel::logout($oUser, true);
            return response()->json([
                $this->keyMessage => ['Ваш аккаунт заблокирован в связи с нарушением Условий использования.']
            ], 401);
        }
    }



    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|email|max:255',
            'password' => 'required|string',
        ]);
    }

    protected function findUser(Request $request)
    {
        $credentials = [
            $this->username() => $request->email
        ];
        $oUser = Sentinel::findByCredentials($credentials);
        return $oUser;
    }

    protected function authenticateUsers(Request $request)
    {
        if (!is_null($this->findUser($request))) {

            $remember = isset($request->remember) && $request->remember ? $request->remember : 0;
            $credentials = [
                $this->username() => $request->email,
                'password'  => $request->password
            ];
            $this->beforeLogin();

            try {
                if ($oUser = Sentinel::authenticateAndRemember($credentials, $remember)) {
                    return $this->validateUser($oUser);
                } else {
                    return response()->json([
                        $this->keyMessage => ['Не верный логин или пароль.']
                    ], 401);
                }
            } catch (NotActivatedException $e) {
                $oUser = Sentinel::bypassCheckpoints(function() use ($credentials, $remember) {
                    return Sentinel::authenticateAndRemember($credentials, $remember);
                });
                return $this->validateUser($oUser);
            }
        } else {
            return response()->json([
                $this->keyMessage => ['Этот номер телефона не зарегистрирован.']
            ], 401);
        }
    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }


    protected function authenticate(Request $request)
    {
        return $this->authenticateUsers($request);
    }

    protected function beforeLogin()
    {

    }

    protected function afterLogin()
    {

    }



}