<?php

namespace App\Http\Controllers\Auth\Sentinel;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use App\Events\ChangeCacheEvent;

trait RegistersUsers
{

    private $keyMessage = 'error';

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validateRegister($request);

        $oUser = $this->create($request);
        $this->afterRegister();

        $this->afterLogin();

        Sentinel::loginAndRemember($oUser);
        $this->beforeLogin();

        return $this->registered($request, $oUser);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     */
    protected function create(Request $request)
    {
        $data = $this->beforeCreate($request);

        $oUser = Sentinel::register($data);

        $role = Sentinel::findRoleBySlug('user');
        $role->users()->attach($oUser);

        $oActivation = Activation::create($oUser);
        $oActivation->code = str_random(48);
        $oActivation->save();
        //Activation::complete($oUser, $oActivation->code);
        /*
        $credential = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        $oUser = Sentinel::authenticate($credential, false);
        */
        return $oUser;
    }


    protected function beforeCreate(Request $request)
    {
        $input['password'] = $request->password;
        $input['login'] = $request->email;
        $input['first_name'] = $request->first_name;
        return $input;
    }





    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateRegister($request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    protected function beforeLogin()
    {

    }

    protected function afterLogin()
    {

    }

    public function afterRegister()
    {
        event(new ChangeCacheEvent('user'));
    }


    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        return [
            'success' => true,
            'redirect' => url()->previous()
        ];
    }
}