<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BasketController extends Controller
{
    public function index(Request $request)
    {
        if (Session::exists('basket') && Session::has('basket')) {
            $aBasket = Session::get('basket');
            $oItems = Product::whereIn('id', $aBasket)->get();
        } else {
            $oItems = null;
        }
        return [
            'success' => true,
            'count' => !is_null($oItems) ? count($oItems) : 0,
            'view' => view('dsi.components.basket.form', [
                'oItems' => $oItems
            ])->render()
        ];
    }

    public function store(Request $request, $id)
    {
        if (Session::exists('basket') && Session::has('basket')) {
            $aBasket = Session::get('basket');
            if (!in_array($id, $aBasket)) {
                $aBasket[] = $id;
                Session::put('basket', $aBasket);
            }
            $oItems = Product::whereIn('id', $aBasket)->get();
        } else {
            $aBasket[] = $id;
            Session::put('basket', $aBasket);
            $oItems = Product::whereIn('id', $aBasket)->get();
        }
        return [
            'success' => true,
            'count' => !is_null($oItems) ? count($oItems) : 0,
            'view' => view('dsi.components.basket.form', [
                'oItems' => $oItems
            ])->render()
        ];
    }

    public function destroy(Request $request, $id)
    {
        if (Session::exists('basket') && Session::has('basket')) {
            $aBasket = Session::get('basket');
            $key = array_search($id, $aBasket);
            unset($aBasket[$key]);
            sort($aBasket);
            Session::put('basket', $aBasket);
        }
        return [
            'success' => true
        ];
    }

    public function clear(Request $request)
    {
        Session::forget('basket');
        return [
            'success' => true
        ];
    }

    public function order(Request $request)
    {
        return [
            'success' => true
        ];
    }

}
