<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Services\Comment\CanComment;
use Cartalyst\Sentinel\Activations\EloquentActivation as SentinelEloquentActivation;
use Cartalyst\Sentinel\Reminders\EloquentReminder as SentinelEloquentReminder;
use Cartalyst\Sentinel\Roles\EloquentRole as SentinelEloquentRole;
use Cartalyst\Sentinel\Users\EloquentUser;
use App\Services\Status\Statusable;

class User extends EloquentUser
{
    use Notifiable;
    use CanComment;
    use Statusable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'permissions',
        'first_name',
        'last_name',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Login by phone
     * @var array
     */
    protected $loginNames = ['email'];


    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Пользователь заблокирован',
        1 => 'Пользователь активен',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Заблокирован'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активен'
        ],
    ];


    /**
     * User activation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activation()
    {
        return $this->hasOne(SentinelEloquentActivation::class, 'user_id', 'id');
    }

    /**
     * User reminder
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reminder()
    {
        return $this->hasOne(SentinelEloquentReminder::class, 'user_id', 'id');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    protected $columnsTable = [
        'id' => '#',
        'image' => 'IMG',
        'first_name' => 'Имя',
        'email' => 'Email',
        'relation' => [
            'title' => 'Роль',
            'key' => 'role->name'
        ],
        'status' => 'Статус'
    ];
    protected $columnsForm = [
        'first_name' => [
            'type' => 'string',
            'title' => 'Имя',
            'icon' => 'fa-user',
        ],
        'email' => [
            'type' => 'email',
            'title' => 'Email',
            'icon' => 'fa-envelope',
        ],
        'role_id' => [
            'type' => 'select',
            'title' => 'Роль',
            'hidden' => true,
            'select' => [
                'key' => 'id',
                'value' => 'name',
                'item_key' => 'role->id',
                'cache' => 'oComposerRoles'
            ],
            'icon' => 'fa-lock',
        ],
        'password' => [
            'type' => 'password',
            'title' => 'Пароль',
            'icon' => 'fa-lock',
        ],
        'password_confirmation' => [
            'type' => 'password',
            'title' => 'Confirm password',
            'icon' => 'fa-lock',
        ]
    ];

    public function getColumns($type)
    {
        switch($type) {
            case 'form':
                return $this->columnsForm;
            case 'table':
                return $this->columnsTable;
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }
}
