<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Status\Statusable;

class Order extends Model
{
    use Statusable;

    protected $table = 'orders';

    protected $fillable = [
        'type', 'name', 'email', 'phone', 'comment', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активный',
        1 => 'Активный',
        2 => 'Важный',
        3 => 'Отменен',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не активный'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активный'
        ],
        2 => [
            'class' => 'badge badge-success',
            'title' => 'Важный'
        ],
        3 => [
            'class' => 'badge badge-success',
            'title' => 'Отменен'
        ],
    ];

    /**
     * The type attributes for model
     *
     * @var array
     */
    protected $types = [
        0 => 'Неизвестно',
        1 => 'Заказ продукции',
        2 => 'Обратный звонок',
    ];
    protected $typeIcons = [
        0 => [
            'class' => 'lock',
            'title' => 'Неизвестно'
        ],
        1 => [
            'class' => 'truck',
            'title' => 'Заказ продукции'
        ],
        2 => [
            'class' => 'phone',
            'title' => 'Обратный звонок'
        ],
    ];

    public function products()
    {
        return $this->BelongsToMany(Product::class, 'order_products')
            ->withPivot('product_id', 'order_id', 'id');
    }

    public function getTypeTextAttribute()
    {
        return $this->types[$this->type];
    }
    public function getTypeIconAttribute()
    {
        return $this->typeIcons[$this->type];
    }

    public function isCall()
    {
        return intval($this->type) === 2;
    }

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    public function getColumns($type)
    {
        switch($type) {
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }
}
