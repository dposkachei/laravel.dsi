<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Status\Statusable;

class Category extends Model
{
    use Statusable;

    protected $table = 'categories';

    protected $fillable = [
        'title', 'text', 'status', 'parent_id', 'priority', 'url',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не опубликовано',
        1 => 'Опубликовано',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не опубликовано'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Опубликовано'
        ],
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }
    public function activeProducts()
    {
        return $this->hasMany(Product::class, 'category_id', 'id')->where('status', 1);
    }

    protected $columnsTable = [
        'id' => '#',
        'image' => 'IMG',
        'title' => 'Заголовок',
    ];
    protected $columnsForm = [
        'title' => [
            'type' => 'string',
            'title' => 'Заголовок'
        ]
    ];

    public function getColumns($type)
    {
        switch($type) {
            case 'form':
                return $this->columnsForm;
            case 'table':
                return $this->columnsTable;
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->orderBy('priority', 'desc');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    public function getParents($type = 'product')
    {
        $oCategory = $this;
        $oParents = collect();
        if ($type === 'product') {
            $oParents->push($oCategory);
        }
        while (!is_null($this->getParent($oCategory))) {
            $oCategory = $this->getParent($oCategory);
            if (!is_null($oCategory)) {
                $oParents->push($oCategory);
            }
        }
        $oParents = $oParents->sortBy(function ($product, $key) {
            return $product;
        });

        return $oParents;
    }

    public function hasChildren()
    {
        $oChildren = $this->children()->count();
        if ($oChildren === 0) {
            return false;
        } else {
            return true;
        }
    }

    private function getParent($oCategory)
    {
        if (!is_null($oCategory->parent_id)) {
            return $oCategory->parent;
        } else {
            return null;
        }
    }
}
