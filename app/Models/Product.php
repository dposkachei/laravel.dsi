<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Comment\Commentable;
use App\Services\Status\Statusable;

class Product extends Model
{
    use Commentable;
    use Statusable;

    protected $canBeRated = true;
    protected $mustBeApproved = true;

    protected $fillable = [
        'title', 'status', 'text', 'url', 'category_id', 'priority', 'pay_count',
    ];

    protected $with = [
        'images'
    ];

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function seo()
    {
        return $this->hasOne(ProductParameters::class, 'product_id', 'id')->where('type', 1);
    }

    public function certificates()
    {
        return $this->BelongsToMany(Certificate::class, 'product_certificates')
            ->withPivot('product_id', 'priority', 'id')
            ->orderBy('product_certificates.priority', 'desc')
            ->withTimestamps();
    }

    public function activeCertificates()
    {
        return $this->BelongsToMany(Certificate::class, 'product_certificates')
            ->withPivot('product_id', 'priority', 'id')
            ->where('certificates.status', 1)
            ->orderBy('product_certificates.priority', 'desc')
            ->withTimestamps();
    }


    protected $columnsTable = [
        'id' => '#',
        'image' => 'IMG',
        'title' => 'Заголовок',
        'status' => 'Статус'
    ];
    protected $columnsForm = [
        'title' => [
            'type' => 'string',
            'title' => 'Заголовок'
        ],
        'category_id' => [
            'type' => 'select',
            'title' => 'Категория',
            'select' => [
                'key' => 'id',
                'value' => 'title',
                'cache' => 'oComposerCategories'
            ]
        ],
        'description' => [
            'type' => 'text',
            'title' => 'Описание'
        ]
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активно',
        1 => 'Активно',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не активно'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ],
    ];

    public function getColumns($type)
    {
        switch($type) {
            case 'form':
                return $this->columnsForm;
            case 'table':
                return $this->columnsTable;
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }


}
