<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Status\Statusable;

class ProductParameters extends Model
{
    use Statusable;

    protected $table = 'product_parameters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'type', 'description', 'keywords', 'status'
    ];
    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Пост не опубликован на этой странице',
        1 => 'Пост опубликован на этой странице',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не активно'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ]
    ];
}
