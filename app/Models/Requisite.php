<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Status\Statusable;

class Requisite extends Model
{
    use Statusable;

    protected $table = 'requisites';

    protected $fillable = [
        'title', 'value', 'priority', 'status'
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Скрыто',
        1 => 'Активно',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Скрыто'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ],
    ];

    protected $columnsTable = [
        'id' => '#',
        'image' => 'IMG',
        'title' => 'Заголовок',
        'value' => 'Значение',
        'priority' => 'Приоритет',
        'status' => 'Статус',
    ];
    protected $columnsForm = [
        'value' => [
            'type' => 'string',
            'title' => 'Значение'
        ],
        'title' => [
            'type' => 'string',
            'title' => 'Заголовок'
        ],
        'priority' => [
            'type' => 'integer',
            'title' => 'Приоритет'
        ]
    ];

    public function getColumns($type)
    {
        switch($type) {
            case 'form':
                return $this->columnsForm;
            case 'table':
                return $this->columnsTable;
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }
}
