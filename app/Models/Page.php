<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Status\Statusable;

class Page extends Model
{
    use Statusable;

    protected $table = 'pages';

    protected $fillable = [
        'title', 'page_title', 'name', 'text', 'status', 
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активный',
        1 => 'Активный',
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function getColumns($type)
    {
        switch($type) {
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }
}
