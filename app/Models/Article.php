<?php

namespace App\Models;

use App\Observers\ArticleObserver;
use Illuminate\Database\Eloquent\Model;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\DateFormatable;

class Article extends Model
{
    use Statusable;
    use DateFormatable;

    protected $table = 'articles';

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    protected $fillable = [
        'title', 'text', 'icon', 'status',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'saved ' => ArticleObserver::class,
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Скрыто',
        1 => 'Активно',
        2 => 'Выводить в преимуществах',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Скрыто'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ],
        2 => [
            'class' => 'badge badge-success',
            'title' => 'Выводить в преимуществах'
        ],
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    protected $columnsTable = [
        'id' => '#',
        'image' => 'IMG',
        'title' => 'Заголовок',
        'text' => 'Текст',
    ];
    protected $columnsForm = [
        'title' => [
            'type' => 'string',
            'title' => 'Заголовок'
        ],
        'text' => [
            'type' => 'string',
            'title' => 'Текст'
        ],
    ];

    public function getColumns($type)
    {
        switch($type) {
            case 'form':
                return $this->columnsForm;
            case 'table':
                return $this->columnsTable;
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }

    public function getBeginningAtEventAttribute()
    {
        return $this->dmYHi($this->created_at);
    }

}
