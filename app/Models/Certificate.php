<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Status\Statusable;

class Certificate extends Model
{
    use Statusable;

    protected $table = 'certificates';

    protected $fillable = [
        'title', 'text', 'type', 'priority', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не опубликовано',
        1 => 'Опубликовано',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не опубликовано'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Опубликовано'
        ],
    ];

    /**
     * The type attributes for model
     *
     * @var array
     */
    protected $types = [
        0 => 'Обычный сертификат',
        1 => 'Сертификат продукции',
    ];

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    public function scopeForProducts($query)
    {
        return $query->where('type', 1);
    }


    protected $columnsTable = [
        'id' => '#',
        'image' => 'IMG',
        'title' => 'Заголовок',
        'value' => 'Значение',
        'priority' => 'Приоритет',
        'status' => 'Статус',
    ];
    protected $columnsForm = [
        'value' => [
            'type' => 'string',
            'title' => 'Значение'
        ],
        'title' => [
            'type' => 'string',
            'title' => 'Заголовок'
        ],
        'priority' => [
            'type' => 'integer',
            'title' => 'Приоритет'
        ]
    ];

    public function getColumns($type)
    {
        switch($type) {
            case 'form':
                return $this->columnsForm;
            case 'table':
                return $this->columnsTable;
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
