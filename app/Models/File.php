<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table   = 'files';
    protected $guarded = ['id'];

    public function imageable()
    {
        return $this->morphTo('branches');
    }
}
