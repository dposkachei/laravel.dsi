<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Status\Statusable;

class Contact extends Model
{
    use Statusable;

    protected $table = 'contacts';

    protected $fillable = [
        'key', 'value', 'title', 'type', 'priority', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Скрыто',
        1 => 'Активно',
        2 => 'Выводить в меню',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Скрыто'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ],
        2 => [
            'class' => 'badge badge-success',
            'title' => 'Выводить в меню'
        ],
    ];

    /**
     * The type attributes for model
     *
     * @var array
     */
    protected $types = [
        0 => 'Неизвестно',
        1 => 'Обычное поле',
        2 => 'Текстовое поле',
        3 => 'Числовое поле',
        4 => 'Изображение',
        5 => 'Телефон',
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function getIsImageAttribute()
    {
        return $this->type === 4 ? true : false;
    }
    public function getIsPhoneAttribute()
    {
        return $this->type === 4 ? true : false;
    }

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    public function getColumns($type)
    {
        switch($type) {
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }
}
