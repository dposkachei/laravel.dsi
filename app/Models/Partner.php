<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Status\Statusable;

class Partner extends Model
{
    use Statusable;

    protected $table = 'partners';

    protected $fillable = [
        'title', 'text', 'url', 'icon', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Скрыто',
        1 => 'Активно',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Скрыто'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ],
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    protected $columnsTable = [
        'id' => '#',
        'image' => 'IMG',
        'title' => 'Заголовок',
        'text' => 'Текст',
    ];
    protected $columnsForm = [
        'title' => [
            'type' => 'string',
            'title' => 'Заголовок'
        ],
        'text' => [
            'type' => 'string',
            'title' => 'Текст'
        ],
    ];

    public function getColumns($type)
    {
        switch($type) {
            case 'form':
                return $this->columnsForm;
            case 'table':
                return $this->columnsTable;
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }
}
