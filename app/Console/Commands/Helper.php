<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;
use App\Models\Product;
use Faker\Factory as Faker;

class Helper extends Command
{
    protected $layout = 'app';
    /**
     * php artisan helper:project view add --name={name}
     * php artisan helper:project faker add --name={User}
     * php artisan helper:project cmf add --name={User}
     */

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helper:project {type} {action} {--name=} {--with=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->layout = Config::get('site.layout');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        switch($this->argument('type')) {
            case 'view':
                $this->info('view');
                $this->view();
                break;
            case 'controller':
                $this->info('controller');
                $this->controller();
                break;
            case 'model':
                $this->info('model');
                $this->model();
                break;
            case 'faker':
                $this->info('faker');
                $this->faker();
                break;
            case 'cmf':
                $this->info('cmf');
                $this->cmf();
                break;
            default:
                $this->info('default');
                break;
        }
    }

    public function view()
    {
        switch($this->argument('action')) {
            case 'add':
                $this->info('add...');
                $this->viewAdd();
                break;
            case 'delete':
                $this->info('delete...');
                $this->viewDelete();
                break;
            default:
                $this->info('default');
                break;
        }
    }

    public function controller()
    {
        switch($this->argument('action')) {
            case 'add':
                $this->info('add...');
                $this->controllerAdd();
                break;
            default:
                $this->info('default');
                break;
        }
    }

    public function model()
    {
        switch($this->argument('action')) {
            case 'add':
                $this->info('add...');
                $this->modelAdd();
                break;
            default:
                $this->info('default');
                break;
        }
    }

    public function faker()
    {
        switch($this->argument('action')) {
            case 'add':
                $this->info('add...');
                $this->fakerAdd();
                break;
            case 'delete':
                $this->info('delete...');
                $this->fakerDelete();
                break;
            default:
                $this->info('default');
                break;
        }
    }

    public function fakerAdd()
    {
        $queueName = '\App\\Models\\'.$this->option('name');

        $faker = Faker::create();

        if (class_exists($queueName)) {
            $object = new $queueName();
            $columns = $object->getTableColumns();
            unset($columns['id']);
            for($i=0; $i < 10; $i++){
                /*
                $saveData = [];
                $this->info($faker->title);
                foreach($columns as $column) {
                    if (isset($faker->$column) && !empty($faker->$column)) {
                        $saveData[$column] = $faker->$column;
                    }
                }
                */
                $saveData = [
                    'title' => $faker->company
                ];
                $queueName::create($saveData);
            }
        } else {
            $this->info($queueName.' class not found');
        }
    }



    public function viewAdd()
    {
        $queueName = $this->option('name');
        $this->info('make:view...');

        Artisan::call('make:view', [
            'name'      => 'bulma.pages.'.$queueName.'.content',
            '--extends' => 'bulma.layouts.'.$this->layout,
            '--section' => 'content'
        ]);
        Artisan::call('make:view', [
            'name' => 'bulma.pages.'.$queueName.'.readme'
        ]);
        $this->info('cache:clear...');
        Artisan::call('cache:clear');
    }

    public function viewDelete()
    {
        $queueName = $this->option('name');
        $this->info('delete:view...');
        File::deleteDirectory(storage_path('../resources/views/bulma/content/'.$queueName));
        $this->info('cache:clear...');
        Artisan::call('cache:clear');
    }

    public function controllerAdd()
    {
        $queueName = $this->option('name');
        $this->info('make:controller...');
        Artisan::call('make:controller', [
            'name' => '..\\..\\Cmf\\Project\\'.studly_case($queueName).'\\'.studly_case($queueName.'_controller'),
            '--resource' => 'true'
        ]);
        $this->info('make:controller...');
        Artisan::call('make:controller', [
            'name' => '..\\..\\Cmf\\Project\\'.studly_case($queueName).'\\'.studly_case($queueName.'_seo_provider'),
            '--resource' => 'true'
        ]);
        $this->info('make:controller...');
        Artisan::call('make:controller', [
            'name' => '..\\..\\Cmf\\Project\\'.studly_case($queueName).'\\'.studly_case($queueName.'_settings_trait'),
        ]);

        $aRoutes = [
            [
                'name' => 'index',
                'with_layout' => true
            ],
            [
                'name' => 'create',
                'with_layout' => true
            ],
            [
                'name' => 'show',
                'with_layout' => true
            ],
            [
                'name' => 'edit',
                'with_layout' => true
            ],
            [
                'name' => 'components.modals.all',
                'with_layout' => false
            ],
            [
                'name' => 'components.modals.create',
                'with_layout' => false
            ],
            [
                'name' => 'components.modals.edit',
                'with_layout' => false
            ],
            [
                'name' => 'components.table',
                'with_layout' => false
            ]
        ];

        foreach($aRoutes as $aRoute) {
            if ($aRoute['with_layout']) {
                Artisan::call('make:view', [
                    'name'      => 'bulma.content.'.$queueName.'.'.$aRoute['name'],
                    '--extends' => 'bulma.layouts.'.$this->layout,
                    '--section' => 'content,modals'
                ]);
            } else {
                Artisan::call('make:view', [
                    'name'      => 'bulma.content.'.$queueName.'.'.$aRoute['name']
                ]);
            }
        }

        $this->withAdd();

        $this->info('cache:clear...');
        Artisan::call('cache:clear');
    }

    public function modelAdd()
    {
        $queueName = $this->option('name');
        $this->info('make:model...');
        Artisan::call('make:model', [
            'name' => 'Models\\'.studly_case($queueName),
            '--migration' => 'true'
        ]);

        //$this->withAdd();

        $this->info('cache:clear...');
        Artisan::call('cache:clear');
    }


    public function withAdd()
    {
        $queueWith = $this->option('with');
        if (!empty($queueWith)) {
            $method = $queueWith.'Add';
            $this->$method();
        }
    }

    public function cmf()
    {
        $queueName = $this->option('name');
        $queueWith = $this->option('with');

        $path = explode('/', strtolower($queueName));
        if (count($path) > 1) {
            $view = 'bulma.content.'.$path[0].'.';
            $class = $queueName.'/'.$queueName;
            $model = $queueName;
        } else {
            $view = 'bulma.content.'.strtolower($queueName).'.';
            $class = $queueName.'/'.$queueName;
            $model['class'] = 'App\Models\\'.$queueName;
            $model['model'] = 'Models\\'.$queueName;
        }

        if (!empty($queueWith) && $queueWith === 'model' && !class_exists($model['class'])) {
            $this->info('make:model...');
            Artisan::call('make:model', [
                'name' => $model['model'],
                '--migration' => true
            ]);
        }



        $this->info('make:cmf...');
        Artisan::call('generate:controller', [
            'name' => $class
        ]);
        $this->info('make:cmf...');
        Artisan::call('generate:model', [
            'name' => $class
        ]);
        $this->info('make:cmf...');
        Artisan::call('generate:seed', [
            'name' => $class
        ]);


        $array = [
            [
                'view' => $view.'index',
                'stub' => 'view_index'
            ],
            [
                'view' => $view.'components.table',
                'stub' => 'view_table'
            ],
            [
                'view' => $view.'components.modals.create',
                'stub' => 'view_modal_create'
            ],
            [
                'view' => $view.'components.modals.edit',
                'stub' => 'view_modal_edit'
            ],
            [
                'view' => $view.'components.modals.images',
                'stub' => 'view_modal_images'
            ],
            [
                'view' => $view.'components.modals.files.block',
                'stub' => 'view_modal_images_block'
            ],
            [
                'view' => $view.'components.modals.files',
                'stub' => 'view_modal_files'
            ],
            [
                'view' => $view.'components.modals.files.block',
                'stub' => 'view_modal_files_block'
            ],
        ];
        foreach($array as $item) {
            $this->info('make:cmf '.$item['view']);
            Artisan::call('generate:view', [
                'name' => $item['view'],
                '--stub' => $item['stub']
            ]);
        }
        $this->info('cache:clear...');
        Artisan::call('cache:clear');

    }
}
