<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:project {action} {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup project by snapshot.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        switch($this->argument('action')) {
            case 'snapshot':
                $this->info('snapshot...');
                $this->snapshot();
                break;
            default:
                $this->info('default');
                break;
        }
    }

    public function snapshot()
    {
        $name = $this->option('name');
        Artisan::call('snapshot:create', [
            'name' => Carbon::now()->format('d-m-Y').'-'.$name,
        ]);
        Mail::send('email.welcome', [], function ($message) {
            $message->from('dposkachei@gmail.com', 'Dmitri mail');
            $message->to('dimapos@bk.ru');
        });
    }
}
