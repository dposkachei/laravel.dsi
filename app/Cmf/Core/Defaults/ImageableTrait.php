<?php

namespace App\Cmf\Core\Defaults;

use Illuminate\Http\Request;
use App\Models\Image;
use App\Services\Image\ImageService;
use App\Services\Image\Facades\ImagePath;

trait ImageableTrait
{
    /*
    private $class = null;

    private $view = null;

    private $image = [
        'key' => 'category',
        'with_main' => true,
        'unique' => false,
    ];

    private $rules = [
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
    ];

    private $attributes = [
        'image' => 'Изображение',
    ];
    */

    /**
     * Upload images in modal
     *
     * @param Request $request
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageUpload(Request $request, $id)
    {
        $rules = $this->setRulesForUpload($request->file('images'), $this->rules, 'upload', $this->attributes);
        $this->rules = $rules['rules'];
        $this->attributes = $rules['attributes'];
        $validation = $this->validation($request, $this->rules, $this->attributes, 'upload');
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $this->upload($this->class, $request->get('id'), $request->file('images'), $this->image);
            $model = $this->class;
            $oItem = $model::find($id);
            $view = view('bulma.content.'.$this->view.'.components.modals.images.block', [
                'oItem' => $oItem
            ])->render();
            return $this->success([
                'view' => $view,
                //'src' => ImagePath::cache()->main($this->view, 'original', $oItem)
            ]);
        }
    }

    /**
     * Destroy single image
     *
     * @param Request $request
     * @param $id
     * @param $image_id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageDestroy(Request $request, $id, $image_id)
    {
        $oImage = Image::find($image_id);

        $oService = new ImageService();
        $oService->deleteImages($oImage->filename, $this->image['key'], $id);
        $oImage->destroy($oImage->id);
        $this->setDefaultMainImages($this->image['key'], $id);

        $model = $this->class;
        $oItem = $model::find($id);
        $view = view('bulma.content.'.$this->view.'.components.modals.images.block', [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Set main single image
     *
     * @param Request $request
     * @param $id
     * @param $image_id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageMain(Request $request, $id, $image_id)
    {
        $oImage = Image::find($image_id);
        $this->clearMainImages($this->image['key'], $id);
        $oImage->update([
            'is_main' => 1
        ]);
        $model = $this->class;
        $oItem = $model::find($id);
        $view = view('bulma.content.'.$this->view.'.components.modals.images.block', [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    public function clearMainImages($key, $id)
    {
        $oMainImages = Image::where('type', $key)->where('imageable_id', $id)->where('is_main', 1)->get();
        foreach($oMainImages as $oMainImage) {
            $oMainImage->update([
                'is_main' => 0
            ]);
        }
    }

    public function setDefaultMainImages($key, $id)
    {
        $oMainImage = Image::where('type', $key)->where('imageable_id', $id)->where('is_main', 1)->first();
        if (is_null($oMainImage)) {
            $oMainImage = Image::where('type', $key)->where('imageable_id', $id)->first();
            if (!is_null($oMainImage)) {
                $oMainImage->update([
                    'is_main' => 1
                ]);
            }
        }
    }


}