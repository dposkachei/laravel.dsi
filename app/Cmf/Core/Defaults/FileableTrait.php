<?php

namespace App\Cmf\Core\Defaults;

use App\Models\File;
use Illuminate\Http\Request;
use App\Services\File\FileService;

trait FileableTrait
{
    /*
    private $class = null;

    private $view = null;

    private $file = [
        'key' => null,
        'unique' => false,
    ];

    private $rules = [
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];

    private $attributes = [
        'file' => 'Файл',
    ];
    */


    /**
     * Upload images in modal
     *
     * @param Request $request
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function fileUpload(Request $request, $id)
    {
        $rules = $this->setRulesForUpload($request->file('files'), $this->rules, 'uploadFiles', $this->attributes, 'files', 'file');
        $this->rules = $rules['rules'];
        $this->attributes = $rules['attributes'];
        $validation = $this->validation($request, $this->rules, $this->attributes, 'uploadFiles');
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $this->upload($this->class, $request->get('id'), $request->file('files'), $this->file, 'files');
            $model = $this->class;
            $oItem = $model::find($id);
            $view = view('bulma.content.'.$this->view.'.components.modals.files.block', [
                'oItem' => $oItem
            ])->render();
            return $this->success([
                'view' => $view
            ]);
        }
    }

    /**
     * Destroy single image
     *
     * @param Request $request
     * @param $id
     * @param $file_id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function fileDestroy(Request $request, $id, $file_id)
    {
        $oFile = File::find($file_id);

        $oService = new FileService();
        $oService->deleteImages($oFile->filename, $this->file['key'], $id);
        $oFile->destroy($oFile->id);

        $model = $this->class;
        $oItem = $model::find($id);
        $view = view('bulma.content.'.$this->view.'.components.modals.files.block', [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }
}