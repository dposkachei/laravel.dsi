<?php

namespace App\Cmf\Core\Defaults;

use App\Models\File;
use Illuminate\Http\Request;
use App\Services\File\FileService;

trait TextableTrait
{
    public function textModal($id)
    {
        $class = $this->class;
        $oItem = $class::find($id);
        $view = view('bulma.content.components.modals.text', [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    public function textSave(Request $request, $id)
    {
        $class = $this->class;
        $oItem = $class::find($id);
        $text = $request->get('text');
        if (empty($text)) {
            $text = '';
        }
        $oItem->update([
            'text' => $text
        ]);
        return $this->success([
            'toastr' => $this->toastr['update']
        ]);
    }
}