<?php

namespace App\Cmf\Core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class ProjectController extends Controller
{
    protected $oController = null;
    private $sClass;
    private $sPath;

    public function __construct()
    {
        $this->__autoload();
    }


    /**
     * Autoload controller file
     */
    public function __autoload()
    {
        $this->sPath = studly_case(stristr(Route::current()->action['as'], '.', true));
        $this->sClass = $this->sPath.studly_case('_controller');
        $this->sClass = 'App\Cmf\Project\\'.$this->sPath.'\\'.$this->sClass;
        if (!class_exists($this->sClass)) {
            $this->sClass = null;
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->oController = new $this->sClass();
        return $this->oController->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->oController = new $this->sClass();
        return $this->oController->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->oController = new $this->sClass();
        return $this->oController->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->update($request, $id);
    }

    /**
     * Update the status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, $id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->status($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->destroy($id);
    }

    /**
     * @param Request $request
     * @param $name
     * @return mixed
     */
    public function action(Request $request, $name)
    {
        $this->oController = new $this->sClass();
        return $this->oController->{$name}($request);
    }

    /**
     * Update view after add/edit/delete
     *
     * @param Request $request
     * @return mixed
     */
    public function view(Request $request)
    {
        $this->oController = new $this->sClass();
        return $this->oController->view($request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $this->oController = new $this->sClass();
        return $this->oController->search($request);
    }

    /**
     * Create modal window for add item
     *
     * @param Request $request
     * @return mixed
     */
    public function createModal(Request $request)
    {
        $this->oController = new $this->sClass();
        return $this->oController->createModal($request);
    }

    /**
     * Create modal window for edit item
     *
     * @param $id
     * @return mixed
     */
    public function editModal($id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->editModal($id);
    }

    /**
     * Create modal window for edit item
     *
     * @param $id
     * @return mixed
     */
    public function image($id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->image($id);
    }

    /**
     * Create modal window for edit item
     *
     * @param $id
     * @return mixed
     */
    public function files($id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->files($id);
    }

    /**
     * XHR upload images
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function imageUpload(Request $request, $id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->imageUpload($request, $id);
    }

    /**
     * XHR upload images
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function imageDestroy(Request $request, $id, $image_id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->imageDestroy($request, $id, $image_id);
    }

    /**
     * XHR upload files
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function fileUpload(Request $request, $id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->fileUpload($request, $id);
    }

    /**
     * XHR destroy file
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function fileDestroy(Request $request, $id, $image_id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->fileDestroy($request, $id, $image_id);
    }


    /**
     * XHR upload images
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function imageMain(Request $request, $id, $image_id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->imageMain($request, $id, $image_id);
    }

    /**
     * Sort items
     *
     * @param Request $request
     * @return mixed
     */
    public function sort(Request $request)
    {
        $this->oController = new $this->sClass();
        return $this->oController->sort($request);
    }

    /**
     * Sort items
     *
     * @param Request $request
     * @return mixed
     */
    public function query(Request $request)
    {
        $this->oController = new $this->sClass();
        return $this->oController->query($request);
    }

    /**
     * Get modal wysiwyg redactor
     *
     * @param $id
     * @return mixed
     */
    public function textModal($id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->textModal($id);
    }

    /**
     * Save modal with wysiwyg text
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function textSave(Request $request, $id)
    {
        $this->oController = new $this->sClass();
        return $this->oController->textSave($request, $id);
    }



}

