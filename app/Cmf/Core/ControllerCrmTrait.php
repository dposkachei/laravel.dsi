<?php

namespace App\Cmf\Core;

use App\Helpers\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Events\ChangeCacheEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use App\Models\Image;
use App\Models\File;
use App\Services\Image\ImageService;
use App\Services\File\FileService;

trait ControllerCrmTrait
{
    /**
     * Custom action
     *
     * @param Request $request
     * @param $name
     * @return mixed
     */
    public function action(Request $request, $name)
    {
        $model = Str::camel($name);
        if(!method_exists($this, $model)){
            abort(500, 'Method not found');
        }
        return $this->$model($request);
    }

    /**
     * Validate mode
     *
     * @param Request $request
     * @param $rules
     * @param $attributes
     * @param $method
     * @return mixed
     */
    public function validation(Request $request, $rules, $attributes, $method)
    {
        return isset($rules[$method]) ?
            Validator::make($request->all(), $rules[$method], [], $attributes) :
            Validator::make($request->all(), $rules, [], $attributes);
    }

    /**
     * После любых изменений очистить кэш по этому ключу
     *
     * @param $key
     */
    public function afterChange($key, $support = null)
    {
        if (is_array($key)) {
            foreach($key as $value) {
                event(new ChangeCacheEvent($value));
            }
        } else {
            if (!is_null($key)) {
                if (!is_null($support)) {
                    if (is_array($support)) {
                        foreach($support as $value) {
                            event(new ChangeCacheEvent($value));
                        }
                    } else {
                        event(new ChangeCacheEvent($support));
                    }
                }
                event(new ChangeCacheEvent($key));
            }
        }
    }


    /**
     * Get orderBy by session
     *
     * @param $order
     * @param $session
     * @return array
     */
    public function setOrderBy($order, $session)
    {
        if (Session::exists($session) && Session::has($session.'.sort')) {
            $column = Session::has($session.'.sort.column') ? Session::get($session.'.sort.column') : 'created_at';
            $type = Session::has($session.'.sort.type') ? Session::get($session.'.sort.type') : 'desc';
            $order = $column.'.'.$type;
        }

        $orderColumn = strripos($order, '.') ? stristr($order,'.', true) : $order;
        $orderType = substr(stristr($order,'.'), 1);
        $orderBy = [
            'column' => $orderColumn ? $orderColumn : 'created_at',
            'type' => $orderType ? $orderType : 'desc',
        ];
        return $orderBy;
    }

    /**
     * Get query in session
     *
     * @param $session
     * @param null $aQuery
     * @return null
     */
    public function setQuery($session, $aQuery = null)
    {
        if (Session::exists($session) && Session::has($session.'.query')) {
            return Session::get($session.'.query');
        } else {
            return !is_null($aQuery) ? $aQuery : [];
        }
    }

    /**
     * Cache all items
     *
     * @param $class
     * @param $cache
     * @return mixed
     */
    public function getCacheModel($class, $cache)
    {
        return Cache::remember($cache, 60, function() use ($class) {
            return $class::all();
        });
    }


    /**
     * Загрузить изображение/файл
     *
     * @param $class
     * @param $id
     * @param $aFiles
     * @param $options
     * @param string $type
     * @return null|string
     */
    public function upload($class, $id, $aFiles, $options, $type = 'images')
    {
        switch($type) {
            case 'images':
                return $this->uploadByTypeImages($class, $id, $aFiles, $options);
            case 'files':
                return $this->uploadByTypeFiles($class, $id, $aFiles, $options);
            default:
                return null;
        }
    }

    /**
     * Загрузить изображение
     *
     * @param $class
     * @param $id
     * @param $aFiles
     * @param $options
     * @return string
     */
    public function uploadByTypeImages($class, $id, $aFiles, $options)
    {
        $oService = new ImageService();
        $filters = isset($options['filters']) ? $options['filters'] : null;
        try {
            $files = [];
            foreach($aFiles as $aFile) {
                $files[] = $oService->upload($aFile, $options['key'], $id, $filters);
            }

            if (isset($options['unique']) && $options['unique']) {
                $images = Image::where('type', $options['key'])->where('imageable_id', $id)->get();
                if (!empty($images)) {
                    foreach($images as $image) {
                        $oService->deleteImages($image->filename, $options['key'], $image->imageable_id);
                        $image->delete();
                    }
                }
            }
            foreach($files as $key => $file) {
                Image::create([
                    'type' => $options['key'],
                    'imageable_id' => $id,
                    'imageable_type' => $class,
                    'filename' => $file
                ]);
            }
            if (isset($options['with_main']) && $options['with_main']) {
                $oImages = Image::where('type', $options['key'])->where('imageable_id', $id)->where('is_main', 1)->get();
                if (empty($oImages[0])) {
                    $oImages = Image::where('type', $options['key'])->where('imageable_id', $id)->first();
                    $oImages->update([
                        'is_main' => 1
                    ]);
                }
            }
            return $files[0];
        } catch(\Exception $e) {
            dd($e);
            return 'Image ddd not found.';
        }
    }

    /**
     * Загрузить файл
     *
     * @param $class
     * @param $id
     * @param $aFiles
     * @param $options
     * @return string
     */
    public function uploadByTypeFiles($class, $id, $aFiles, $options)
    {
        $oService = new FileService();
        try {
            $files = [];
            foreach ($aFiles as $aFile) {
                $files[] = $oService->upload($aFile, $options['key'], $id);
            }
            if (isset($options['unique']) && $options['unique']) {
                $images = File::where('type', $options['key'])->where('fileable_id', $id)->get();
                if (!empty($images)) {
                    foreach ($images as $image) {
                        $oService->deleteImages($image->filename, $options['key'], $image->fileable_id);
                        $image->delete();
                    }
                }
            }
            foreach ($files as $key => $file) {
                File::create([
                    'type' => $options['key'],
                    'fileable_id' => $id,
                    'fileable_type' => $class,
                    'filename' => $file
                ]);
            }
            return $files[0];
        } catch (\Exception $e) {
            dd($e);
            return 'File ddd not found.';
        }
    }

    /**
     * Правила для загрузки фотографий, с учетом каждой фотографии
     *
     * @param $images
     * @param $rules
     * @param $rulesKey
     * @param $attributes
     * @param string $validatorKey
     * @param string $validatorAttribute
     * @return array
     */
    public function setRulesForUpload($images, $rules, $rulesKey, $attributes, $validatorKey = 'images', $validatorAttribute = 'image')
    {
        foreach($images as $key => $image) {
            $rules[$rulesKey][$validatorKey.'.'.$key] = $rules[$rulesKey][$validatorKey];
            $attributes[$validatorKey.'.'.$key] = $attributes[$validatorAttribute];
        }
        unset($rules[$rulesKey][$validatorKey]);
        return [
            'rules' => $rules,
            'attributes' => $attributes
        ];
    }


    /**
     * Ajax return success with default properties
     *
     * @param array $aData
     * @return array
     */
    public function success(array $aData = [])
    {
        return array_merge([
            'success' => true
        ], $aData);
    }

    /**
     * Ajax return error with default properties
     *
     * @param array $aData
     * @return array
     */
    public function error(array $aData = [])
    {
        return array_merge([
            'success' => false
        ], $aData);
    }

    /**
     * Ajax return json error with status 422
     *
     * @param $text
     * @param string $key
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonError($text, $key = 'error')
    {
        return response()->json([
            $key => [$text]
        ], 422);
    }


    /**
     * Получить поля для сохранения в модель
     *
     * @param $request
     * @param $key
     * @return array|null
     */
    public function getSaveColumns($request, $key)
    {
        if (Cache::has('model_columns')) {
            $cacheColumns = Cache::get('model_columns');
            if (isset($cacheColumns[$key]['fillable'])) {
                $array = [];
                foreach($cacheColumns[$key]['fillable'] as $key => $value) {
                    if ($request->exists($value)) {
                        $array[$value] = $request->get($value);
                    }
                }
                return $array;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Рекурсивно получить массив со всеми дочерними id
     *
     * @param $oModel
     * @param array $array
     * @return array
     */
    public function getChildrenId($oModel, $array = [])
    {
        if (!empty($oModel->children) && !empty($oModel->children[0])) {
            foreach($oModel->children as $oChildren) {
                $array[] = $oChildren->id;
                $array = $this->getChildrenId($oChildren, $array);
            }
        }
        return $array;
    }

    /**
     * Обновление родительских категорий после изменений
     *
     * @param $class
     * @param $oModel
     * @param bool|false $replaceParent
     * @param bool|false $needAddToChildren
     * @param null $oldParent
     */
    public function parentAbleChange($class, $oModel, $replaceParent = false, $needAddToChildren = false, $oldParent = null)
    {
        if ($replaceParent) {
            $oFirstChildren = $class::where('parent_id', $oModel->id)->get();
            foreach($oFirstChildren as $oChildren) {
                $oChildren->update([
                    'parent_id' => null
                ]);
            }
        }
        if ($needAddToChildren && !is_null($oldParent)) {
            $oFirstChildren = $class::where('parent_id', $oModel->id)->get();
            foreach($oFirstChildren as $oChildren) {
                $oChildren->update([
                    'parent_id' => $oldParent
                ]);
            }
        }
    }

    /**
     * Корректировка массива с учетом значений по умолчанию
     *
     * @param $data
     * @param $defaults|null
     * @return mixed
     */
    public function withDefaults($data, $defaults = null)
    {
        if (!is_null($defaults)) {
            foreach ($defaults as $key => $default) {
                if (is_array($default)) {
                    if (isset($default['type']) && $default['type'] === 'slug') {
                        $data[$key] = str_slug($data[$default['column']]);
                    }
                    if (isset($default['filter'])) {
                        $method = 'columnFilter' . studly_case('_' . $default['filter']);
                        if (method_exists($this, $method)) {
                            $data[$key] = $this->{$method}($data[$key]);
                        }
                    }
                } else {
                    $data[$key] = $default;
                }
            }
        }
        return $data;
    }


    public function columnFilterPhone($phone)
    {
        return !empty($phone) ? preg_replace("/[^0-9]/", '', $phone) : null;
    }


    public function sendMail($oModel, $oMailObject, $to = null)
    {
        if (is_null($to)) {
            $to = Config::get('mail.to.address');
        }
        Mail::to($to)
            ->send(new $oMailObject($oModel));
    }

}

