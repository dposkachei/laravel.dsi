<?php

namespace App\Cmf\Core;

trait SeoProviderTrait
{
    /**
     * @param $seoProvider
     * @param $seoName
     * @param null $oModel
     */
    public function seo($seoProvider, $seoName, $oModel = null)
    {
        $oSeo = new $seoProvider();
        if(method_exists($oSeo, $seoName)) {
            if (!is_null($oModel)) {
                $oSeo->$seoName($oModel);
            } else {
                $oSeo->$seoName();
            }
        }
    }
}