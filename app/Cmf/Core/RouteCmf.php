<?php

namespace App\Cmf\Core;

use Route;

abstract class RouteCmf
{

    public static function resource($name, $controller, array $options = [])
    {
        Route::get('/'.$name,                       ['uses' => $controller.'@index',        'as' => $name.'.index']);
        Route::get('/'.$name.'/create',             ['uses' => $controller.'@create',       'as' => $name.'.create']);
        Route::post('/'.$name.'/create',            ['uses' => $controller.'@createModal',  'as' => $name.'.create.modal.post']);
        Route::post('/'.$name,                      ['uses' => $controller.'@store',        'as' => $name.'.store']);
        Route::get('/'.$name.'/{id}',               ['uses' => $controller.'@show',         'as' => $name.'.show']);
        Route::get('/'.$name.'/{id}/edit',          ['uses' => $controller.'@edit',         'as' => $name.'.edit']);
        Route::post('/'.$name.'/{id}/edit',         ['uses' => $controller.'@editModal',    'as' => $name.'.edit.modal.post']);
        Route::post('/'.$name.'/{id}/update',       ['uses' => $controller.'@update',       'as' => $name.'.update']);
        Route::post('/'.$name.'/{id}/delete',       ['uses' => $controller.'@destroy',      'as' => $name.'.destroy']);

        Route::post('/'.$name.'/{id}/image',        ['uses' => $controller.'@image',        'as' => $name.'.image.post']);
        Route::post('/'.$name.'/{id}/files',        ['uses' => $controller.'@files',        'as' => $name.'.files.post']);
        Route::post('/'.$name.'/view',              ['uses' => $controller.'@view',         'as' => $name.'.view.post']);
        Route::post('/'.$name.'/search',            ['uses' => $controller.'@search',       'as' => $name.'.search.post']);
        Route::post('/'.$name.'/action/{name}',     ['uses' => $controller.'@action',       'as' => $name.'.action.post']);
        //Route::get('/'.$name.'/action/{name}',      ['uses' => $controller.'@action',       'as' => $name.'.action']);

        Route::post('/'.$name.'/{id}/status',       ['uses' => $controller.'@status',       'as' => $name.'.status']);
        Route::post('/'.$name.'/sort',              ['uses' => $controller.'@sort',         'as' => $name.'.sort']);
        Route::post('/'.$name.'/query',             ['uses' => $controller.'@query',        'as' => $name.'.query']);


        Route::post('/'.$name.'/{id}/image/upload', ['uses' => $controller.'@imageUpload',  'as' => $name.'.image.upload.post']);
        Route::post('/'.$name.'/{id}/image/{image_id}/destroy',['uses' => $controller.'@imageDestroy', 'as' => $name.'.image.destroy.post']);
        Route::post('/'.$name.'/{id}/image/{image_id}/main',['uses' => $controller.'@imageMain', 'as' => $name.'.image.main.post']);

        Route::post('/'.$name.'/{id}/file/upload', ['uses' => $controller.'@fileUpload',  'as' => $name.'.file.upload.post']);
        Route::post('/'.$name.'/{id}/file/{file_id}/destroy',['uses' => $controller.'@fileDestroy', 'as' => $name.'.file.destroy.post']);

        Route::post('/'.$name.'/{id}/text/modal',   ['uses' => $controller.'@textModal',    'as' => $name.'.text.modal.post']);
        Route::post('/'.$name.'/{id}/text/save',    ['uses' => $controller.'@textSave',     'as' => $name.'.text.save.post']);
    }
}