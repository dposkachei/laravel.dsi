<?php

namespace App\Cmf\Project\Option;

use App\Cmf\Core\SettingsTrait;

trait OptionSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Параметры',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'option',
            ],
        ],
        'icon' => 'fa fa-gear'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'option';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'option';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = 'option';


    private $supportCache = ['options', 'site'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'key' => 'Ключ',
        'title' => 'Заголовок',
        'value' => 'Значение',
        'image' => 'Изображение'
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    public $image = [
        'key' => 'option',
        'with_main' => true,
        'unique' => true,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'key' => 'required|max:255',
            'title' => 'required|max:255',
            'value' => 'required_unless:type,4',
            'type' => 'required',
        ],
        'update' => [
            'key' => 'required|max:255',
            'title' => 'required|max:255',
            'value' => 'required_unless:type,4',
            'type' => 'required',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
    ];

}
