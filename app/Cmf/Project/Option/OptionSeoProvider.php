<?php

namespace App\Cmf\Project\Option;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class OptionSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Option Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Option Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
