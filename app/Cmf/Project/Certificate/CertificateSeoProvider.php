<?php

namespace App\Cmf\Project\Certificate;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class CertificateSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Certificate Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Certificate Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
