<?php

namespace App\Cmf\Project\Certificate;

use App\Cmf\Core\SettingsTrait;

trait CertificateSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Сертификаты',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'certificate',
            ],
        ],
        'icon' => 'fa fa-certificate'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'certificate';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'certificate';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = 'certificate';

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'certificate',
        'with_main' => true,
        'unique' => false,
    ];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'certificate',
        'unique' => true,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
            'priority' => 'required|max:255',
            'status' => 'required|max:255',
        ],
        'update' => [
            'title' => 'required|max:255',
            'priority' => 'required|max:255',
            'status' => 'required|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];

}
