<?php

namespace App\Cmf\Project\Product;

use App\Cmf\Core\SettingsTrait;

trait ProductSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Продукты',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'product',
            ],
        ],
        'icon' => 'fa fa-cubes'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'product';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'product';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = 'product';

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 0,
            'url' => [
                'column' => 'title',
                'type' => 'slug'
            ]
        ],
        'update' => [
            'url' => [
                'column' => 'title',
                'type' => 'slug'
            ]
        ],
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'description' => 'Описание',
        'status' => 'Статус',
        'category_id' => 'Категория',
        'image' => 'Изображение'
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'product',
        'with_main' => true,
        'unique' => false,
        'filters' => [
            'block' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '10:20'
                ]
            ]
        ]
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
            'category_id' => 'required|max:255',
        ],
        'update' => [
            'title' => 'required|max:255',
            'category_id' => 'required|max:255',
            'url' => 'required|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
    ];

    /**
     * Требуется для вывода количества найденых продукций
     * Например: "Найдено 2 продукции"
     *
     * @var array
     */
    private $find = [
        'units' => ['продукция', 'продукции', 'продукций'],
        'gender' => true
    ];

    /**
     * Переопределение свойства $toastr SettingsTrait
     *
     * @var array
     */
    public $_toastr = [
        'status'    => [
            0 => [
                'title' => 'Успех',
                'text' => 'Продукция не активна',
                'type' => 'success',
            ],
            1 => [
                'title' => 'Успех',
                'text' => 'Продукция активна',
                'type' => 'success',
            ],
        ],
    ];

}
