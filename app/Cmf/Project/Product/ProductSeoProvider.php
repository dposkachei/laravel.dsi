<?php

namespace App\Cmf\Project\Product;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class ProductSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Product Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Product Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
