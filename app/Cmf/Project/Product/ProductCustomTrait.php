<?php

namespace App\Cmf\Project\Product;

use App\Models\Certificate;
use App\Models\ProductParameters;
use Illuminate\Http\Request;

trait ProductCustomTrait
{
    /**
     * Модальное окно с формой
     *
     * @param Request $request
     * @return mixed
     */
    public function getSeoParameters(Request $request)
    {
        $id = $request->get('id');
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view('bulma.content.'.$this->view.'.components.modals.seo', [
            'oItem' => $oItem,
            'oSeo' => !is_null($oItem->seo) ? $oItem->seo : null
        ])->render();

        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Сохранение формы
     *
     * @param Request $request
     * @return mixed
     */
    public function saveSeoParameters(Request $request)
    {
        $id = $request->get('id');
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $oSeo = $oItem->seo;
        if (!is_null($oSeo)) {
            $oSeo->update([
                'description' => $request->get('description'),
                'keywords' => $request->get('keywords'),
            ]);
            return $this->success([
                'toastr' => $this->toastr['update']
            ]);
        } else {
            ProductParameters::create([
                'product_id' => $oItem->id,
                'type' => 1,
                'description' => $request->get('description'),
                'keywords' => $request->get('keywords'),
            ]);
            return $this->success([
                'toastr' => $this->toastr['store']
            ]);
        }
    }


    /**
     * Модально окно с сертификатами
     *
     * Возможно не используется!!!!!!!!!
     *
     * @param Request $request
     * @return mixed
     */
    public function getProductCertificates(Request $request)
    {
        $id = $request->get('id');
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $oItems = Certificate::where('status', 1)->get();
        $view = view('bulma.content.'.$this->view.'.components.modals.certificates', [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Модально окно с сертификатами
     *
     * @param Request $request
     * @return mixed
     */
    public function getCertificates(Request $request)
    {
        $oCertificates = Certificate::forProducts()->where('status', 1)->get();
        $id = $request->get('id');
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view('bulma.content.'.$this->view.'.components.modals.certificates', [
            'oCertificates' => $oCertificates,
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Сохранение сертификатов
     *
     * @param Request $request
     * @return mixed
     */
    public function saveCertificates(Request $request)
    {
        $id = $request->get('id');
        $aData = $request->get('product');
        $aCertificates = $aData['certificates'];
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        foreach($aCertificates as $key => $aCertificate) {
            if (isset($aCertificate['status'])) {
                if (is_null($oItem->certificates()->where('certificate_id', $aCertificate['id'])->first())) {
                    $oItem->certificates()->attach($aCertificate['id'], [
                        'priority' => $aCertificate['priority'],
                    ]);
                } else {
                    $oItem->certificates()->updateExistingPivot($aCertificate['id'], [
                        'priority' => $aCertificate['priority'],
                    ]);
                }
            } else {
                if (!is_null($oItem->certificates()->where('certificate_id', $aCertificate['id'])->first())) {
                    $oItem->certificates()->detach($aCertificate['id']);
                }
            }
        }
        return $this->success([
            'toastr' => $this->toastr['update']
        ]);
    }
}