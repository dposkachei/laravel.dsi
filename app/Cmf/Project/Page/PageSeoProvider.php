<?php

namespace App\Cmf\Project\Page;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class PageSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Page Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Page Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
