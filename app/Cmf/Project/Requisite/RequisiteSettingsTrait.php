<?php

namespace App\Cmf\Project\Requisite;

use App\Cmf\Core\SettingsTrait;

trait RequisiteSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Реквизиты',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'requisite',
            ],
        ],
        'icon' => 'fa fa-address-book'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'requisite';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'requisite';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = 'requisite';

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'value' => 'Значение',
        'priority' => 'Приоритет',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'requisite',
        'with_main' => true,
        'unique' => false,
    ];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'requisite',
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
            'value' => 'required|max:255',
            'priority' => 'required|integer|max:255',
        ],
        'update' => [
            'title' => 'required|max:255',
            'value' => 'required|max:255',
            'priority' => 'required|integer|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];

}
