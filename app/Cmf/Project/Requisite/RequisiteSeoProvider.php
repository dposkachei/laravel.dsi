<?php

namespace App\Cmf\Project\Requisite;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class RequisiteSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Requisite Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Requisite Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
