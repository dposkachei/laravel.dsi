<?php

namespace App\Cmf\Project\User;

use App\Cmf\Core\SettingsTrait;

trait UserSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Пользователи',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'user',
            ],
        ],
        'icon' => 'fa fa-users'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'user';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'user';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = 'user';

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = true;

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение'
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'user',
        'with_main' => true,
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'first_name' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|confirmed|max:255',
        ],
        'update' => [
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'confirmed|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
    ];

}
