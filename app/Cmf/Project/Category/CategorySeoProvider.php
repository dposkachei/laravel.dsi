<?php

namespace App\Cmf\Project\Category;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class CategorySeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Category Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Category Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
