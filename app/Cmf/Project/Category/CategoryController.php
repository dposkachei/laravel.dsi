<?php

namespace App\Cmf\Project\Category;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Cmf\Core\ControllerCrmTrait;
use App\Cmf\Core\SeoProviderTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\FileableTrait;
use App\Cmf\Core\Defaults\TextableTrait;


use App\Models\Category;

class CategoryController extends Controller
{
    use CategorySettingsTrait, ControllerCrmTrait, SeoProviderTrait;
    use ImageableTrait;
    use FileableTrait;
    use TextableTrait;

    /**
     * Cache model for all items
     *
     * @var mixed|null
     */
     private $cacheModel = null;

    /**
     * Seo Provider
     * @var null
     */
    private $seo = null;

    /**
     * Class model
     * @var null
     */
    private $class = Category::class;

    /**
     * Pagination url
     * @var string
     */
    private $tableUrl;

    /**
     * Table limit item
     * @var int
     */
    private $tableLimit = 100;

    /**
     * Default order columns
     * @var string
     */
    private $order = 'priority.desc';

    /**
     * Default query ['status' => 1, ]
     */
    private $aQuery = [
        'parent_id' => null
    ];

    private $with = [
        'products', 'images'
    ];

    /**
     * Order columns
     * @var array
     */
    private $orderBy = [];

    public function __construct()
    {
        /**
         * Default session query
         */
        $this->aQuery = $this->setQuery($this->session, $this->aQuery);

        /**
         * Route for pagination
         */
        $this->tableUrl = route($this->view.'.view.post');

        /**
         * Default class model
         */
        $this->cacheModel = $this->getCacheModel($this->class, $this->cache);

        /**
         * Order columns
         */
        $this->orderBy = $this->setOrderBy($this->order, $this->session);

        $this->seo = CategorySeoProvider::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->seo($this->seo, __FUNCTION__);

        $oItems = $this->paginate($this->class);

        return view('bulma.content.'.$this->view.'.index', [
            'oItems' => $oItems
        ]);
    }

    /**
     * Default query items
     *
     * @param $oModel
     * @param null $type
     * @param array $aColumns
     * @return mixed
     */
    public function paginate($oModel, $type = null, $aColumns = [])
    {
        $oItems = $oModel::with('products', 'images')->orderBy($this->orderBy['column'], $this->orderBy['type']);

        if (!is_null($type) && $type === 'search') {
            foreach($aColumns as $key => $value) {
                $oItems = $oItems->where($key, 'like', '%'.$value.'%');
            }
        }
        if (!empty($this->aQuery)) {
            foreach($this->aQuery as $key => $value) {
                $oItems = $oItems->where($key, $value);
            }
        }
        return $oItems->paginate($this->tableLimit)->withPath($this->tableUrl);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->seo($this->seo, __FUNCTION__);
        $error = $this->error;

        return view('bulma.content.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response | array
     */
    public function store(Request $request)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->cache);
            $data = $this->withDefaults($data, $this->defaults[__FUNCTION__]);
            if (!is_null($data)) {
                $model::create($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $this->seo($this->seo, 'show', $oItem);
        return view('bulma.content.'.$this->view.'.show', [
            'oItem' => $oItem
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        return view('bulma.content.'.$this->view.'.edit', [
            'oItem' => $oItem
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->cache);
            $data = $this->withDefaults($data, $this->defaults[__FUNCTION__]);
            if (!is_null($data)) {
                $oModel = $model::find($id);
                $needAddToChildren = false;
                $replaceParent = false;
                $oldParent = null;
                if ($oModel->parent_id === null) {
                    $replaceParent = true;
                }
                if (!empty($data['parent_id']) && in_array($data['parent_id'], $this->getChildrenId($oModel))) {
                    $needAddToChildren = true;
                    $oldParent = $oModel->parent_id;
                }

                $oModel->update($data);


                $this->parentAbleChange($this->class, $oModel, $replaceParent, $needAddToChildren, $oldParent);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Update the status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, $id)
    {
        $model = $this->class;
        $model::find($id)->update([
            'status' => $request->get('status'),
        ]);
        $message = $request->get('status') ? $this->toastr['status'][1] : $this->toastr['status'][0];
        $this->afterChange($this->cache);
        return $this->success([
            'toastr' => $message
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->class;

        $oModel = $model::find($id);
        $replaceParent = false;
        $oldParent = null;
        if ($oModel->parent_id === null) {
            $replaceParent = true;
        }
        $needAddToChildren = true;
        $oldParent = $oModel->parent_id;

        $this->parentAbleChange($this->class, $oModel, $replaceParent, $needAddToChildren, $oldParent);

        $oModel->delete();
        $this->afterChange($this->cache);
        return $this->success([
            'toastr' => $this->toastr[__FUNCTION__]
        ]);
    }

    /**
     * Генерация после добавление/изменения/удаления
     *
     * @param Request $request
     * @return array
     */
    public function view(Request $request)
    {
        $oItems = $this->paginate($this->class);

        return $this->table($oItems);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        if (!empty($request->get('search'))) {
            $oItems = $this->paginate($this->class, 'search', [
                'title' => $request->get('search')
            ]);
        } else {
            $oItems = $this->paginate($this->class);
        }
        return $this->table($oItems);
    }



    /**
     * Обновление таблицы после добавление/изменения/удаления
     *
     * @param $oItems
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function table($oItems)
    {
        return $this->success([
            'view' => view('bulma.content.'.$this->view.'.components.table', [
                'oItems' => $oItems
            ])->render(),
            'count' => $oItems->total()
        ]);
    }

    /**
     * Модальное окно для добавления
     *
     * @param Request $request
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function createModal(Request $request)
    {
        $model = $this->class;
        $oParents = $model::orderBy('title')->get();
        $view = view('bulma.content.'.$this->view.'.components.modals.create', [
            'oParents' => $oParents,
            'nActiveParentId' => $request->exists('parent_id') ? intval($request->get('parent_id')) : null
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Модальное окно для изменений
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function editModal($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $model = $this->class;
        $oParents = $model::orderBy('title')->get()->reject(function($item) use ($oItem) {
            return $item->id === $oItem->id;
        });
        $view = view('bulma.content.'.$this->view.'.components.modals.edit', [
            'oItem' => $oItem,
            'oParents' => $oParents,
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }



    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function image($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view('bulma.content.'.$this->view.'.components.modals.images', [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function files($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view('bulma.content.'.$this->view.'.components.modals.files', [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }




    /**
     * Set sort in session
     *
     * @param Request $request
     * @return array
     */
    public function sort(Request $request)
    {
        if ($request->exists('sort')) {
            Session::put($this->session.'.sort.column', $request->get('sort'));
        }
        if ($request->exists('type')) {
            if (Session::has($this->session.'.sort.type') && Session::get($this->session.'.sort.type') === 'desc') {
                Session::put($this->session.'.sort.type', 'asc');
            } else {
                Session::put($this->session.'.sort.type', $request->get('type'));
            }
        }
        return $this->success();
    }

    /**
     * Ser where query in session
     *
     * @param Request $request
     * @return array
     */
    public function query(Request $request)
    {
        if (isset($request->all) && !empty($request->all)) {
            Session::forget($this->session.'.query');
        }
        if (isset($request->status) && !empty($request->status)) {
            Session::put($this->session.'.query', [
                'status' => $request->get('status')
            ]);
        }
        return $this->success();
    }

}
