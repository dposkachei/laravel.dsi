<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type')->nullable()->default(null);
            $table->integer('product_id')->unsigned()->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->text('keywords')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::table('product_parameters', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_parameters', function($table) {
            $table->dropForeign('product_parameters_product_id_foreign');
        });
        Schema::dropIfExists('product_parameters');
    }
}
