<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->nullable()->default(null);
            $table->integer('certificate_id')->unsigned()->nullable()->default(null);
            $table->integer('priority')->default(0);
            $table->timestamps();
        });
        Schema::table('product_certificates', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
        Schema::table('product_certificates', function($table) {
            $table->foreign('certificate_id')->references('id')->on('certificates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_certificates', function($table) {
            $table->dropForeign('product_certificates_product_id_foreign');
        });
        Schema::table('product_certificates', function($table) {
            $table->dropForeign('product_certificates_certificate_id_foreign');
        });
        Schema::dropIfExists('product_certificates');
    }
}
