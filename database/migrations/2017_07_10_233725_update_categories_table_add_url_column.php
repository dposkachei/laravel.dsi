<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCategoriesTableAddUrlColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('url')->nullabe()->default(null)->after('title');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->tinyInteger('pay_count')->after('text')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('url');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('pay_count');
        });
    }
}
