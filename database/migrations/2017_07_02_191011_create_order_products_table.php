<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable()->default(null);
            $table->integer('product_id')->unsigned()->nullable()->default(null);
            $table->timestamps();
        });
        Schema::table('order_products', function($table) {
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
        Schema::table('order_products', function($table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_products', function($table) {
            $table->dropForeign('order_products_order_id_foreign');
        });
        Schema::table('order_products', function($table) {
            $table->dropForeign('order_products_product_id_foreign');
        });
        Schema::dropIfExists('order_products');
    }
}
