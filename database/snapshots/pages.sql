
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` VALUES (1,1,'1234',1,'2017-06-26 00:03:42','2017-06-26 00:03:42','2017-06-26 00:03:42');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Условия доставки','Текст','truck',1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(2,'Наши гарантии','Текст','shield',1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(3,'Условия оплаты','Текст','credit-card',1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(4,'Качество','Здесь будет текст в несколько строк. Здесь будет текст в несколько строк','diamond',2,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(5,'Долговечность','Здесь будет текст в несколько строк. Здесь будет текст в несколько строк','lock',2,'2017-06-26 00:03:42','2017-06-26 00:03:42');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Адгезионные добавки к битумам','<p style=\"padding: 0px; margin-bottom: 13px; outline: none;\"><span style=\"padding: 0px; margin: 0px; outline: none;\"><b>Добавки для БМП должны обладат определенными свойствами:</b></span></p><ul><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостойкость, так как температура приготовления БМП на 25–35°С выше, чем при технологической подготовке битумов;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостабильность, так как возможно хранение и транспортирование БМП;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">не ухудшать физико-механические свойства БМП и асфальтобетона;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">способствовать стабилизации полимерной сетки при формировании пространственной структуры;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">обеспечивать максимальную адгезию БМП через несколько часов после ввода АД в вяжущее.</li></ul>',NULL,100,1,'2017-06-26 00:05:33','2017-06-26 00:10:04'),(2,'Стабилизирующие добавки для щебеночно-мастичных асфальтобетонных смесей','<p style=\"padding: 0px; margin-bottom: 13px; outline: none;\"><span style=\"padding: 0px; margin: 0px; outline: none;\"><b>Добавки для БМП должны обладат определенными свойствами:</b></span></p><ul><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостойкость, так как температура приготовления БМП на 25–35°С выше, чем при технологической подготовке битумов;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостабильность, так как возможно хранение и транспортирование БМП;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">не ухудшать физико-механические свойства БМП и асфальтобетона;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">способствовать стабилизации полимерной сетки при формировании пространственной структуры;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">обеспечивать максимальную адгезию БМП через несколько часов после ввода АД в вяжущее.</li></ul>',NULL,90,1,'2017-06-26 00:05:48','2017-06-26 00:05:48'),(3,'Пластификатор ПБВ','<p style=\"padding: 0px; margin-bottom: 13px; outline: none;\"><span style=\"padding: 0px; margin: 0px; outline: none;\"><b>Добавки для БМП должны обладат определенными свойствами:</b></span></p><ul><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостойкость, так как температура приготовления БМП на 25–35°С выше, чем при технологической подготовке битумов;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостабильность, так как возможно хранение и транспортирование БМП;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">не ухудшать физико-механические свойства БМП и асфальтобетона;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">способствовать стабилизации полимерной сетки при формировании пространственной структуры;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">обеспечивать максимальную адгезию БМП через несколько часов после ввода АД в вяжущее.</li></ul>',NULL,80,1,'2017-06-26 00:06:04','2017-06-26 00:06:04'),(4,'Дорожная пропитка для асфальтобетона','<p style=\"padding: 0px; margin-bottom: 13px; outline: none;\"><span style=\"padding: 0px; margin: 0px; outline: none;\"><b>Добавки для БМП должны обладат определенными свойствами:</b></span></p><ul><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостойкость, так как температура приготовления БМП на 25–35°С выше, чем при технологической подготовке битумов;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостабильность, так как возможно хранение и транспортирование БМП;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">не ухудшать физико-механические свойства БМП и асфальтобетона;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">способствовать стабилизации полимерной сетки при формировании пространственной структуры;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">обеспечивать максимальную адгезию БМП через несколько часов после ввода АД в вяжущее.</li></ul>',NULL,70,1,'2017-06-26 00:06:20','2017-06-26 00:06:20'),(5,'Модификатор минерального порошка','<p style=\"padding: 0px; margin-bottom: 13px; outline: none;\"><span style=\"padding: 0px; margin: 0px; outline: none;\"><b>Добавки для БМП должны обладат определенными свойствами:</b></span></p><ul><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостойкость, так как температура приготовления БМП на 25–35°С выше, чем при технологической подготовке битумов;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостабильность, так как возможно хранение и транспортирование БМП;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">не ухудшать физико-механические свойства БМП и асфальтобетона;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">способствовать стабилизации полимерной сетки при формировании пространственной структуры;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">обеспечивать максимальную адгезию БМП через несколько часов после ввода АД в вяжущее.</li></ul>',NULL,60,1,'2017-06-26 00:06:38','2017-06-26 00:06:38'),(6,'Сетки армирующие','<p style=\"padding: 0px; margin-bottom: 13px; outline: none;\"><span style=\"padding: 0px; margin: 0px; outline: none;\"><b>Добавки для БМП должны обладат определенными свойствами:</b></span></p><ul><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостойкость, так как температура приготовления БМП на 25–35°С выше, чем при технологической подготовке битумов;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">иметь высокую термостабильность, так как возможно хранение и транспортирование БМП;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">не ухудшать физико-механические свойства БМП и асфальтобетона;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">способствовать стабилизации полимерной сетки при формировании пространственной структуры;</li><li style=\"padding: 0px; margin-bottom: 13px; outline: none;\">обеспечивать максимальную адгезию БМП через несколько часов после ввода АД в вяжущее.</li></ul>',NULL,50,1,'2017-06-26 00:06:51','2017-06-26 00:06:51');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
INSERT INTO `certificates` VALUES (1,'Сертификат №1','Описание сертификата',0,0,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(2,'Сертификат №2','Описание сертификата',1,0,1,'2017-06-26 00:03:42','2017-06-26 00:13:03'),(3,'Сертификат №3','Описание сертификата',1,0,1,'2017-06-26 00:03:42','2017-06-26 00:13:08');
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `commentable_id` int(11) DEFAULT NULL,
  `commentable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commented_id` int(11) DEFAULT NULL,
  `commented_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `rate` double(15,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `type` int(11) NOT NULL DEFAULT '1',
  `tooltip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'Email','email','dsi@dsi-ug.ru',1,NULL,0,2,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(2,'Телефон','phone','+7 (863) 229-59-74',1,NULL,0,2,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(3,'Телефон','phone','+7 (863) 229-59-74, 229-16-85, факс 210-77-45',1,NULL,0,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(4,'Юридический адрес','address','344019, г. Ростов-на-Дону, ул. 17 линия 2/8',1,NULL,0,2,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(5,'Почтовый адрес','address','344019, г. Ростов-на-Дону, ул. 17 линия 2/8',1,NULL,0,1,'2017-06-26 00:03:42','2017-06-26 00:03:42');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileable_id` int(11) NOT NULL,
  `fileable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (1,'certificate',1,'App\\Models\\Certificate',NULL,'pdf-sample.pdf',1,'2017-06-26 00:12:52','2017-06-26 00:12:52');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_id` int(11) NOT NULL,
  `imageable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (2,'category',1,'App\\Models\\Category',NULL,'TqlxvLQdpBlK.jpg',1,1,'2017-06-26 00:11:19','2017-06-26 00:11:19'),(3,'category',2,'App\\Models\\Category',NULL,'pRoQJS65oVKZ.jpg',1,1,'2017-06-26 00:11:27','2017-06-26 00:11:27'),(4,'category',3,'App\\Models\\Category',NULL,'qEavAyMASUmQ.jpg',1,1,'2017-06-26 00:11:34','2017-06-26 00:11:34'),(5,'category',4,'App\\Models\\Category',NULL,'czaEnMITyEIC.jpg',1,1,'2017-06-26 00:11:40','2017-06-26 00:11:40'),(6,'category',5,'App\\Models\\Category',NULL,'P12oZ2HmoE70.jpg',1,1,'2017-06-26 00:11:46','2017-06-26 00:11:46'),(7,'category',6,'App\\Models\\Category',NULL,'P9rKRwQalVZk.jpg',1,1,'2017-06-26 00:11:54','2017-06-26 00:11:54'),(8,'certificate',1,'App\\Models\\Certificate',NULL,'gKxa0zZ7couT.jpg',1,1,'2017-06-26 00:12:42','2017-06-26 00:12:42'),(9,'certificate',2,'App\\Models\\Certificate',NULL,'zZbqnVOmNdpj.png',1,1,'2017-06-26 00:13:27','2017-06-26 00:13:27'),(10,'certificate',3,'App\\Models\\Certificate',NULL,'ucPvPNbsSjAh.png',1,1,'2017-06-26 00:13:34','2017-06-26 00:13:34'),(12,'product',1,'App\\Models\\Product',NULL,'IcFCB3zIqDsN.jpg',1,1,'2017-06-26 00:17:26','2017-06-26 00:17:26'),(13,'product',2,'App\\Models\\Product',NULL,'kyNwWYRALhMD.jpg',1,1,'2017-06-26 00:35:53','2017-06-26 11:35:42'),(14,'product',2,'App\\Models\\Product',NULL,'XAj7bL1rUQZS.jpg',0,1,'2017-06-26 11:35:28','2017-06-26 11:35:42'),(15,'page',1,'App\\Models\\Page',NULL,'4yAAg6uHJwxW.jpg',1,1,'2017-07-10 00:25:59','2017-07-10 00:25:59'),(18,'option',2,'App\\Models\\Option',NULL,'oOKGR8QnKKZF.png',1,1,'2017-07-10 00:32:34','2017-07-10 00:32:34');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (25,'0000_00_00_000000_create_comments_table',1),(26,'2014_07_02_230147_migration_cartalyst_sentinel',1),(27,'2017_01_11_222838_create_categories_table',1),(28,'2017_02_24_180912_create_products_table',1),(29,'2017_03_03_100000_create_options_table',1),(30,'2017_03_24_001152_create_images_table',1),(31,'2017_05_08_003719_create_files_table',1),(32,'2017_05_29_204738_create_requisites_table',1),(33,'2017_06_07_222328_create_articles_table',1),(34,'2017_06_08_234138_create_contacts_table',1),(35,'2017_06_18_215827_create_certificates_table',1),(36,'2017_06_20_224343_create_partners_table',1),(37,'2017_06_25_182436_create_product_certificates_table',1),(38,'2017_07_02_185929_create_orders_table',2),(39,'2017_07_02_191011_create_order_products_table',2),(40,'2017_07_08_004428_create_pages_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `type` int(11) NOT NULL DEFAULT '1',
  `tooltip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` VALUES (1,'Заголовок сайта','app.title','ООО \"Дорстройиндустрия\"',1,NULL,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(2,'Иконка сайта','app.favicon',NULL,4,NULL,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(3,'Ключевые слова','app.keywords','keywords',2,NULL,1,'2017-06-26 00:03:42','2017-07-10 00:33:31'),(4,'Описание сайта','app.description','Description',2,NULL,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(5,'Права','app.copyright','© ООО \"ДорСтройИндустрия\"',1,NULL,1,'2017-06-26 00:03:42','2017-06-26 00:03:42');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `order_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_products_order_id_foreign` (`order_id`),
  KEY `order_products_product_id_foreign` (`product_id`),
  CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `order_products` WRITE;
/*!40000 ALTER TABLE `order_products` DISABLE KEYS */;
INSERT INTO `order_products` VALUES (1,1,1,NULL,NULL),(2,3,2,NULL,NULL);
/*!40000 ALTER TABLE `order_products` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) unsigned DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,'Тестовый заказ','test@test.com',78005553535,NULL,1,'2017-07-10 00:21:55','2017-07-10 00:21:55'),(2,1,'Второй Тестовый заказ','test@test.com',NULL,NULL,1,'2017-07-10 00:21:55','2017-07-10 00:21:55'),(3,1,'Дмитрий',NULL,71111111111,NULL,1,'2017-07-10 00:34:24','2017-07-10 00:34:24'),(4,2,'Тестовый запрос',NULL,71111111111,NULL,1,'2017-07-10 00:35:07','2017-07-10 00:35:07');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Дорстройиндустрия','Главная','main','<p><b>\"Дорстройиндустрия\" </b>- динамично развивающаяся российская компания. </p><p>Мы специализируемся на рынке <b>дорожно-строительных материалов</b>, создания и комплексного оснащения аналитических, испытательных и дорожно-строительных лабораторий, контролирующих качество проводимых работ и используемых материалов.</p>',1,'2017-07-10 00:21:43','2017-07-10 00:21:43'),(2,'Продукции','Продукции','products',NULL,0,'2017-07-10 00:21:43','2017-07-10 00:29:41'),(3,'Категории','Категории','categories',NULL,0,'2017-07-10 00:21:43','2017-07-10 00:29:42'),(4,'\"Дорстройиндустрия\" - партнерство','Партнеры','partners','<p>ООО «ДорСтройИндустрия» было создано в 2005 году. Основными направлениями деятельности организации является комплектация строительных лабораторий испытательным оборудованием и средствами измерений, торговля адгезионными и стабилизирующими добавками для битумов и асфальтобетонов, а также другими дорожно-строительными материалами.</p><p>Компания является зарегистрированным дилером ряда российских и зарубежных производителей дорожно-строительных материалов.</p><p>Среди них:</p>',1,'2017-07-10 00:21:43','2017-07-10 00:21:43'),(5,'Сертификаты','Сертификаты','certificates',NULL,0,'2017-07-10 00:21:43','2017-07-10 00:21:43'),(6,'ООО «ДСИ» Общество с ограниченной ответственностью «Дорстройиндустрия»','Контакты','contacts',NULL,1,'2017-07-10 00:21:43','2017-07-10 00:21:43');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (1,'ООО \"Фирма ГБЦ\"','http://a.ru','Описание партнера',1,'2017-06-26 00:03:42','2017-06-26 00:27:56');
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `persistences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `persistences` WRITE;
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` VALUES (1,1,'LAgTeBXm8jMiu6oc15wr0tGHgbY9antK','2017-06-26 00:04:43','2017-06-26 00:04:43');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `product_certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `certificate_id` int(10) unsigned DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_certificates_product_id_foreign` (`product_id`),
  KEY `product_certificates_certificate_id_foreign` (`certificate_id`),
  CONSTRAINT `product_certificates_certificate_id_foreign` FOREIGN KEY (`certificate_id`) REFERENCES `certificates` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_certificates_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `product_certificates` WRITE;
/*!40000 ALTER TABLE `product_certificates` DISABLE KEYS */;
INSERT INTO `product_certificates` VALUES (1,1,2,100,'2017-06-26 00:15:02','2017-06-26 11:34:24'),(2,1,3,50,'2017-06-26 00:15:02','2017-06-26 11:34:24'),(5,2,2,40,'2017-06-26 11:34:44','2017-06-26 11:34:59'),(6,2,3,10,'2017-06-26 11:34:44','2017-06-26 11:34:59');
/*!40000 ALTER TABLE `product_certificates` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_category_id_foreign` (`category_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,6,'Геосетка \"Армогрунт\" для армирования слабых грунтовых оснований','geosetka-armogrunt-dlya-armirovaniya-slabykh-gruntovykh-osnovaniy','<p>Геосетка «Армогрунт» является разновидностью геосеток марки «Армопол», и отличается от неё пропитывающим составом.&nbsp;<br style=\"outline: none !important;\"><br style=\"outline: none !important;\">Специально разработанная рецептура связующего, на основе полимеров, придает ей улучшенные свойства при эксплуатации в водной, щелочной и кислотной среде. Геосетка «Армогрунт» применяется как армирующая прослойка в грунтовых конструкциях на слабом основании. Первопричиной существующих проблем качества дорожного полотна является недостаточно эффективное устройство основания дорожных одежд.&nbsp;<br style=\"outline: none !important;\"><br style=\"outline: none !important;\">Геосетка «Армогрунт» позволяет обеспечить общую устойчивость насыпи, сократить неравномерность осадков, снизить требуемый объём применяемых материалов.</p><p>Геосетка «Армогрунт» является разновидностью геосеток марки «Армопол», и отличается от неё пропитывающим составом.&nbsp;<br style=\"outline: none !important;\"><br style=\"outline: none !important;\">Специально разработанная рецептура связующего, на основе полимеров, придает ей улучшенные свойства при эксплуатации в водной, щелочной и кислотной среде. Геосетка «Армогрунт» применяется как армирующая прослойка в грунтовых конструкциях на слабом основании. Первопричиной существующих проблем качества дорожного полотна является недостаточно эффективное устройство основания дорожных одежд.&nbsp;<br style=\"outline: none !important;\"><br style=\"outline: none !important;\">Геосетка «Армогрунт» позволяет обеспечить общую устойчивость насыпи, сократить неравномерность осадков, снизить требуемый объём применяемых материалов.<br></p><p><br></p><table class=\"table table-bordered\"><tbody><tr><td><b>Наименование показателей</b><br></td><td><b>Армогрунт-50</b><br></td><td><b>Армогрунт-100</b><br></td><td><b>Армогрунт-120</b><br></td></tr><tr><td>Ширина полотна, см<br></td><td>120, 240<br></td><td>120, 240<br></td><td>120, 240<br></td></tr><tr><td>Прочностные показатели, кН/м, не менее<br></td><td>50<br></td><td>100</td><td>100</td></tr><tr><td>Удлинение, % не более<br></td><td>4<br></td><td>4<br></td><td>4<br></td></tr><tr><td>Размеры ячеек, мм<br></td><td>25х25<br></td><td>25х25<br></td><td>25х25</td></tr></tbody></table>',1,'2017-06-26 00:14:52','2017-06-26 00:17:30'),(2,6,'Геосетка \"Армогрунт\" для армирования слабых грунтовых оснований','geosetka-armogrunt-dlya-armirovaniya-slabykh-gruntovykh-osnovaniy','<p>Геосетка «Армогрунт» является разновидностью геосеток марки «Армопол», и отличается от неё пропитывающим составом.&nbsp;<br style=\"outline: none !important;\"><br style=\"outline: none !important;\">Специально разработанная рецептура связующего, на основе полимеров, придает ей улучшенные свойства при эксплуатации в водной, щелочной и кислотной среде. Геосетка «Армогрунт» применяется как армирующая прослойка в грунтовых конструкциях на слабом основании. Первопричиной существующих проблем качества дорожного полотна является недостаточно эффективное устройство основания дорожных одежд.&nbsp;<br style=\"outline: none !important;\"><br style=\"outline: none !important;\">Геосетка «Армогрунт» позволяет обеспечить общую устойчивость насыпи, сократить неравномерность осадков, снизить требуемый объём применяемых материалов.</p><p>Геосетка «Армогрунт» является разновидностью геосеток марки «Армопол», и отличается от неё пропитывающим составом.&nbsp;<br style=\"outline: none !important;\"><br style=\"outline: none !important;\">Специально разработанная рецептура связующего, на основе полимеров, придает ей улучшенные свойства при эксплуатации в водной, щелочной и кислотной среде. Геосетка «Армогрунт» применяется как армирующая прослойка в грунтовых конструкциях на слабом основании. Первопричиной существующих проблем качества дорожного полотна является недостаточно эффективное устройство основания дорожных одежд.&nbsp;<br style=\"outline: none !important;\"><br style=\"outline: none !important;\">Геосетка «Армогрунт» позволяет обеспечить общую устойчивость насыпи, сократить неравномерность осадков, снизить требуемый объём применяемых материалов.<br></p><p><br></p><table class=\"table table-bordered\"><tbody><tr><td><b>Наименование показателей</b><br></td><td><b>Армогрунт-50</b><br></td><td><b>Армогрунт-100</b><br></td><td><b>Армогрунт-120</b><br></td></tr><tr><td>Ширина полотна, см<br></td><td>120, 240<br></td><td>120, 240<br></td><td>120, 240<br></td></tr><tr><td>Прочностные показатели, кН/м, не менее<br></td><td>50<br></td><td>100</td><td>100</td></tr><tr><td>Удлинение, % не более<br></td><td>4<br></td><td>4<br></td><td>4<br></td></tr><tr><td>Размеры ячеек, мм<br></td><td>25х25<br></td><td>25х25<br></td><td>25х25</td></tr></tbody></table>',1,'2017-06-26 00:14:52','2017-06-26 11:37:25');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `requisites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requisites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `requisites` WRITE;
/*!40000 ALTER TABLE `requisites` DISABLE KEYS */;
INSERT INTO `requisites` VALUES (1,'ОГРН','1096195006258',100,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(2,'ИНН','6167072620',90,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(3,'КПП','616701001',80,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(4,'Банк','Южный ф-л ПАО «Промсвязьбанк» г.Волгоград',70,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(5,'р/сч.','40702810691000089501',60,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(6,'кор/сч.','30101810100000000715',50,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(7,'БИК','041806715',40,1,'2017-06-26 00:03:42','2017-06-26 00:03:42'),(8,'ОКПО','62286231',30,1,'2017-06-26 00:03:42','2017-06-26 00:03:42');
/*!40000 ALTER TABLE `requisites` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
INSERT INTO `role_users` VALUES (1,1,'2017-06-26 00:03:42','2017-06-26 00:03:42');
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Администратор','','2017-06-26 00:03:42','2017-06-26 00:03:42'),(2,'user','Пользователь','','2017-06-26 00:03:42','2017-06-26 00:03:42');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `throttle` WRITE;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@admin.com','$2y$10$sxFCnAwBCKuNKELWwkepQufyEeUTgvUyPhHQcX.aiz2UAUbc2WHWy',NULL,'2017-06-26 00:04:43','Admin','Admin',1,'2017-06-26 00:03:42','2017-06-26 00:04:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

