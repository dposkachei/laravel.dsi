
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` VALUES (1,1,'1234',1,'2017-04-16 20:31:08','2017-04-16 20:31:07','2017-04-16 20:31:08'),(2,2,'1234',1,'2017-04-16 20:32:20','2017-04-16 20:32:20','2017-04-16 20:32:20'),(3,3,'1234',1,'2017-04-16 20:33:55','2017-04-16 20:33:55','2017-04-16 20:33:55'),(4,4,'1234',1,'2017-04-16 20:34:31','2017-04-16 20:34:31','2017-04-16 20:34:31'),(5,5,'1234',1,'2017-04-16 20:36:09','2017-04-16 20:36:09','2017-04-16 20:36:09'),(6,7,'1234',1,'2017-04-18 20:23:09','2017-04-18 20:23:09','2017-04-18 20:23:09'),(7,8,'1234',1,'2017-04-18 20:23:22','2017-04-18 20:23:22','2017-04-18 20:23:22'),(8,9,'1234',1,'2017-04-18 20:26:28','2017-04-18 20:26:28','2017-04-18 20:26:28'),(9,10,'1234',1,'2017-04-18 20:27:19','2017-04-18 20:27:19','2017-04-18 20:27:19'),(10,11,'1234',1,'2017-04-18 20:29:34','2017-04-18 20:29:34','2017-04-18 20:29:34'),(11,12,'1234',1,'2017-04-18 20:31:24','2017-04-18 20:31:24','2017-04-18 20:31:24'),(12,13,'1234',1,'2017-04-18 20:32:42','2017-04-18 20:32:42','2017-04-18 20:32:42'),(13,15,'1234',1,'2017-04-18 20:35:57','2017-04-18 20:35:57','2017-04-18 20:35:57');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `commentable_id` int(11) DEFAULT NULL,
  `commentable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commented_id` int(11) DEFAULT NULL,
  `commented_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `rate` double(15,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_id` int(11) NOT NULL,
  `imageable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (20,'0000_00_00_000000_create_comments_table',1),(21,'2014_07_02_230147_migration_cartalyst_sentinel',1),(22,'2016_06_01_000001_create_oauth_auth_codes_table',1),(23,'2016_06_01_000002_create_oauth_access_tokens_table',1),(24,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(25,'2016_06_01_000004_create_oauth_clients_table',1),(26,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(27,'2017_02_24_180912_create_products_table',1),(28,'2017_03_03_100000_create_options_table',1),(29,'2017_03_24_001152_create_images_table',1),(30,'2017_03_31_171603_update_products_add_status_column',1),(31,'2017_04_03_003826_update_products_add_description_column',1),(32,'2017_04_11_222838_create_categories_table',1),(33,'2017_04_16_164515_update_products_add_category_id_column',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `persistences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `persistences` WRITE;
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` VALUES (2,2,'no8fktZNkjxgr3r13lRgmIcNnTUBlFkM','2017-04-16 20:32:20','2017-04-16 20:32:20'),(13,9,'6JPuVAUvLDPxlVYIREsKav9LWpWCv26T','2017-04-18 20:26:28','2017-04-18 20:26:28'),(14,10,'6ZiyxGI0y3ETOVE2fC5DoxTTFGGfA64M','2017-04-18 20:27:19','2017-04-18 20:27:19'),(17,1,'w6OB8VQ0OcYIneGjqAHLOycvtCkFJSgn','2017-04-18 22:05:52','2017-04-18 22:05:52');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_category_id_foreign` (`category_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,NULL,'Agnes Wolf',NULL,1,'2017-04-16 19:26:14','2017-04-16 19:26:14'),(2,NULL,'Jack Schroeder',NULL,1,'2017-04-16 19:26:14','2017-04-16 19:26:14'),(3,NULL,'Kurtis Ortiz IV',NULL,1,'2017-04-16 19:27:34','2017-04-16 19:27:34'),(4,NULL,'Mr. Seth Carroll',NULL,1,'2017-04-16 19:27:34','2017-04-16 19:27:34'),(5,NULL,'Santiago Langosh',NULL,1,'2017-04-16 19:29:37','2017-04-16 19:29:37'),(6,NULL,'Jaida Hilpert',NULL,1,'2017-04-16 19:29:37','2017-04-16 19:29:37');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
INSERT INTO `role_users` VALUES (1,1,'2017-04-18 19:33:01','2017-04-18 19:33:01'),(2,2,'2017-04-16 20:32:20','2017-04-16 20:32:20'),(3,2,'2017-04-16 20:33:55','2017-04-16 20:33:55'),(4,2,'2017-04-16 20:34:31','2017-04-16 20:34:31'),(5,2,'2017-04-16 20:36:09','2017-04-16 20:36:09'),(6,2,'2017-04-16 21:47:11','2017-04-16 21:47:11'),(7,2,'2017-04-18 20:23:09','2017-04-18 20:23:09'),(8,2,'2017-04-18 20:23:22','2017-04-18 20:23:22'),(9,2,'2017-04-18 20:26:28','2017-04-18 20:26:28'),(10,2,'2017-04-18 20:27:19','2017-04-18 20:27:19'),(11,2,'2017-04-18 20:29:34','2017-04-18 20:29:34'),(12,2,'2017-04-18 20:31:24','2017-04-18 20:31:24'),(13,2,'2017-04-18 20:32:42','2017-04-18 20:32:42'),(14,2,'2017-04-18 20:34:31','2017-04-18 20:34:31'),(15,2,'2017-04-18 20:35:57','2017-04-18 20:35:57');
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Администратор','','2017-04-16 19:30:39','2017-04-16 19:30:39'),(2,'user','Пользователь','','2017-04-16 19:30:39','2017-04-16 19:30:39');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `throttle` WRITE;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
INSERT INTO `throttle` VALUES (1,NULL,'global',NULL,'2017-04-16 20:35:28','2017-04-16 20:35:28'),(2,NULL,'ip','127.0.0.1','2017-04-16 20:35:28','2017-04-16 20:35:28'),(3,1,'user',NULL,'2017-04-16 20:35:28','2017-04-16 20:35:28'),(4,NULL,'global',NULL,'2017-04-16 20:35:35','2017-04-16 20:35:35'),(5,NULL,'ip','127.0.0.1','2017-04-16 20:35:35','2017-04-16 20:35:35'),(6,1,'user',NULL,'2017-04-16 20:35:35','2017-04-16 20:35:35'),(7,NULL,'global',NULL,'2017-04-16 20:44:47','2017-04-16 20:44:47'),(8,NULL,'ip','127.0.0.1','2017-04-16 20:44:47','2017-04-16 20:44:47'),(9,5,'user',NULL,'2017-04-16 20:44:47','2017-04-16 20:44:47'),(10,NULL,'global',NULL,'2017-04-16 20:44:47','2017-04-16 20:44:47'),(11,NULL,'ip','127.0.0.1','2017-04-16 20:44:47','2017-04-16 20:44:47'),(12,5,'user',NULL,'2017-04-16 20:44:47','2017-04-16 20:44:47');
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'dposkachei@gmail.com','$2y$10$dhKE4G8iQg8UIuyQtHDJ6u7BKcM1nbs6YAfPoqxsnrx0ylIJvFtDe',NULL,'2017-04-18 22:05:52','Дмитрий2',NULL,1,'2017-04-16 20:31:05','2017-04-18 22:05:52'),(2,'dposkachei2@gmail.com','$2y$10$5lXcdMUoJQr77cJhM9dsxeP4bOBwLKMZ1MPjXcHilCmG7aw43/WMi',NULL,'2017-04-16 20:32:20','Дмитрий',NULL,0,'2017-04-16 20:32:20','2017-04-16 21:05:54'),(3,'dposkachei3@gmail.com','$2y$10$Fq5fYaM8i1pRCNP7/BXyNub9orL0qrVOSzDa6Wp9bUOd3JUVQuJO.',NULL,NULL,'Дмитрий',NULL,0,'2017-04-16 20:33:55','2017-04-16 22:11:09'),(4,'dposkachei4@gmail.com','$2y$10$fU/uEYRKfekWMMs191t3selVTXniHrVcCTuAJ5fWYU0HVJmS47qcG',NULL,NULL,'Дмитрий',NULL,1,'2017-04-16 20:34:31','2017-04-16 20:34:31'),(5,'dimapos@bk.ru','$2y$10$BvIH9PlbbSAfkXOv6vAsduX7hAUIqeQb4/9S7r.4shwp6GHWRzDhu',NULL,'2017-04-16 21:01:15','Дмитрий',NULL,1,'2017-04-16 20:36:09','2017-04-16 21:01:15'),(6,'dposkachei233@gmail.com','$2y$10$YF1qoavI2Ss81kjUyWKjGuDBpLDj.2OJRwP2S02FGRZqkcNDwNVM2',NULL,NULL,'Дмитрий',NULL,1,'2017-04-16 21:47:11','2017-04-16 21:47:28'),(7,'dposk123achei@gmail.com','$2y$10$L4nZINnuP6X7yPVOl7fEqezHAMJBcpPL2AqS7Ko4JRbtBS8MpuXBC',NULL,NULL,'Дмитрий',NULL,1,'2017-04-18 20:23:09','2017-04-18 20:23:09'),(8,'dposk1223achei@gmail.com','$2y$10$9C6m0cyQN1lwwrLiFB2N4.EfIZ4y/.gy51O4pZkP9ZitbWm0zQ2rK',NULL,NULL,'Дмитрий',NULL,1,'2017-04-18 20:23:22','2017-04-18 20:23:22'),(9,'dposk122323achei@gmail.com','$2y$10$HXH/ObX//FM3rFmNGCi4k.fp7H/Vi55pu2Atrj4mHvMPllrg8eteC',NULL,'2017-04-18 20:26:28','Дмитрий',NULL,1,'2017-04-18 20:26:28','2017-04-18 20:26:28'),(10,'dposk1223223achei@gmail.com','$2y$10$oMs51S66UKaEqhe4mL46U.p2q1bSHEgX5X6QI1TSdUD7wM.zKewVK',NULL,'2017-04-18 20:27:19','Дмитрий',NULL,1,'2017-04-18 20:27:19','2017-04-18 20:27:19'),(11,'dposk1achei@gmail.com','$2y$10$SPyn17iKnYycCThm05haveBMZxtgPs1VBPmIalZK2CIld/LIiGD/O',NULL,NULL,'Дмитрий',NULL,1,'2017-04-18 20:29:34','2017-04-18 20:29:34'),(12,'dposka2chei@gmail.com','$2y$10$cdsAjsunU/lu63zYN8jMZuQllUibsMLpIJY3LgqI9InkI.NkGCm2y',NULL,NULL,'Дмитрий',NULL,1,'2017-04-18 20:31:24','2017-04-18 20:31:24'),(13,'dposka2c2hei@gmail.com','$2y$10$xF4bbcLSmQ0rvFk1k2/Obeel95eMkp.zuCJrR9XL53VPM1d9YO6B.',NULL,NULL,'Дмитрий',NULL,1,'2017-04-18 20:32:42','2017-04-18 20:32:42'),(14,'dposka2c222hei@gmail.com','$2y$10$QYfbjGdcRuJiwttz8E/ZHOnP/.Yl6NfWTWDrlgow4DQCk1HppWwq2',NULL,NULL,'Дмитрий',NULL,1,'2017-04-18 20:34:31','2017-04-18 20:34:31'),(15,'dposka2c2222hei@gmail.com','$2y$10$Rd0NzumUQcw4LltTtenn/.Jb54WTV6.Uy/km9DDxp3pBLklwdN45S',NULL,'2017-04-18 20:35:57','Дмитрий',NULL,1,'2017-04-18 20:35:57','2017-04-18 20:35:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

