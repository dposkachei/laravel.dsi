<?php

use Illuminate\Database\Seeder;
use App\Models\Contact;

class ContactsTableSeeder extends Seeder
{
    private $default = [
        'email' => [
            [
                'title' => 'Email',
                'value' => 'dsi@dsi-ug.ru',
                'status' => 2,
            ],
        ],
        'phone' => [
            [
                'title' => 'Телефон',
                'value' => '+7 (863) 229-59-74',
                'status' => 2,
            ], [
                'title' => 'Телефон',
                'value' => '+7 (863) 229-59-74, 229-16-85, факс 210-77-45',
                'status' => 1,
            ]
        ],
        'address' => [
            [
                'title' => 'Юридический адрес',
                'value' => '344019, г. Ростов-на-Дону, ул. 17 линия 2/8',
                'status' => 2,
            ], [
                'title' => 'Почтовый адрес',
                'value' => '344019, г. Ростов-на-Дону, ул. 17 линия 2/8',
                'status' => 1,
            ],
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->default as $key => $default) {
            foreach($default as $value) {
                Contact::create([
                    'key' => $key,
                    'title' => $value['title'],
                    'value' => $value['value'],
                    'status' => $value['status'],
                ]);
            }
        }
    }
}
