<?php

use Illuminate\Database\Seeder;
use App\Models\Article;

class ArticlesTableSeeder extends Seeder
{
    private $default = [
        [
            'title' => 'Условия доставки',
            'text' => 'Текст',
            'icon' => 'truck',
            'status' => 1,
        ], [
            'title' => 'Наши гарантии',
            'text' => 'Текст',
            'icon' => 'shield',
            'status' => 1,
        ], [
            'title' => 'Условия оплаты',
            'text' => 'Текст',
            'icon' => 'credit-card',
            'status' => 1,
        ], [
            'title' => 'Качество',
            'text' => 'Здесь будет текст в несколько строк. Здесь будет текст в несколько строк',
            'icon' => 'diamond',
            'status' => 2,
        ], [
            'title' => 'Долговечность',
            'text' => 'Здесь будет текст в несколько строк. Здесь будет текст в несколько строк',
            'icon' => 'lock',
            'status' => 2,
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->default as $default) {
            Article::create([
                'title' => $default['title'],
                'text' => $default['text'],
                'icon' => $default['icon'],
                'status' => $default['status'],
            ]);
        }
    }
}
