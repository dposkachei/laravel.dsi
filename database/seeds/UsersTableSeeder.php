<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Config;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'email' => 'admin@admin.com',
            'password' => '1234567890',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
        ];
        $oUser = Sentinel::register($data);
        $slugRole = isset(Config::get('site.roles')[0]['slug']) ? Config::get('site.roles')[0]['slug'] : 'admin';
        $role = Sentinel::findRoleBySlug($slugRole);
        $role->users()->attach($oUser);

        $oActivation = Activation::create($oUser);
        $oActivation->code = 1234;
        $oActivation->save();
        Activation::complete($oUser, $oActivation->code);
    }
}
