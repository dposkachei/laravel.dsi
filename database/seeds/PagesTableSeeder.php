<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PagesTableSeeder extends Seeder
{
	private $default = [
		[
			'title' => 'Дорстройиндустрия',
			'page_title' => 'Главная',
			'name' => 'main',
			'text' => '<p><b>"Дорстройиндустрия" </b>- динамично развивающаяся российская компания. </p><p>Мы специализируемся на рынке <b>дорожно-строительных материалов</b>, создания и комплексного оснащения аналитических, испытательных и дорожно-строительных лабораторий, контролирующих качество проводимых работ и используемых материалов.</p>'
		], [
			'title' => 'Продукции',
			'page_title' => 'Продукции',
			'name' => 'products',
		], [
			'title' => 'Категории',
			'page_title' => 'Категории',
			'name' => 'categories',
		], [
			'title' => '"Дорстройиндустрия" - партнерство',
			'page_title' => 'Партнеры',
			'name' => 'partners',
			'text' => '<p>ООО «ДорСтройИндустрия» было создано в 2005 году. Основными направлениями деятельности организации является комплектация строительных лабораторий испытательным оборудованием и средствами измерений, торговля адгезионными и стабилизирующими добавками для битумов и асфальтобетонов, а также другими дорожно-строительными материалами.</p><p>Компания является зарегистрированным дилером ряда российских и зарубежных производителей дорожно-строительных материалов.</p><p>Среди них:</p>'
		], [
			'title' => 'Сертификаты',
			'page_title' => 'Сертификаты',
			'name' => 'certificates',
			'status' => 0
		], [
			'title' => 'ООО «ДСИ» Общество с ограниченной ответственностью «Дорстройиндустрия»',
			'page_title' => 'Контакты',
			'name' => 'contacts',
		]
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$losts = Page::all();
    	if (!empty($losts) && !empty($losts[0])) {
    		foreach($losts as $lost) {
    			$lost->delete();
    		}
    	}
    	foreach($this->default as $key => $value) {
			Page::create([
				'title' => $value['title'],
				'page_title' => $value['page_title'],
				'name' => $value['name'],
				'text' => isset($value['text']) ? $value['text'] : null,
				'status' => isset($value['status']) ? $value['status'] : 1,
			]);
    	}
    }
}
