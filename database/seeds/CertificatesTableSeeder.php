<?php

use Illuminate\Database\Seeder;
use App\Models\Certificate;

class CertificatesTableSeeder extends Seeder
{
    private $default = [
        [
            'title' => 'Сертификат №1',
            'text' => 'Описание сертификата',
            'type' => 0,
            'priority' => 0,
            'status' => 1,
        ], [
            'title' => 'Сертификат №2',
            'text' => 'Описание сертификата',
            'type' => 0,
            'priority' => 0,
            'status' => 1,
        ], [
            'title' => 'Сертификат №3',
            'text' => 'Описание сертификата',
            'type' => 0,
            'priority' => 0,
            'status' => 1,
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->default as $default) {
            Certificate::create([
                'title' => $default['title'],
                'text' => $default['text'],
                'type' => $default['type'],
                'priority' => $default['priority'],
                'status' => $default['status'],
            ]);
        }
    }
}
