<?php

use Illuminate\Database\Seeder;

class RequisitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aRequisites = [
            [
                'title' => 'ОГРН',
                'value' => '1096195006258',
                'priority' => 100
            ], [
                'title' => 'ИНН',
                'value' => '6167072620',
                'priority' => 90
            ], [
                'title' => 'КПП',
                'value' => '616701001',
                'priority' => 80
            ], [
                'title' => 'Банк',
                'value' => 'Южный ф-л ПАО «Промсвязьбанк» г.Волгоград',
                'priority' => 70
            ], [
                'title' => 'р/сч.',
                'value' => '40702810691000089501',
                'priority' => 60
            ], [
                'title' => 'кор/сч.',
                'value' => '30101810100000000715',
                'priority' => 50
            ], [
                'title' => 'БИК',
                'value' => '041806715',
                'priority' => 40
            ], [
                'title' => 'ОКПО',
                'value' => '62286231',
                'priority' => 30
            ],
        ];
        foreach($aRequisites as $aRequisite) {
            \App\Models\Requisite::create($aRequisite);
        }
    }
}
