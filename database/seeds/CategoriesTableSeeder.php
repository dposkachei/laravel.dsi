<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    private $default = [
        [
            'title' => 'Адгезионные добавки к битумам',
            'priority' => 100,
            'status' => 1,
        ], [
            'title' => 'Стабилизирующие добавки для щебеночно-мастичных асфальтобетонных смесей',
            'priority' => 90,
            'status' => 1,
        ], [
            'title' => 'Пластификатор ПБВ',
            'priority' => 80,
            'status' => 1,
        ], [
            'title' => 'Дорожная пропитка для асфальтобетона',
            'priority' => 70,
            'status' => 1,
        ], [
            'title' => 'Модификатор минерального порошка',
            'priority' => 60,
            'status' => 1,
        ], [
            'title' => 'Сетки армирующие',
            'priority' => 50,
            'status' => 1,
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->default as $key => $value) {
            \App\Models\Category::create([
                'title' => $value['title'],
                'priority' => $value['priority'],
                'status' => $value['status'],
            ]);
        }
    }
}
