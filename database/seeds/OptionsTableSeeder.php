<?php

use Illuminate\Database\Seeder;
use App\Models\Option;
use Illuminate\Support\Facades\Config;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Option::create([
            'key' => 'app.title',
            'value' => Config::get('app.name'),
            'title' => 'Заголовок сайта',
            'type' => 1,
            'status' => 1,
        ]);
        $favicon = Option::create([
            'key' => 'app.favicon',
            'title' => 'Иконка сайта',
            'type' => 4,
            'status' => 1,
        ]);
        \App\Models\Image::create([
            'type' => 'option',
            'imageable_type' => Option::class,
            'imageable_id' => $favicon->id,
            'filename' => '6jtanPZXJAHC.png',
            'is_main' => 1,
            'status' => 1
        ]);
        Option::create([
            'key' => 'app.keywords',
            'value' => 'keywords',
            'title' => 'Ключевые слова',
            'type' => 2,
            'status' => 1,
        ]);
        Option::create([
            'key' => 'app.description',
            'value' => 'Description',
            'title' => 'Описание сайта',
            'type' => 2,
            'status' => 1,
        ]);
        Option::create([
            'key' => 'app.copyright',
            'value' => '© ООО "ДорСтройИндустрия"',
            'title' => 'Права',
            'type' => 1,
            'status' => 1,
        ]);
    }
}
