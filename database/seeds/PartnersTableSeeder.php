<?php

use Illuminate\Database\Seeder;
use App\Models\Partner;

class PartnersTableSeeder extends Seeder
{
    private $default = [
        [
            'title' => 'Партнер №1',
            'url' => 'http://a.ru',
            'text' => 'Описание партнера',
            'status' => 1,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->default as $default) {
            Partner::create([
                'title' => $default['title'],
                'url' => $default['url'],
                'text' => $default['text'],
                'status' => $default['status'],
            ]);
        }
    }
}
