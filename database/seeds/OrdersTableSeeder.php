<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oOrder = \App\Models\Order::create([
            'name' => 'Тестовый заказ',
            'email' => 'test@test.com',
            'phone' => 78005553535,
        ]);
        $oProduct = \App\Models\Product::first();
        $oOrder->products()->attach($oProduct->id);
        $oOrder = \App\Models\Order::create([
            'name' => 'Второй Тестовый заказ',
            'email' => 'test@test.com',
        ]);
    }
}
