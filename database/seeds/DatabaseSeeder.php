<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArticlesTableSeeder::class);
        $this->call(CertificatesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
        $this->call(PartnersTableSeeder::class);
        $this->call(RequisitesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        /*
        factory(App\Models\Product::class, 2)->create()->each(function($u) {
            $u->issues()->save(factory(App\Models\Product::class)->make());
        });
        */
    }
}
