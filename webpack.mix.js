const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 | Env settings 'admin', 'app', 'all'
 |
 */

var env = 'app';


var manageAssets = {
    app : function(mix) {
        mix.webpackConfig({
                resolve: {
                    alias: {
                        'load-image':          'blueimp-load-image/js/load-image.js',
                        'load-image-meta':     'blueimp-load-image/js/load-image-meta.js',
                        'load-image-exif':     'blueimp-load-image/js/load-image-exif.js',
                        'canvas-to-blob':      'blueimp-canvas-to-blob/js/canvas-to-blob.js',
                        'jquery-ui/ui/widget': 'blueimp-file-upload/js/vendor/jquery.ui.widget.js'
                    }
                }})
            .sass('resources/assets/sass/app/app.scss', 'public/css/app.css')
            .js([
                'resources/assets/js/app/app.js',
                'resources/assets/js/app/package/app.js',
                'resources/assets/js/common/components/cleave-masks.js',
                'resources/assets/js/common/components/helpers.js',
                'resources/assets/js/common/components/dialogs.js',
                'resources/assets/js/app/package/assets/util.js',
                'resources/assets/js/app/package/assets/main.js',
                'resources/assets/js/common/components/callbacks.js',
                'resources/assets/js/common/components/form.js',
                'resources/assets/js/app/package/main.js'
            ], 'public/js/app.js')
            .version();
    },
    admin : function(mix) {
        mix.webpackConfig({
            resolve: {
                alias: {
                    'load-image':          'blueimp-load-image/js/load-image.js',
                    'load-image-meta':     'blueimp-load-image/js/load-image-meta.js',
                    'load-image-exif':     'blueimp-load-image/js/load-image-exif.js',
                    'canvas-to-blob':      'blueimp-canvas-to-blob/js/canvas-to-blob.js',
                    'jquery-ui/ui/widget': 'blueimp-file-upload/js/vendor/jquery.ui.widget.js'
                }
            }})
            .sass('resources/assets/sass/admin/app.scss', 'public/css/admin.css')
            .js([
                'resources/assets/js/admin/app.js',
                'resources/assets/js/common/components/table/icon.js',
                'resources/assets/js/common/components/cleave-masks.js',
                'resources/assets/js/common/components/helpers.js',
                'resources/assets/js/common/components/dialogs.js',
                'resources/assets/js/common/components/crop.js',
                'resources/assets/js/common/components/file-button.js',
                'resources/assets/js/common/components/desta.js',
                'resources/assets/js/common/components/callbacks.js',
                'resources/assets/js/common/components/form.js',
                'resources/assets/js/admin/package/app.js',
                'resources/assets/js/admin/package/slider.js',
                'resources/assets/js/admin/package/blueimp.js',
                'resources/assets/js/admin/package/fancybox.js'

            ], 'public/js/admin.js');
    }
};

switch(env) {
    case 'app':
        manageAssets.app(mix);
        break;
    case 'admin':
        manageAssets.admin(mix);
        break;
    case 'all':
        manageAssets.app(mix);
        manageAssets.admin(mix);
        break;
    default: break;
}